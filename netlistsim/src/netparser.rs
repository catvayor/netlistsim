use std::collections::HashMap;
use std::io::{Read, BufRead, BufReader};
//use std::str::Chars;

#[derive(PartialEq, Debug)]
pub enum TokOp{
    //basic
    Tnot,
    Tand,
    Tnand,
    Tor,
    Tnor,
    Txor,
    Txnor,
    Tmux,

    //bus
    Tconcat,
    Tslice,
    Tselect,

    //memory
    Treg,
    Trom,
    Tram
}

#[derive(PartialEq, Debug)]
pub enum TToken{
    Tsep,
    TbusSize,
    Tident(String),
    Tnum(String),
    Top(TokOp),
    Tequality,

    Tinput,
    Toutput,
    Tvar,
    Tbegin,

    Tnone,
    Tinvalid
}

fn token_from(tok : &str) -> TToken{
    use TToken::*;
    use TokOp::*;

    match tok{
        //block delimiters
        "INPUT" =>  return Tinput,
        "OUTPUT" => return Toutput,
        "VAR" =>    return Tvar,
        "IN" =>     return Tbegin,
        
        //gate
        "NOT" =>  return Top(Tnot),
        "AND" =>  return Top(Tand),
        "NAND" => return Top(Tnand),
        "OR" =>   return Top(Tor),
        "NOR" =>  return Top(Tnor),
        "XOR" =>  return Top(Txor),
        "XNOR" => return Top(Txnor),
        "MUX" =>  return Top(Tmux),

        "CONCAT" => return Top(Tconcat),
        "SLICE" =>  return Top(Tslice),
        "SELECT" => return Top(Tselect),

        "REG" => return Top(Treg),
        "ROM" => return Top(Trom),
        "RAM" => return Top(Tram),

        //other
        "=" => return Tequality,
        "," => return Tsep,
        ":" => return TbusSize,
        "" => Tnone,

        _ => {
            if tok.chars().all(|c : char| { c.is_ascii_digit() }) {
                return Tnum(tok.to_string());
            }
            
            if tok.chars().all(|c : char| { c.is_ascii_alphanumeric() || c == '_'}) &&
                 ! tok.chars().next().unwrap().is_ascii_digit()
            {
                return Tident(tok.to_string());
            }

            return Tinvalid;
        }
    }
}

pub struct Lexer<R: Read>{
    buffer : BufReader<R>,
    line_buf : Vec<char>,
    line_iter : usize,
    line_counter : usize,
    tok_pos_ : (usize, usize),
}

impl<R : Read> Lexer<R>{

    pub fn new(reader : R) -> Lexer<R>{
        Lexer{
            buffer : BufReader::new(reader),
            line_buf : Vec::new(),
            line_iter : 0,
            line_counter : 0,
            tok_pos_ : (0,0),
        }
    }

    fn get_next_line(&mut self) -> Result<(), ()>{
        self.line_buf.clear();
        self.line_iter = 0;
        self.line_counter += 1;
        
        let mut line = String::new();
        let size = self.buffer.read_line(&mut line)
            .expect(format!("failed to read buffer at line {}", self.line_counter).as_str());
        
        if size == 0 { return Err(()); }
        self.line_buf = line.chars().collect();
        self.line_buf.push(' '); //risky if don't end by white caractere
        Ok(())
    }

    fn skip_blank(&mut self) -> Result<(), ()>{
        loop{
            if self.line_iter == self.line_buf.len(){
                self.get_next_line()?;
            }
            while self.line_iter < self.line_buf.len(){
                if !self.line_buf[self.line_iter].is_ascii_whitespace() { return Ok(()); }
                self.line_iter += 1;
            }
        }
    }

    fn get_non_white(&mut self) -> Option<char>{
        let c = self.line_buf[self.line_iter];
        self.line_iter += 1;
        if c.is_ascii_whitespace() {
            None
        } else {
            Some(c)
        }
    }

    pub fn get_pos(&self) -> (usize, usize){//line, char
        (self.line_counter, self.line_iter + 1)
    }

    pub fn tok_pos(&self) -> (usize, usize){
        self.tok_pos_
    } 

    pub fn token(&mut self) -> Result<TToken, ()>{
        use TToken::*;

        self.skip_blank()?;
        self.tok_pos_ = self.get_pos();
        
        let mut current = String::new();

        while let Some(c) = self.get_non_white(){
            if [':', ',', '='].contains(&c) {
                if current.is_empty(){
                    match c {
                        ':' => return Ok(TbusSize),
                        ',' => return Ok(Tsep),
                        '=' => return Ok(Tequality),

                        _ => panic!("absurd comportement"),
                    }
                } else{
                    self.line_iter -= 1;
                    break;
                } 
            }
            current.push(c);
        }

        let tok = token_from(&current);
        if tok == Tinvalid{
            panic!("{:?} : invalid token {}", self.tok_pos(), current);
        }
        return Ok(tok);
    }
}


pub type Ident = usize;

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum Arg {
    Var(Ident),
    Constant((u64, usize)),
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum Op {
    Identity(Arg),
    
    //basic op
    Not(Arg),
    And(Arg,Arg),
    Nand(Arg,Arg),
    Or(Arg,Arg),
    Nor(Arg,Arg),
    Xor(Arg,Arg),
    Xnor(Arg,Arg),
    Mux(Arg,Arg,Arg),

    //bus
    Concat(Arg,Arg),
    Slice(u32, u32, Arg),
    Select(u32, Arg),

    //memory
    Reg(Ident),
    Rom(u32, u32, Arg),
    Ram(u32, u32, Arg, Arg, Arg, Arg),
}


#[derive(Debug)]
pub struct VarT{
    pub name : String,
    pub size : usize,
}

#[derive(Debug)]
pub struct ProgramT{
    pub vars : Vec<VarT>,
    pub inputs : Vec<Ident>,
    pub outputs : Vec<Ident>,
    pub equations : Vec<(Ident, Op)>,
    pub reg_var : Vec<Ident>,
}

pub fn is_reg_op(op: &Op) -> bool{
    match op{
        &Op::Reg(_) => true,
        _ => false
    }
}

fn get_arg(tok : &TToken, indexes : &HashMap<String, Ident>, vars : &Vec<VarT>)
    -> Result<(Arg, usize), ()>
{
    use TToken::*;
    use Arg::*;
    match tok{
        Tident(name) => {
            let idx = match indexes.get(name){ Some(i) => i, None => return Err(()) };
            Ok((Var(*idx), vars[*idx].size))
        },
        Tnum(n) => {
            let size = n.len();
            let val = match u64::from_str_radix(n.as_str(), 2){ Ok(v) => v, Err(_) => return Err(())};
            Ok((Constant((val, size)), size))
        },
        _ => Err(()),
    }
}

fn get_num(tok : &TToken) -> Result<u32, ()>
{
    use TToken::*;
    if let Tnum(n) = tok { 
        Ok(u32::from_str_radix(n.as_str(), 10).unwrap()) 
    } else { Err(()) }
}

fn get_ident(tok : &TToken) -> Result<String, ()>
{
    use TToken::*;
    if let Tident(name) = tok { Ok(name.clone()) } else { Err(()) }
}

fn get_arg_lex<R: Read>(lexer : &mut Lexer<R>, indexes : &HashMap<String, Ident>, vars : &Vec<VarT>)
    -> (Arg, usize)
{
    let tok = lexer.token().expect("expected identifier or constant, got eof");
    get_arg(&tok, indexes, vars).expect(
        format!("{:?} : expected identifier or constant, got {:?} {}", lexer.tok_pos(), tok,
                "(this could be caused by an unknown identifier or il-formed constant)").as_str())
}

fn get_num_lex<R: Read>(lexer : &mut Lexer<R>) -> u32{ 
    let tok = lexer.token().expect("expected integer, got eof");
    get_num(&tok).expect(
        format!("{:?} : expected integer, got {:?}", lexer.tok_pos(), tok).as_str())
}

fn get_ident_lex<R: Read>(lexer : &mut Lexer<R>) -> String{
    let tok = lexer.token().expect("expected identifier, got eof");
    get_ident(&tok).expect(
        format!("{:?} : expected identifier, got {:?}", lexer.tok_pos(), tok).as_str())
}

fn get_op<R: Read>(lexer : &mut Lexer<R>, indexes : &HashMap<String, Ident>,
          dest : &VarT, vars : &Vec<VarT>)
    -> Op
{
    use TToken::*;
    use TokOp::*;
    use Op::*;

    let tok = lexer.token().expect("expected expression, got eof");
    let expr_pos = lexer.tok_pos();
    match tok{
        Top(op) =>{
            match op{
                //basic
                Tnot => {
                    let (arg, s) = get_arg_lex(lexer, indexes, vars);
                    if s == dest.size {
                        return Not(arg);
                    }
                    panic!("{:?}, not: operand and destination should be of same size", expr_pos);
                },
                Tand => {
                    let (arg1, s1) = get_arg_lex(lexer, indexes, vars);
                    let (arg2, s2) = get_arg_lex(lexer, indexes, vars);
                    if s1 == s2 && dest.size == s1 {
                        return And(arg1, arg2);
                    }
                    panic!("{:?}, and: every operand should be of same size as dest", expr_pos);
                },
                Tnand => {
                    let (arg1, s1) = get_arg_lex(lexer, indexes, vars);
                    let (arg2, s2) = get_arg_lex(lexer, indexes, vars);
                    if s1 == s2 && dest.size == s1{
                        return Nand(arg1, arg2);
                    }
                    panic!("{:?} nand: every operand should be of same size as dest", expr_pos);
                },
                Tor => {
                    let (arg1, s1) = get_arg_lex(lexer, indexes, vars);
                    let (arg2, s2) = get_arg_lex(lexer, indexes, vars);
                    if s1 == s2 && dest.size == s1{
                        return Or(arg1, arg2);
                    }
                    panic!("{:?}, or: every operand should be of same size as dest", expr_pos);
                },
                Tnor => {
                    let (arg1, s1) = get_arg_lex(lexer, indexes, vars);
                    let (arg2, s2) = get_arg_lex(lexer, indexes, vars);
                    if s1 == s2 && dest.size == s1{
                        return Nor(arg1, arg2)
                    }
                    panic!("{:?}, nor: every operand should be of same size as dest", expr_pos);
                },
                Txor => {
                    let (arg1, s1) = get_arg_lex(lexer, indexes, vars);
                    let (arg2, s2) = get_arg_lex(lexer, indexes, vars);
                    if s1 == s2 && dest.size == s1{
                        return Xor(arg1, arg2);
                    }
                    panic!("{:?}, xor: every operand should be of same size as dest", expr_pos);
                },
                Txnor => {
                    let (arg1, s1) = get_arg_lex(lexer, indexes, vars);
                    let (arg2, s2) = get_arg_lex(lexer, indexes, vars);
                    if s1 == s2 && dest.size == s1{
                        return Xnor(arg1, arg2);
                    }
                    panic!("{:?}, xnor: every operand should be of same size as dest", expr_pos);
                },
                Tmux => {
                    let (arg1, s1) = get_arg_lex(lexer, indexes, vars);
                    let (arg2, s2) = get_arg_lex(lexer, indexes, vars);
                    let (arg3, s3) = get_arg_lex(lexer, indexes, vars);
                    if s1 == 1 && s2 == s3 && dest.size == s3{    
                        return Mux(arg1, arg2, arg3);
                    }
                    panic!("{:?}, mux: choices should have dest size, chooser size 1", expr_pos);
                },

                //bus
                Tconcat => {
                    let (arg1, s1) = get_arg_lex(lexer, indexes, vars);
                    let (arg2, s2) = get_arg_lex(lexer, indexes, vars);
                    if s1 + s2 == dest.size{
                        return Concat(arg1, arg2);
                    }
                    panic!("{:?}, concat: sum of input sizes should be dest size", expr_pos);
                },
                Tslice => {
                    let i1 = get_num_lex(lexer);
                    let i2 = get_num_lex(lexer);
                    let (arg, s) = get_arg_lex(lexer, indexes, vars);
                    if dest.size == (i2 - i1 + 1) as usize && s > i2 as usize{
                        return Slice(i1, i2, arg);
                    }
                    panic!("{:?}, slice: out bus should have sam size && in bus big enough", expr_pos);
                },
                Tselect => {
                    let i = get_num_lex(lexer);
                    let (arg, s) = get_arg_lex(lexer, indexes, vars);
                    if s > (i as usize) && dest.size == 1{
                        return Select(i, arg)
                    }
                    panic!("{:?}, select: dest should of size 1 && in bus big enough", expr_pos);
                },

                //memory
                Treg => {
                    let name = get_ident_lex(lexer);
                    let idx = indexes.get(&name).expect(
                        format!("{:?} : unknow identifier {}", lexer.tok_pos(), name).as_str());
                    if dest.size == vars[*idx].size{
                        return Reg(*idx);
                    }
                    panic!("{:?}, reg : both input and output should have the same size", expr_pos);
                },
                Trom => {
                    let addrs = get_num_lex(lexer);
                    let words = get_num_lex(lexer);
                    let (arg, s) = get_arg_lex(lexer, indexes, vars);
                    if dest.size == words as usize && s == addrs as usize{
                        return Rom(addrs, words, arg)
                    }
                    panic!("{:?}, rom: out bus should have word size && in bus addr size", expr_pos);
                },
                Tram => {
                    let addrs = get_num_lex(lexer);
                    let words = get_num_lex(lexer);
                    let (argr, sr) = get_arg_lex(lexer, indexes, vars);
                    let (arge, se) = get_arg_lex(lexer, indexes, vars);
                    let (argwa, swa) = get_arg_lex(lexer, indexes, vars);
                    let (argwv, sva) = get_arg_lex(lexer, indexes, vars);
                    if dest.size == words as usize && sva == words as usize &&
                       sr == addrs as usize && swa == addrs as usize && se == 1
                    {
                        return Ram(addrs, words, argr, arge, argwa, argwv)
                    }
                    panic!("{:?} ram: out/write bus should have word size && addr bus addr size and we 1", expr_pos);
                },
            }
        },
        _ => {
            let (arg, s) = get_arg(&tok, indexes, vars).expect(
                format!("{:?} expected expression got {:?} {}", expr_pos, tok,
                        "(this could be caused by an unknown identifier or il-formed constant)").as_str());
            if dest.size == s{
                return Identity(arg);
            }
            panic!("{:?} : unmatched bus size", expr_pos);
        },
    }
}

pub fn make_program<R: Read> (lexer : &mut Lexer<R>) -> ProgramT{
    use TToken::*;

    if let Ok(Tinput) = lexer.token(){ }
    else {
        panic!("no inputs");
    }

    //read inputs
    let mut in_names = Vec::<String>::new();
    
    loop { 
        let tok = lexer.token().expect("expected identifier or output, got eof");
        
        if tok == Toutput{ break; }

        let name = get_ident(&tok).expect(
            format!("{:?} : expected identifer, got {:?}", lexer.tok_pos(), tok).as_str());
        in_names.push(name.to_string());

        if let Ok(tok) = lexer.token(){
            if tok == Toutput{ break; }
            if tok == Tsep{ continue; }
            panic!("{:?} : expected output or , got {:?}", lexer.tok_pos(), tok);
        }//loop will do Err case
    }
    
    //read outputs
    let mut out_names = Vec::<String>::new();
    
    loop{
        let tok = lexer.token().expect("expected identifier or var, got eof");
        
        if tok == Tvar{ break; }

        let name = get_ident(&tok).expect(
            format!("{:?} : expected identifier, got {:?}", lexer.tok_pos(), tok).as_str());
        out_names.push(name.to_string());

        if let Ok(tok) = lexer.token(){
            if tok == Tvar { break; }
            if tok == Tsep { continue; }
            panic!("{:?} : expected var or , got {:?}", lexer.tok_pos(), tok);
        }//loop will do Err case
    }

    //read var and register them
    let mut indexes = HashMap::<String, Ident>::new();
    let mut vars = Vec::<VarT>::new();
    
    loop{
        let tok = lexer.token().expect("expected identifier or in, got eof");
        
        if tok == Tbegin{ break; }
        
        let name = get_ident(&tok).expect(
            format!("{:?} : expected identifier, got {:?}", lexer.tok_pos(), tok).as_str());
        
        if indexes.contains_key(&name){
            panic!("{:?} : second declaration of {}", lexer.tok_pos(), name);
        }

        let idx = vars.len();
        indexes.insert(name.to_string(), idx);

        let mut tok = lexer.token().expect("expected :, , or in, got eof");
        
        if tok == TbusSize{
            tok = lexer.token().expect("expected integer, got eof");

            let size = get_num(&tok).expect(
                format!("{:?} : expected integer, got {:?}", lexer.tok_pos(), tok).as_str());
            if size > 0  && size < 65{
                vars.push(VarT{name: name.to_string(), size: size as usize});
            } else if size < 65{
                panic!("{:?} : bus size should be > 0", lexer.tok_pos());
            } else {
                panic!("{:?} : bus size > 64 are not supported", lexer.tok_pos());
            }

            tok = lexer.token().expect("expected , or in, got eof");
        } else {
            vars.push(VarT{name: name.to_string(), size: 1});
        }

        if tok == Tbegin { break; }
        if tok == Tsep { continue; }

        panic!("{:?} expected , or in, got {:?}", lexer.tok_pos(), tok);
    }

    let mut inputs = Vec::<Ident>::new();
    let mut outputs = Vec::<Ident>::new();
    for name in in_names{
        let idx = indexes.get(&name).expect(
            format!("inputs : unknown identifier {}", name).as_str());
        inputs.push(*idx);
    }
    for name in out_names{
        let idx = indexes.get(&name).expect(
            format!("outputs : unknown identifier {}", name).as_str());
        outputs.push(*idx);
    }


    //read equations
    let mut eqs = Vec::<(Ident, Op)>::new();
    let mut reg = Vec::<Ident>::new();
    while let Ok(tok) = lexer.token() {
        let name = get_ident(&tok).expect(
            format!("{:?} : expected identifier, got {:?}", lexer.tok_pos(), tok).as_str());
        
        let idx = indexes.get(&name).expect(
            format!("{:?} : unknown identifier {}", lexer.tok_pos(), name).as_str());
        
        let tok = lexer.token().expect("expected =, got eof");

        if tok != Tequality { panic!("{:?} : expected =, got {:?}", lexer.tok_pos(), tok); }

        let op = get_op(lexer, &indexes, &vars[*idx], &vars);
        
        if is_reg_op(&op){
            reg.push(*idx);
        }
        eqs.push((*idx, op));
    }

    return ProgramT{
        vars: vars,
        inputs: inputs,
        outputs: outputs,
        equations: eqs,
        reg_var: reg,
    };
}

