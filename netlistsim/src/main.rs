use std::env;
use std::fs::File;
use std::process::{Command, Stdio};

mod netparser;
mod scheduler;
mod code_maker;

fn print_help(){
    println!(
"Usage : `netlistsim <netlist_file> [-s <source_output>] [[--lto] -o <output_file>]`  

Compile the `netlist_file` input into rust code and eventually compile it.  
    `-s` : specify a file for the compiled code.  
    `-o` : specify a file for the executable file.
    `--lto` : tells rustc to activate lto (time expensive).
    `-h`, `-?`, `--help` : print this and stop.

If `-s` and `-o` are not specified, the code will be printed in console.

Note : not giving any arguments will print this but with an error code.

## Produced code

Let `circuit` be the executable file.

Usage : `circuit [rom_file0 [rom_file1 [ ... ]]] [-n <step_number>] [--no-out | --print-freq <freq>]`

Will simulate the circuit, with the `rom_file`s specified.  
    `-n` : specify the number of steps to simulate (will not stop if not specified).  
    `--no-out` : will print only the last output.  
    `--print-freq` : will print the output every `freq` steps (every step if not specified).  
    `-h`, `-?`, `--help` : print this and stop.  

Note : extra `rom_file`s will be ignored, and if the circuit requires more `rom_file`s than given, it will ask them with the name of the variable reading ROM.
");
}

fn main() -> Result<(),()> {
    let args: Vec<String> = env::args().collect();

    if args.len() <= 1{
        print_help();
        return Err(());
    }

    let mut i = 1;

    let mut lto = false;
    let mut in_filename = String::new();
    let mut src_filename = String::new();
    let mut out_filename = String::new();

    while i < args.len(){
        match args[i].as_str(){
            "-s" => {
                if i == args.len() - 1{
                    panic!("-s : file not specified");
                }
                src_filename = args[i+1].clone();
                i += 2;
            },

            "-o" => {
                if i == args.len() - 1{
                    panic!("-o : file not specified");
                }
                out_filename = args[i+1].clone();
                i += 2;
            },

            "--lto" => {
                lto = true;
                i += 1;
            },

            "-h" | "--help" | "-?" => {
                print_help();
                return Ok(());
            },

            _ =>{
                if args[i].starts_with("-"){
                    panic!("unknown option : {}", args[i]);
                }
                if !in_filename.is_empty(){
                    panic!("can't specify two file");
                }
                in_filename = args[i].clone();
                i += 1;
            }
        }
    }

    if in_filename.is_empty(){
        panic!("no input file !");
    }

    let file = File::open(in_filename).expect("failed to open input file");

    let mut lexer = netparser::Lexer::new(file);
    let mut prgm = netparser::make_program(&mut lexer);
    scheduler::schedule(&mut prgm);
    
    if !src_filename.is_empty(){
        let mut file = File::create(src_filename.clone()).expect("failed to open source file");
        code_maker::make_rs(&prgm, &mut file).expect("failed to write code in source file");
    }

    if !out_filename.is_empty() {
        let mut rustc = match (lto, src_filename.is_empty()){
            
            (false, false) =>
                Command::new("rustc")
                    .arg("-C").arg("opt-level=3")
                    //.arg("-C").arg("lto")
                    .arg(src_filename.clone())
                    .arg("-o")
                    .arg(out_filename.as_str())
                    .stdin(Stdio::piped())
                    .stdout(Stdio::inherit())
                    .stderr(Stdio::inherit())
                    .spawn()
                    .expect("failed to start rustc"),

            (true, false) =>
                Command::new("rustc")
                    .arg("-C").arg("opt-level=3")
                    .arg("-C").arg("lto")
                    .arg(src_filename.clone())
                    .arg("-o")
                    .arg(out_filename.as_str())
                    .stdin(Stdio::piped())
                    .stdout(Stdio::inherit())
                    .stderr(Stdio::inherit())
                    .spawn()
                    .expect("failed to start rustc"),

            (false, true) =>
                Command::new("rustc")
                    .arg("-C").arg("opt-level=3")
                    //.arg("-C").arg("lto")
                    .arg("-")
                    .arg("-o")
                    .arg(out_filename.as_str())
                    .stdin(Stdio::piped())
                    .stdout(Stdio::inherit())
                    .stderr(Stdio::inherit())
                    .spawn()
                    .expect("failed to start rustc"),

            (true, true) =>
                Command::new("rustc")
                    .arg("-C").arg("opt-level=3")
                    .arg("-C").arg("lto")
                    .arg("-")
                    .arg("-o")
                    .arg(out_filename.as_str())
                    .stdin(Stdio::piped())
                    .stdout(Stdio::inherit())
                    .stderr(Stdio::inherit())
                    .spawn()
                    .expect("failed to start rustc"),
        };

        if src_filename.is_empty() {
            let mut rustc_stdin =
                rustc.stdin.take().expect("failed to take input of rustc");
            code_maker::make_rs(&prgm, &mut rustc_stdin)
                .expect("failed to pipe program in rustc"); 
        }

        let success = rustc.wait()
                           .expect("failed to wait rustc")
                           .success();
        if !success{
            panic!("compilation failed");
        }
    } else if src_filename.is_empty(){
        code_maker::make_rs(&prgm, &mut std::io::stdout())
            .expect("failed to pipe program in rustc"); 
    }

    Ok(())
}
