use crate::netparser::*;
use std::io::*;

macro_rules! PREFIX { () => { 
"use std::env;
use std::cmp;

#[allow(unused_imports)]
use std::{{
    fs::{{self, File}},
    io::{{self, Write, Read}}
    }};

fn print_help(){{
    println!(
\"Let `circuit` be this executable file.

Usage : `circuit [rom_file0 [rom_file1 [ ... ]]] [-n <step_number>] [--no-out | --print-freq <freq>]`

Will simulate the circuit, with the `rom_file`s specified.  
    `-n` : specify the number of steps to simulate (will not stop if not specified).  
    `--no-out` : will print only the last output.  
    `--print-freq` : will print the output every `freq` steps (every step if not specified).  
    `-h`, `-?`, `--help` : print this and stop.

Note : extra `rom_file`s will be ignored, and if the circuit requires more `rom_file`s than given, it will ask them with the name of the variable reading ROM.\");
}}

#[allow(dead_code)]
fn get_in_bus(size: usize, name: &str) -> u64{{
    loop{{
        print!(\"{{}}: {{}}? \", name, size);
        let _ = io::stdout().flush();
        let mut buf = String::new();
        let stdin = io::stdin();
    
        if match stdin.read_line(&mut buf){{
            Ok(n) => n == size,
            _ => false
        }} {{ continue; }}
        
        if let Ok(res) = u64::from_str_radix(buf.trim(), 2){{
            return res;
        }}
    }}
}}

fn main(){{
    //step number prevision
    let args: Vec<String> = env::args().collect();
    let argc = args.len();

    let mut step_number = !(0u64);
    let mut argi = 1usize;
    let mut print_all = true;
    let mut print_freq = 0u64;

    let mut rom_filenames : Vec<String> = Vec::new();

    while argi < argc{{
        match args[argi].as_str(){{
            \"-n\" => {{
                if argi+1 == argc {{
                    panic!(\"-n requiere the step count\");
                }}
                step_number = args[argi+1].parse::<u64>().unwrap();
                argi += 2;
            }},
            \"--no-out\" => {{
                print_all = false;
                argi += 1;
            }},
            \"--print-freq\" => {{
                if argc == argi+1 {{
                    panic!(\"--print-freq requiere the step count\");
                }}
                print_all = false;
                print_freq = args[argi+1].parse::<u64>().unwrap();
                argi += 2;
            }},
            \"-?\"| \"-h\" | \"--help\" =>{{
                print_help();
                return;
            }},
            _ => {{
                if args[argi].starts_with(\"-\"){{
                    panic!(\"unknow option : {{}}\", args[argi]);
                }}
                rom_filenames.push(args[argi].clone());
                argi += 1;
            }}
        }}
    }}

    struct Circuit {{
        step_: u64,

        //roam & registers"
} ; }

macro_rules! DECL_RAM {() =>{"
        ram{} : [u64; {}],"
};}

macro_rules! DECL_REG {() =>{"
        b{}: u64,"
};}

macro_rules! DECL_ROM {() => {"
        rom{} : [u64; {}],"
};}

macro_rules! CONSTRUCT {() =>{"
    }}
    
    impl Circuit{{
        fn default(rom_files: &Vec<String>) -> Circuit{{
            let mut _i = rom_files.iter(); 
            return Circuit {{
                step_: 0,"
};}

macro_rules! DEF_RAM {() => {"
                ram{} : [0; {}],"
};}

macro_rules! DEF_ROM {() => {"
                rom{0} : {{
                    let filename = 
                        match _i.next() {{
                            Some(f) => f.clone(),
                            None => {{
                                print!(\"rom for {2} ? \");
                                let _ = io::stdout().flush();
                                let mut buf = String::new();
                                
                                let stdin = io::stdin();
                                match stdin.read_line(&mut buf) {{
                                    Ok(_) => (),
                                    _ => panic!(\"failed to read input\")
                                }}

                                buf = buf.trim().to_string();
                                buf
                            }}
                        }};
                    let mut f = File::open(&filename).expect(\"failed to read file\");
                    let metadata = fs::metadata(&filename).expect(\"failed to read metadata\");
                    
                    assert_eq!(metadata.len(), 8*{1}, \"{2} : unmatched rom size\");

                    let mut buf = [0; 8*{1}];
                    f.read(&mut buf).expect(\"buffer overflow\");
                    
                    //println!(\"{{:?}}\", buf);
                    let mut ret = [0u64; {1}];
                    for i in 0..{1} {{
                        //little endian
                        ret[i] = ((buf[8*i+7] as u64) << 56) |
                                 ((buf[8*i+6] as u64) << 48) |
                                 ((buf[8*i+5] as u64) << 40) |
                                 ((buf[8*i+4] as u64) << 32) |
                                 ((buf[8*i+3] as u64) << 24) |
                                 ((buf[8*i+2] as u64) << 16) |
                                 ((buf[8*i+1] as u64) << 8 ) |
                                 ((buf[8*i+0] as u64)      );
                    }}
                    //println!(\"{{:?}}\", ret);
                    ret
                }},"
};}

macro_rules! DEF_REG {() => {"
                b{}: 0,"
};}

macro_rules! START_INPUT{() =>{"
            }};
        }}

        fn step<const P: bool>(&mut self){{
            if P {{
                println!(\"step : {{}}\", self.step_);
            }}
            //input"
}; }

macro_rules! INPUT { () => {"//id size name
            let b{} = get_in_bus({}, \"{}\");"
};}

macro_rules! START_CALC { () => {" 
        
            //calculus
"
};}

macro_rules! VAR { () =>{ "b{}" } ; }
macro_rules! MEM_VAR { () =>{ "self.b{}" } ; }

macro_rules! IDENTITY { () =>{"
            let b{} = {};"
};}

//basic op

macro_rules! NOT { () => {"
            let b{} = !{} & {};"
};}

macro_rules! AND { () => {" 
            let b{} = {}&{};"
};} 

macro_rules! NAND { () => {"
            let b{} = !({}&{}) & {};"
} ;}

macro_rules! OR { () => {"
            let b{} = {}|{};"
} ;}

macro_rules! NOR{ () =>{"
            let b{} = !({}|{}) & {};"
} ;}

macro_rules! XOR{ () =>{"
            let b{} = {}^{};"
} ;}

macro_rules! XNOR{ () => {"
            let b{} = !({}^{}) & {};"
} ; }

macro_rules! MUX{ () =>{" 
            let b{} = if {}==0 {{ {} }} else {{ {} }};"
};}

//bus

//a, taille b, b
macro_rules! CONCAT{ () =>{"
            let b{} = ({} << {}) | {};"
};}

//b, taille b - i2-1, mask(i2 - i1 + 1)
macro_rules! SLICE{ () =>{"
            let b{} = ({} >> {}) & {};"
};}

//b, taille b - i-1
macro_rules! SELECT{ () =>{"
            let b{} = ({} >> {}) & 1u64;"
};}

//memory op
macro_rules! REG{ () =>{"
            let r{} = {}; //REG"
};}

//read addr, mask(word size)
macro_rules! ROM{ () =>{"
            let b{0} = self.rom{0}[{1} as usize] & {2};"
};}

//read addr, mask(word size)
macro_rules! READ_RAM{ () =>{"
            let b{0} = self.ram{0}[{1} as usize] & {2};" 
};}

macro_rules! WRITE_BEG{ () =>{"
        
            // write in ram" 
};}

//write enabale, write addr, write val
macro_rules! WRITE_RAM{ () =>{"
            if {1}==1 {{
                self.ram{0}[{2} as usize] = {3};
            }}"
};}

macro_rules! BEG_FLUSH{ () => {"
   
            // flush registers"
}; }

macro_rules! FLUSH{ () =>{"
            self.b{0} = r{0};"
};}

//output :
macro_rules! DEB_P{ () =>{"
       
            //output
            if P {{"
};}

//name, size, id
macro_rules! OUTPUT{ () =>{"
                println!(\"-> {0}: {{:0>{1}b}}\", {2});   "
};}

macro_rules! SUFFIXE{ () =>{"
            }}
            self.step_ += 1;
        }}

    }}

    let mut circuit = Circuit::default(&rom_filenames);
    
    if print_all {{
        while circuit.step_ < step_number{{
            circuit.step::<true>();
        }}
    }} else if print_freq != 0{{
        while circuit.step_ < step_number{{
            let count_before_print = cmp::min(print_freq, step_number - circuit.step_) - 1;

            for _ in 0..count_before_print {{
                circuit.step::<false>();
            }}
            circuit.step::<true>();
        }}
    }} else {{
        let max_st = step_number - 1;
        for _ in 0..max_st {{
            circuit.step::<false>();
        }}
        circuit.step::<true>();
    }}
}}
"};}

fn mask(size : usize) -> u64{{
    ((1u128 << size) - 1) as u64
}}

fn arg_cpp(arg: &Arg, prgm: &ProgramT) -> String{
    match arg{
        Arg::Var(idx) => 
            if prgm.reg_var.contains(&idx) {
                format!(MEM_VAR!(), idx)
            } else {
                format!(VAR!(), idx)
            },
        Arg::Constant((val, _)) => format!("{}", val),
    }
}


fn arg_size(arg: &Arg, vars: &Vec<VarT>) -> usize{
    match arg{
        Arg::Var(idx) => return vars[*idx].size,
        Arg::Constant((_,s)) => return *s
    }
}


pub fn make_rs<W: Write>(prgm: &ProgramT, dest : &mut W) -> Result<()>{
    use Op::*;

    write!(dest, PREFIX!())?;

    let mut roms = Vec::new();
    let mut rams = Vec::new();

    for i in 0..prgm.equations.len(){
        let idx = prgm.equations[i].0;
        match &prgm.equations[i].1{
            Ram(addrs,_,_,we,raddr,rval) =>{
                rams.push((idx, addrs, we, raddr, rval));
            },
            Rom(addrs, _, _) =>{
                roms.push((idx, addrs));
            },
            _ => (),
        }
    }

    for regi in prgm.reg_var.iter(){
        write!(dest, DECL_REG!(), regi)?;
    }
    for (romi, addrs) in roms.iter(){
        write!(dest, DECL_ROM!(), romi, 1<<*addrs)?;
    }
    for (rami, addrs, _,_,_) in rams.iter(){
        write!(dest, DECL_RAM!(), rami, 1<<**addrs)?;
    }

    write!(dest, CONSTRUCT!())?;

    for regi in prgm.reg_var.iter(){
        write!(dest, DEF_REG!(), regi)?;
    }
    for (romi, addrs) in roms.iter(){
        write!(dest, DEF_ROM!(), romi, 1<<*addrs, prgm.vars[*romi].name)?;
    }
    for (rami, addrs, _,_,_) in rams.iter(){
        write!(dest, DEF_RAM!(), rami, 1<<**addrs)?;
    }

    write!(dest, START_INPUT!())?;
    
    for idx in &prgm.inputs{
        write!(dest, INPUT!(), *idx, prgm.vars[*idx].size, prgm.vars[*idx].name)?;
    }

    write!(dest, START_CALC!())?;

    for i in 0..prgm.equations.len(){
        let idx = prgm.equations[i].0;
        match &prgm.equations[i].1{
            Identity(arg) => { 
                write!(dest, IDENTITY!(), idx, arg_cpp(arg, prgm))?;
            },
            
            //basic op
            Not(arg) => {
                write!(dest, NOT!(), idx, arg_cpp(arg, prgm), mask(arg_size(arg, &prgm.vars)))?;
            },
            And(arg1, arg2) => {
                write!(dest, AND!(), idx, arg_cpp(arg1, prgm), arg_cpp(arg2, prgm))?;
            },
            Nand(arg1, arg2) => {
                writeln!(dest, NAND!(), idx, arg_cpp(arg1, prgm), arg_cpp(arg2, prgm),
                                        mask(arg_size(arg1, &prgm.vars)))?;
            },
            Or(arg1, arg2) => {
                write!(dest, OR!(), idx, arg_cpp(arg1, prgm), arg_cpp(arg2, prgm))?;
            },
            Nor(arg1, arg2) => {
                write!(dest, NOR!(), idx, arg_cpp(arg1, prgm), arg_cpp(arg2, prgm),
                                     mask(arg_size(arg1, &prgm.vars)))?;
            },
            Xor(arg1, arg2) => {
                write!(dest, XOR!(), idx, arg_cpp(arg1, prgm), arg_cpp(arg2, prgm))?;
            },
            Xnor(arg1, arg2) => {
                write!(dest, XNOR!(), idx, arg_cpp(arg1, prgm), arg_cpp(arg2, prgm),
                                      mask(arg_size(arg1, &prgm.vars)))?;
            },
            Mux(arg1, arg2, arg3) => { 
                write!(dest, MUX!(), idx, arg_cpp(arg1, prgm), arg_cpp(arg2, prgm), 
                                     arg_cpp(arg3, prgm))?;
            },

            //bus
            Concat(arg1, arg2) => {
                write!(dest, CONCAT!(), idx, arg_cpp(arg1, prgm), arg_size(arg2, &prgm.vars), 
                                        arg_cpp(arg2, prgm))?;
            },
            
            Slice(i1, i2, arg) => {
                write!(dest, SLICE!(), idx, arg_cpp(arg, prgm),
                                       arg_size(arg, &prgm.vars) - (*i2+1) as usize, 
                                       mask((*i2 - *i1 + 1) as usize))?;
            },
            
            Select(i, arg) => {
                write!(dest, SELECT!(), idx, arg_cpp(arg, prgm),
                                        arg_size(arg, &prgm.vars) - (*i+1) as usize)?;
            },
            
            //memory
            Reg(bus) => {
                write!(dest, REG!(), idx, arg_cpp(&Arg::Var(*bus), prgm))?;
            },
            Rom(_, words, addr) => {
                write!(dest, ROM!(), idx, arg_cpp(addr, prgm), 
                                     mask(*words as usize))?;
            },
            Ram(_, words, raddr, _,_,_) =>{
                write!(dest, READ_RAM!(), idx, arg_cpp(raddr, prgm), 
                                          mask(*words as usize))?;
            },
        }
    }

    write!(dest, WRITE_BEG!())?;

    for (rami, _, we, waddr, wval) in rams.iter(){
        write!(dest, WRITE_RAM!(), rami, arg_cpp(we, prgm),
                                   arg_cpp(waddr, prgm), arg_cpp(wval, prgm))?;
    }

    write!(dest, BEG_FLUSH!())?;
    
    for regi in prgm.reg_var.iter(){
        write!(dest, FLUSH!(), regi)?;
    }

    
    write!(dest, DEB_P!())?;

    for idx in &prgm.outputs{
        write!(dest, OUTPUT!(), prgm.vars[*idx].name, prgm.vars[*idx].size, 
                                arg_cpp(&Arg::Var(*idx), prgm))?; 
    }

    write!(dest, SUFFIXE!())?;

    Ok(())
}

