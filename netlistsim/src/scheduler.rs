use crate::netparser::*;

#[derive(Clone)]
enum Mark{
    NotVisited,
    InProgress,
    Visited
}

type Node = (Mark, Vec<usize>);

struct Graph {
    nodes: Vec<Node>
}

impl Graph{
    fn add_link(&mut self, n1: usize, n2: usize){
        (self.nodes[n1].1).push(n2);
    }

    fn clear_mark(&mut self){
        for i in 0..self.nodes.len() {
            self.nodes[i].0 = Mark::NotVisited;
        }
    }
}

fn topological(g : &mut Graph)-> Vec<usize>{
    use Mark::*;

    let mut topo = Vec::<usize>::new();

    fn dfs(n: usize, g: &mut Graph, topo: &mut Vec<usize>){
        match g.nodes[n].0{
            Visited => (),
            InProgress => panic!("cycle !"),
            NotVisited => {
                g.nodes[n].0 = InProgress;
                for i in 0..(g.nodes[n].1).len() {
                    dfs(g.nodes[n].1[i], g, topo);
                }
                g.nodes[n].0 = Visited;
                topo.push(n);
            }
        }
    }

    for i in 0..g.nodes.len() {
        dfs(i,g,&mut topo);
    }
    g.clear_mark();
    topo.reverse();
    return topo;
}

fn add_dep_arg(var: usize, arg: &Arg, g: &mut Graph, reg: &Vec<Ident>){
    use Arg::*;

    if let Var(varg) = arg{
        if !reg.contains(varg){
            g.add_link(var, *varg);
        } else {
        }
    }
}

fn add_dependency(var: usize, op: &Op, g: &mut Graph, mem: &Vec<Ident>){
    use Op::*;

    match &op{
        Identity(arg) => add_dep_arg(var, arg, g, mem),
        
        //basic op
        Not(arg) => add_dep_arg(var, arg, g, mem),
        And(arg1, arg2) =>{
            add_dep_arg(var, arg1, g, mem);
            add_dep_arg(var, arg2, g, mem);
        },
        Nand(arg1, arg2) =>{
            add_dep_arg(var, arg1, g, mem);
            add_dep_arg(var, arg2, g, mem);
        },
        Or(arg1, arg2) =>{
            add_dep_arg(var, arg1, g, mem);
            add_dep_arg(var, arg2, g, mem);
        },
        Nor(arg1, arg2) =>{
            add_dep_arg(var, arg1, g, mem);
            add_dep_arg(var, arg2, g, mem);
        },
        Xor(arg1, arg2) =>{
            add_dep_arg(var, arg1, g, mem);
            add_dep_arg(var, arg2, g, mem);
        },
        Xnor(arg1, arg2) =>{
            add_dep_arg(var, arg1, g, mem);
            add_dep_arg(var, arg2, g, mem);
        },
        Mux(arg1, arg2, arg3) =>{
            add_dep_arg(var, arg1, g, mem);
            add_dep_arg(var, arg2, g, mem);
            add_dep_arg(var, arg3, g, mem);
        },

        //bus
        Concat(arg1, arg2) =>{
            add_dep_arg(var, arg1, g, mem);
            add_dep_arg(var, arg2, g, mem);
        },
        Slice(_,_,arg) => add_dep_arg(var, arg, g, mem),
        Select(_,arg) => add_dep_arg(var, arg, g, mem),

        //memory
        Reg(arg) => if !mem.contains(arg){
            g.add_link(var, *arg);
        },
        Rom(_,_,arg) => add_dep_arg(var, arg, g, mem),
        Ram(_,_,arg,_,_,_) =>{
            add_dep_arg(var, arg, g, mem);
        },
    }
}

pub fn schedule(prgm: &mut ProgramT){
    use Mark::*;

    let mut eqs = Vec::<Option<Op> >::new();
    let mut g = Graph {nodes: Vec::<Node>::new()};

    g.nodes.resize(prgm.vars.len(), (NotVisited, Vec::<usize>::new()));
    eqs.resize(prgm.vars.len(), None);

    while let Some((var, op)) = prgm.equations.pop() {
        if let Some(_) = eqs[var]{
            panic!("2 equations for same bus");
        }
        add_dependency(var, &op, &mut g, &prgm.reg_var);
        eqs[var] = Some(op);
    }

    let mut order = topological(&mut g);

    while let Some(i) = order.pop(){
        if let Some(eq) = &eqs[i] {
            prgm.equations.push((i,*eq));
        }
    }
}
