# Project
`netlistsim` is a netlist compiler : it will not simulate the circuit but will produce rust code which will simulate the circuit. This code could be piped into `rustc` depending on arguments.

# Requirement

Compilation of `netlistsim` requires `cargo` (rust), and the execution requires `rustc`.

# Compilation

To compile `netlistsim`, just execute `make` or `make netlistsim`.

# Usage

## `netlistsim`

Usage : `netlistsim <netlist_file> [-s <source_output>] [[--lto] -o <output_file>]`  

Compile the `netlist_file` input into rust code and eventually compile it. 

* `-s` : specify a file for the compiled code.
* `-o` : specify a file for the executable file.
* `--lto` : tells rustc to activate lto (time expensive).
* `-h`, `-?`, `--help` : print this and stop.

If `-s` and `-o` are not specified, the code will be printed in console.

Note : not giving any arguments will print this but with an error code.

## Produced code

Let `circuit` be the executable file.

Usage : `circuit [rom_file0 [rom_file1 [ ... ]]] [-n <step_number>] [--no-out | --print-freq <freq>]`

Will simulate the circuit, with the `rom_file`s specified.  

* `-n` : specify the number of steps to simulate (will not stop if not specified).  
* `--no-out` : will print only the last output.  
* `--print-freq` : will print the output every `freq` steps (every step if not specified).  
* `-h`, `-?`, `--help` : print this and stop. 

Note : extra `rom_file`s will be ignored, and if the circuit requires more `rom_file`s than given, it will ask them with the name of the variable reading ROM.

# Behavior

As said above, `netlistsim` compile a netlist into rust code that will simulate this netlist.    
Only the netlist is given to `netlistsim` wich means that changing parameters like ROMs don't require to recompile it.    
The keywords `INPUT`, `OUTPUT`, `VAR`, `IN` and all operators are reserved in uppercase.   
Variable names are pretty classic : composed of ascii alphanumeric characters and `_`, not starting with a number.  
Currently, bus of size 65 or more are not supported.  
ROM file are expected to be binary file, each entry of the rom take 64 bits, stored in little endian.

Compilation Errors are still to be improved : they currently use `panic!` and debugging system of rust, which cause compilation errors to look like :   

    thread 'main' panicked at '(7, 1) : unknown identifier c', src/netparser.rs:570:38
    note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace

(the couple `(7, 1)` means the error is line 7, character 1).   
or for a syntax error :  

    thread 'main' panicked at '(2, 8) : expected identifier, got Top(Treg): ()', src/netparser.rs:495:36 
    note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace

## About what is not in specifications 

Logic gates `NOR`, `XOR` and `XNOR` are supported.  
All the logic gate `NOT`, `AND`, `NAND`, `OR`, `NOR`, `XOR` and `XNOR` are bitwise if given buses have coherent sizes.  
`MUX` can be applied to bus, but will choose the entire bus (chooser bus still of size 1).  
There is a ram per `RAM` operation, reading is instant but writting is done at the end of cycle.  
Registers store a temporary bus, wich is 'flushed' into the register after ram writing and before output print.

# Encountered Difficulities

I did not encountered many difficulties, most of them was giving sens to operations that have unclear specifications.

