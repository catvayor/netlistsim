open Ast

exception Redef_label of string
exception Not_def_label of string
exception Text_in_data_segment
exception Data_in_text_segment

let print_bit fmt (i, x) =
	Format.fprintf fmt "%d" (((if x > 0 then x else (1 lsl 32)+x) lsr i) mod 2)

let print_bits fmt (first, last, x) =
	for i = first to last do
		print_bit fmt (i, x)
	done

let print_reg fmt reg =
	print_bits fmt (0,4,reg)

let print_comp fmt comp = Format.fprintf fmt
	(match comp with
	| Eq  -> "000"
	| Ne  -> "100"
	| Lt  -> "001"
	| Ge  -> "101"
	| Ltu -> "011"
	| Geu -> "111"
	)

let print_op fmt op = Format.fprintf fmt
	(match op with
	| Add | Sub -> "000"
	| Sll       -> "100"
	| Slt       -> "010"
	| Sltu      -> "110"
	| Xor       -> "001"
	| Srl | Sra -> "101"
	| Or        -> "011"
	| And       -> "111"
	)

let print_size fmt size = Format.fprintf fmt
	(match size with
	| Byte -> "00"
	| Half -> "10"
	| Word -> "01"
	)

let compile_imm lbls = function
    | LabHi(lbl,ofs) -> (Hashtbl.find lbls lbl + ofs) lsr 12
    | LabLo(lbl,ofs) -> let x = Hashtbl.find lbls lbl + ofs in x - (x lsr 12)
    | Const(i) -> i

let rec compile_instr lbls fmt (no, instr) = match instr with
	| Lui (rd, imm) ->
        let imm = compile_imm lbls imm in
		Format.fprintf fmt "1110110%a%a"
		print_reg  rd
		print_bits (0,19,imm)
	| Auipc (rd, imm) ->
        let imm = compile_imm lbls imm in
		Format.fprintf fmt "1110100%a%a"
		print_reg  rd
		print_bits (0,19,imm)
	| Jal (rd, imm) ->
		(* 0 in the middle : RISC-V supports 16-bits aligned jumps,
		we only support 32-bits aligned ones *)
		Format.fprintf fmt "1111011%a%a%a0%a%a"
		print_reg  rd
		print_bits (10,17,imm)
		print_bit  ( 9,   imm)
		print_bits ( 0, 8,imm)
		print_bit  (18,   imm)
	| JalLbl (rd, lbl) ->
		(try compile_instr lbls fmt (no, Jal (rd, Hashtbl.find lbls lbl - no))
		with Not_found -> raise (Not_def_label lbl))
	| Jalr (rd, rs1, imm) ->
		Format.fprintf fmt "1110011%a000%a%a"
		print_reg  rd
		print_reg  rs1
		print_bits ( 0,11,imm*4)
	| Branch (comp, rs1, rs2, imm) ->
		let imm = 2*imm in
		Format.fprintf fmt "1100011%a%a%a%a%a%a%a"
		print_bit  (10,   imm)
		print_bits ( 0, 3,imm)
		print_comp comp
		print_reg  rs1
		print_reg  rs2
		print_bits ( 4, 9,imm)
		print_bit  (11,   imm)
	| BranchLbl (comp, rs1, rs2, lbl) ->
		(try
		  compile_instr lbls fmt
		  	(no, Branch (comp, rs1, rs2, Hashtbl.find lbls lbl - no))
		with Not_found -> raise (Not_def_label lbl))
	| Load (size, sign_ext, rd, rs1, imm) ->
		Format.fprintf fmt "1100000%a%a%d%a%a"
		print_reg  rd
		print_size size
		(if sign_ext then 1 else 0)
		print_reg  rs1
		print_bits ( 0,11,imm)
	| Store (size, rs1, rs2, imm) ->
		Format.fprintf fmt "1100010%a%a0%a%a%a"
		print_bits ( 0, 4,imm)
		print_size size
		print_reg  rs1
		print_reg  rs2
		print_bits ( 5,11,imm)
	| Opimm (Sub, rd, rs1, imm) ->
        let imm = compile_imm lbls imm in
		compile_instr lbls fmt (no, Opimm (Add, rd, rs1, Const(-imm)))
	| Opimm (op, rd, rs1, imm) when op = Sll || op = Srl || op = Sra ->
        let imm = compile_imm lbls imm in
		Format.fprintf fmt "1100100%a%a%a%a00000%d0"
		print_reg  rd
		print_op   op
		print_reg  rs1
		print_bits ( 0, 4,imm)
		(match op with Sra -> 1 | _ -> 0)
	| Opimm (op, rd, rs1, imm) ->
        let imm = compile_imm lbls imm in
		Format.fprintf fmt "1100100%a%a%a%a"
		print_reg  rd
		print_op   op
		print_reg  rs1
		print_bits ( 0,11,imm)
	| Opreg (op, rd, rs1, rs2) ->
		Format.fprintf fmt "1100110%a%a%a%a00000%d0"
		print_reg  rd
		print_op   op
		print_reg  rs1
		print_reg  rs2
		(match op with Sub | Sra -> 1 | _ -> 0)
	| Opio (op, rd, rs1, rs2) ->
		Format.fprintf fmt "1101000%a%a%a%a00000%d0"
		print_reg  rd
		print_op   op
		print_reg  rs1
		print_reg  rs2
		(match op with Sub | Sra -> 1 | _ -> 0)

let compile_file prog ofile =
  let n = ref 0 and lbls = Hashtbl.create 17 in
  let seg_counter = ref 0 and txt = ref true in
  let prog = List.fold_left (fun acc -> function
  	| Label s ->
  		if Hashtbl.mem lbls s then
  			raise (Redef_label s)
  		else
  			Hashtbl.add lbls s (if !txt then !n else !seg_counter);
  		acc
  	| Instr instr ->
        if not !txt then
            raise Text_in_data_segment;
  		incr n;
  		acc @ [!n-1,instr]
    | Spec spec ->
        begin match spec with
              | Text -> txt := true
              | Bss -> txt := false
              | Zero size ->
                if !txt then
                    raise Data_in_text_segment;
                seg_counter := !seg_counter + size
        end; acc
  ) [] prog in
  let f = open_out ofile in
  let fmt = Format.formatter_of_out_channel f in
  Format.fprintf fmt "memory_initialization_radix=2;@.memory_initialization_vector=@.";
  List.iter (function i ->
  	Format.fprintf fmt "%a@." (compile_instr lbls) i) prog;
  Format.fprintf fmt ";@?";
  close_out f
