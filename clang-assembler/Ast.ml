module Smap = Map.Make(String)

type reg = int
type size = Byte | Half | Word

type comp =
	| Eq  | Ne
	| Lt  | Ge
	| Ltu | Geu

type op =
	| Add | Sub
	| Sll | Srl | Sra
	| Slt | Sltu
	| Xor | Or | And

type imm =
    | LabHi of string*int
    | LabLo of string*int
    | Const of int

type instr =
	| Lui of reg * imm
	| Auipc of reg * imm
	| Jal of reg * int
	| JalLbl of reg * string
	| Jalr of reg * reg * int
	| Branch of comp * reg * reg * int
	| BranchLbl of comp * reg * reg * string
	| Load of size * bool * reg * reg * int
	| Store of size * reg * reg * int
	| Opimm of op * reg * reg * imm
	| Opreg of op * reg * reg * reg
	| Opio of op * reg * reg * reg

type specifier =
    | Text
    | Bss
    | Zero of int

type line =
    | Spec of specifier
	| Instr of instr
	| Label of string

type file = line list
