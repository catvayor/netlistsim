
/* Analyseur syntaxique pour Mini-Python */

%{
  open Ast

  let ra = 1 (* ra = x1 *)
%}

%token <int> INT
%token <string> LBL
%token <int> REG
%token LUI AUIPC JAL JALR BEQ BNE BLT BGE BLTU BGEU
%token LB LH LW LBU LHU SB SH SW
%token MOV NOP J BNEZ BEQZ BLEZ BGEZ BLTZ BGTZ RET CALL SEQZ SNEZ NOT NEG
%token PLUS
%token <Ast.op> OP
%token <Ast.op> OPI
%token <Ast.op> OPIO
%token COLON COMMA LPAR RPAR

%token TEXT BSS ZERO HI LO

%token EOF


/* Point d'entrée de la grammaire */
%start file

/* Type des valeurs renvoyées par l'analyseur syntaxique */
%type <Ast.file> file

%%

%inline hi_lo_plus:
| PLUS i=INT {i}
| {0}
;

imm:
| i=INT { Const(i) }
| HI LPAR s=LBL ofs=hi_lo_plus RPAR { LabHi(s,ofs) }
| LO LPAR s=LBL ofs=hi_lo_plus RPAR { LabLo(s,ofs) }
;

file:
| l = list(line) EOF { List.fold_right (@) l [] }
;

line:
| TEXT          { [Spec Text] }
| BSS           { [Spec Bss] }
| ZERO size=INT { [Spec (Zero size)] }
| s=LBL COLON   { [Label s] }
| l=instr       { List.map (fun i -> Instr i) l }
;

%inline instr:
| LUI rd=REG COMMA imm=imm             { [Lui (rd, imm)] }
| AUIPC rd=REG COMMA imm=imm           { [Auipc (rd, imm)] }
| JAL rd=REG COMMA ofs=INT             { [Jal (rd, ofs)] }
| JAL rd=REG COMMA lbl=LBL             { [JalLbl (rd, lbl)]}
| JALR rd=REG COMMA rs1=REG COMMA ofs=INT    { [Jalr (rd, rs1, ofs)] }
| c=comp rs1=REG COMMA rs2=REG COMMA ofs=INT { [Branch (c, rs1, rs2, ofs)] }
| c=comp rs1=REG COMMA rs2=REG COMMA lbl=LBL { [BranchLbl (c, rs1, rs2, lbl)] }
| LB rd=REG COMMA ofs=INT LPAR addr=REG RPAR     { [Load (Byte, false, rd, addr, ofs)] }
| LH rd=REG COMMA ofs=INT LPAR addr=REG RPAR     { [Load (Half, false, rd, addr, ofs)] }
| LW rd=REG COMMA ofs=INT LPAR addr=REG RPAR     { [Load (Word, false, rd, addr, ofs)] }
| LBU rd=REG COMMA ofs=INT LPAR addr=REG RPAR    { [Load (Byte, true, rd, addr, ofs)] }
| LHU rd=REG COMMA ofs=INT LPAR addr=REG RPAR    { [Load (Half, true, rd, addr, ofs)] }
| SB data=REG COMMA ofs=INT LPAR addr=REG RPAR   { [Store (Byte, addr, data, ofs)] }
| SH data=REG COMMA ofs=INT LPAR addr=REG RPAR   { [Store (Half, addr, data, ofs)] }
| SW data=REG COMMA ofs=INT LPAR addr=REG RPAR   { [Store (Word, addr, data, ofs)] }
| o=OPI rd=REG COMMA rs1=REG COMMA imm=imm    { [Opimm (o, rd, rs1, imm)] }
| o=OP rd=REG COMMA rs1=REG COMMA rs2=REG    { [Opreg (o, rd, rs1, rs2)] }
| o=OPIO rd=REG COMMA rs1=REG COMMA rs2=REG  { [Opio (o, rd, rs1, rs2)] }

/* Pseudo-instructions : */
| MOV rd=REG COMMA rs=REG              { [Opreg (Or, rd, rs, 0)] }
| MOV rd=REG COMMA imm=INT             {
  let all_sup = (1 lsl 21) - 1 in

  let imm_sup = imm lsr 12 in
  let imm_inf = imm - (imm_sup lsl 12) in
  let sign_inf = imm_inf lsr 11 in
  let real_imm_sup = imm_sup lxor (all_sup * sign_inf) in

  if real_imm_sup != 0 then
    if imm_inf != 0 then
      [Lui (rd, Const(real_imm_sup)); Opimm (Xor, rd, rd, Const(imm_inf))]
    else
      [Lui (rd, Const(imm_sup))]
  else
    [Opimm (Xor, rd, 0, Const(imm_inf))]
}
| NOP                            { [Opreg (Or, 0, 0, 0)] }
| J imm=INT                      { [Jal (0, imm)] }
| J lbl=LBL                      { [JalLbl (0, lbl)] }
| BNEZ r=REG COMMA lbl=LBL       { [BranchLbl (Ne, r, 0, lbl)] }
| BEQZ r=REG COMMA lbl=LBL       { [BranchLbl (Eq, r, 0, lbl)] }
| BLEZ r=REG COMMA lbl=LBL       { [BranchLbl (Ge, 0, r, lbl)] }
| BGEZ r=REG COMMA lbl=LBL       { [BranchLbl (Ge, r, 0, lbl)] }
| BLTZ r=REG COMMA lbl=LBL       { [BranchLbl (Lt, r, 0, lbl)] }
| BGTZ r=REG COMMA lbl=LBL       { [BranchLbl (Lt, 0, r, lbl)] }
| SEQZ rd=REG COMMA rs1=REG      { [Opimm (Sltu, rd, rs1, Const(1))] }
| NOT rd=REG COMMA rs1=REG       { [Opimm (Xor, rd, rs1, Const(-1))] }
| NEG rd=REG COMMA rs1=REG       { [Opreg (Sub, rd, 0, rs1)] }
| SNEZ rd=REG COMMA rs1=REG      { [Opreg (Sltu, rd, 0, rs1)] }
| CALL lbl=LBL                   { [JalLbl (ra, lbl)] }
| RET                            { [Jalr (0, ra, 0)] }

%inline comp:
| BEQ {Eq}
| BNE {Ne}
| BLT {Lt} | BLTU {Ltu}
| BGE {Ge} | BGEU {Geu}
