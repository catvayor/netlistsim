# Assembler for our RISC-V processor

## Instructions

TODO

## Immediates format

The only supported format is `$x` where `x` is a decimal number. For an upper immediate (`lui` and `auipc`), it must be in the range [-2^19;2^19-1], not a multiple of 2^12. Offsets for jumps are in words (= 4 bytes = 1 instruction).