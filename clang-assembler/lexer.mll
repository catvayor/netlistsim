
(* Analyseur lexical pour Mini-Python *)

{
  open Lexing
  open Ast
  open Parser

  exception Lexing_error of string

  let id_or_kwd =
    let h = Hashtbl.create 32 in
    List.iter (fun (s, tok) -> Hashtbl.add h s tok)
      [
        "lui", LUI; "auipc", AUIPC; "jal", JAL; "jalr", JALR;
        "beq", BEQ; "bne", BNE; "bge", BGE; "bltu", BLTU; "bgeu", BGEU;
        "blt", BLT; "bge", BGE;
        "lb", LB; "lh", LH; "lw", LW; "lbu", LBU; "lhu", LHU;
        "sb", SB; "sh", SH; "sw", SW; "add", OP Add; "sub", OP Sub;
        "sll", OP Sll; "slt", OP Slt; "sltu", OP Sltu; "xor", OP Xor;
        "srl", OP Srl; "sra", OP Sra; "or", OP Or; "and", OP And;
        "addi", OPI Add; "subi", OPI Sub;
        "slli", OPI Sll; "slti", OPI Slt; "sltui", OPI Sltu; "xori", OPI Xor;
        "srli", OPI Srl; "srai", OPI Sra; "ori", OPI Or; "andi", OPI And;
        "sllio", OPIO Sll; "xorio", OPIO Xor; "srlio", OPIO Srl;
        "sraio", OPIO Sra; "orio", OPIO Or; "andio", OPIO And;

        "mv", MOV; "nop", NOP; "j", J; "bnez", BNEZ; "beqz", BEQZ; "blez", BLEZ; "bgez", BGEZ; "bltz", BLTZ; "bgtz", BGTZ; "ret", RET;
        "call",CALL; "seqz",SEQZ; "snez",SNEZ; "not",NOT; "neg", NEG;

        ".text", TEXT; ".bss", BSS; ".zero", ZERO;

        "x0",REG 0;"x1",REG 1;"x2",REG 2;"x3",REG 3;"x4",REG 4;"x5",REG 5;"x6",REG 6;
        "x7",REG 7;"x8",REG 8;"x9",REG 9;"x10",REG 10;"x11",REG 11;"x12",REG 12;
        "x13",REG 13;"x14",REG 14;"x15",REG 15;"x16",REG 16;"x17",REG 17;"x18",REG 18;
        "x19",REG 19;"x20",REG 20;"x21",REG 21;"x22",REG 22;"x23",REG 23;"x24",REG 24;
        "x25",REG 25;"x26",REG 26;"x27",REG 27;"x28",REG 28;"x29",REG 29;"x30",REG 30;
        "x31",REG 31;

        "zero" , REG 0; "ra", REG 1; "sp", REG 2; "gp", REG 3; "tp", REG 4;
        "t0", REG 5; "t1", REG 6; "t2", REG 7; "fp",REG 8; "s0",REG 8;
        "s1", REG 9;"a0", REG 10;"a1", REG 11;"a2", REG 12;"a3", REG 13;"a4", REG 14;
        "a5", REG 15;"a6",REG 16;"a7",REG 17;"s2",REG 18;"s3",REG 19;"s4",REG 20;
        "s5",REG 21;"s6",REG 22;"s7",REG 23;"s8",REG 24;"s9",REG 25;"s10",REG 26;
        "s11",REG 27;"t3",REG 28;"t4",REG 29;"t5",REG 30;"t6",REG 31;
      ];
   fun s -> try Hashtbl.find h s with Not_found -> LBL s
}

let letter = ['a'-'z' 'A'-'Z']
let digit = ['0'-'9']
let ident = (letter|'.') (letter | digit | '_')*
let integer = '-'? ['0'-'9']+
let space = ' ' | '\t'
let comment = ("#"|".attribute"|".file"|".globl"|".p2align"|".size"|".type"|".ident"|".section"|".addrsig"|".addrsig_sym") [^'\n']*

rule next_token = parse
  | '\n'    { new_line lexbuf; next_token lexbuf }
  | (space | comment)+
            { next_token lexbuf }
  | ident as id { id_or_kwd id }
  | ':'     { COLON }
  | ','     { COMMA }
  | '('     { LPAR }
  | ')'     { RPAR }
  | '+'     { PLUS }
  | "%hi"   { HI }
  | "%lo"   { LO }
  | integer as s
            { try INT (int_of_string s)
              with _ -> raise (Lexing_error ("constant too large: " ^ s)) }
  | eof     { EOF }
  | _ as c  { raise (Lexing_error ("illegal character: " ^ String.make 1 c)) }

{



}
