nop

mv sp, 239996
j main

xorio_:
    xorio t0, a1, zero
	sw	t0, 0(a0)
	ret
	.text
	.attribute	4, 16
	.attribute	5, "rv32i2p0"
	.file	"clock.c"
	.globl	month_length                    # -- Begin function month_length
	.p2align	2
	.type	month_length,@function
month_length:                           # @month_length
# %bb.0:
	addi	a5, zero, 1
	bne	a0, a5, .LBB0_5
# %bb.1:
	slli	a0, a2, 1
	add	a0, a0, a1
	andi	a0, a0, 3
	seqz	a0, a0
	or	a1, a2, a1
	snez	a2, a1
	and	a2, a2, a0
	addi	a0, zero, 29
	bnez	a2, .LBB0_6
# %bb.2:
	seqz	a1, a1
	beqz	a1, .LBB0_4
# %bb.3:
	slli	a1, a4, 1
	add	a1, a1, a3
	andi	a1, a1, 3
	beqz	a1, .LBB0_6
.LBB0_4:
	addi	a0, zero, 28
	ret
.LBB0_5:
	slti	a1, a0, 7
	srli	a2, a0, 31
	add	a2, a0, a2
	andi	a2, a2, -2
	sub	a0, a0, a2
	xor	a0, a0, a1
	seqz	a0, a0
	addi	a1, zero, 31
	sub	a0, a1, a0
.LBB0_6:
	ret
.Lfunc_end0:
	.size	month_length, .Lfunc_end0-month_length
                                        # -- End function
	.globl	color_octupler                  # -- Begin function color_octupler
	.p2align	2
	.type	color_octupler,@function
color_octupler:                         # @color_octupler
# %bb.0:
	addi	sp, sp, -16
	sw	a0, 12(sp)
	lw	a0, 12(sp)
	lw	a1, 12(sp)
	lw	a2, 12(sp)
	slli	a1, a1, 4
	add	a0, a1, a0
	lw	a1, 12(sp)
	slli	a2, a2, 8
	add	a0, a0, a2
	lw	a2, 12(sp)
	slli	a1, a1, 12
	add	a0, a0, a1
	lw	a1, 12(sp)
	slli	a2, a2, 16
	add	a0, a0, a2
	lw	a2, 12(sp)
	slli	a1, a1, 20
	lw	a3, 12(sp)
	add	a0, a0, a1
	slli	a1, a2, 24
	add	a0, a0, a1
	slli	a1, a3, 28
	add	a0, a0, a1
	addi	sp, sp, 16
	ret
.Lfunc_end1:
	.size	color_octupler, .Lfunc_end1-color_octupler
                                        # -- End function
	.globl	display_char                    # -- Begin function display_char
	.p2align	2
	.type	display_char,@function
display_char:                           # @display_char
# %bb.0:
	addi	sp, sp, -16
	mv	a7, zero
	sw	a2, 12(sp)
	sw	a3, 8(sp)
	addi	a2, a4, -32
	andi	a2, a2, 255
	slli	a2, a2, 5
	add	a0, a0, a2
	lui	a2, %hi(ram)
	addi	t1, a2, %lo(ram)
	addi	t0, zero, 16
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	lhu	a4, 0(a0)
	andi	a3, a4, 255
	slli	a3, a3, 2
	add	a3, a1, a3
	lw	a3, 0(a3)
	and	t3, a3, a5
	not	a3, a3
	lw	t2, 12(sp)
	lw	a2, 8(sp)
	and	a3, a3, a6
	or	t3, t3, a3
	lw	a3, 8(sp)
	add	a2, a7, a2
	slli	a2, a2, 4
	add	a2, a2, t2
	add	a3, a7, a3
	slli	a3, a3, 6
	add	a2, a2, a3
	slli	a2, a2, 2
	add	a2, a2, t1
	sw	t3, 0(a2)
	srli	a2, a4, 6
	andi	a2, a2, 1020
	add	a2, a1, a2
	lw	a2, 0(a2)
	and	a3, a2, a5
	not	a2, a2
	lw	t2, 12(sp)
	lw	a4, 8(sp)
	and	a2, a2, a6
	or	a2, a3, a2
	lw	a3, 8(sp)
	add	a4, a7, a4
	slli	a4, a4, 4
	add	a4, t2, a4
	add	a3, a7, a3
	slli	a3, a3, 6
	add	a3, a4, a3
	slli	a3, a3, 2
	add	a3, t1, a3
	sw	a2, 4(a3)
	addi	a7, a7, 1
	addi	a0, a0, 2
	bne	a7, t0, .LBB2_1
# %bb.2:
	addi	sp, sp, 16
	ret
.Lfunc_end2:
	.size	display_char, .Lfunc_end2-display_char
                                        # -- End function
	.globl	display_text                    # -- Begin function display_text
	.p2align	2
	.type	display_text,@function
display_text:                           # @display_text
# %bb.0:
	addi	sp, sp, -32
	sw	ra, 28(sp)
	sw	s0, 24(sp)
	sw	s1, 20(sp)
	sw	s2, 16(sp)
	sw	s3, 12(sp)
	sw	s4, 8(sp)
	sw	s5, 4(sp)
	sw	s6, 0(sp)
	lbu	a7, 0(a4)
	beqz	a7, .LBB3_3
# %bb.1:
	mv	s2, a6
	mv	s3, a5
	mv	s4, a3
	mv	s1, a2
	mv	s5, a1
	mv	s6, a0
	addi	s0, a4, 1
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	andi	a4, a7, 255
	mv	a0, s6
	mv	a1, s5
	mv	a2, s1
	mv	a3, s4
	mv	a5, s3
	mv	a6, s2
	call	display_char
	lbu	a7, 0(s0)
	addi	s1, s1, 2
	addi	s0, s0, 1
	bnez	a7, .LBB3_2
.LBB3_3:
	lw	s6, 0(sp)
	lw	s5, 4(sp)
	lw	s4, 8(sp)
	lw	s3, 12(sp)
	lw	s2, 16(sp)
	lw	s1, 20(sp)
	lw	s0, 24(sp)
	lw	ra, 28(sp)
	addi	sp, sp, 32
	ret
.Lfunc_end3:
	.size	display_text, .Lfunc_end3-display_text
                                        # -- End function
	.globl	fill_rect                       # -- Begin function fill_rect
	.p2align	2
	.type	fill_rect,@function
fill_rect:                              # @fill_rect
# %bb.0:
	addi	sp, sp, -32
	sw	a0, 28(sp)
	sw	a1, 24(sp)
	sw	a2, 20(sp)
	sw	a3, 16(sp)
	sw	a4, 12(sp)
	lw	a0, 24(sp)
	sw	a0, 8(sp)
	lw	a0, 8(sp)
	lw	a1, 16(sp)
	bge	a0, a1, .LBB4_6
# %bb.1:
	lui	a0, %hi(ram)
	addi	a0, a0, %lo(ram)
	j	.LBB4_3
.LBB4_2:                                #   in Loop: Header=BB4_3 Depth=1
	lw	a1, 8(sp)
	addi	a1, a1, 1
	sw	a1, 8(sp)
	lw	a1, 8(sp)
	lw	a2, 16(sp)
	bge	a1, a2, .LBB4_6
.LBB4_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_4 Depth 2
	lw	a1, 28(sp)
.LBB4_4:                                #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	sw	a1, 4(sp)
	lw	a1, 4(sp)
	lw	a2, 20(sp)
	bge	a1, a2, .LBB4_2
# %bb.5:                                #   in Loop: Header=BB4_4 Depth=2
	lw	a1, 12(sp)
	lw	a2, 4(sp)
	lw	a3, 8(sp)
	lw	a4, 8(sp)
	slli	a3, a3, 4
	add	a2, a3, a2
	slli	a3, a4, 6
	add	a2, a2, a3
	slli	a2, a2, 2
	add	a2, a2, a0
	sw	a1, 0(a2)
	lw	a1, 4(sp)
	addi	a1, a1, 1
	j	.LBB4_4
.LBB4_6:
	addi	sp, sp, 32
	ret
.Lfunc_end4:
	.size	fill_rect, .Lfunc_end4-fill_rect
                                        # -- End function
	.globl	main                            # -- Begin function main
	.p2align	2
	.type	main,@function
main:                                   # @main
# %bb.0:
	addi	sp, sp, -2032
	sw	ra, 2028(sp)
	sw	s0, 2024(sp)
	sw	s1, 2020(sp)
	sw	s2, 2016(sp)
	sw	s3, 2012(sp)
	sw	s4, 2008(sp)
	sw	s5, 2004(sp)
	sw	s6, 2000(sp)
	sw	s7, 1996(sp)
	sw	s8, 1992(sp)
	sw	s9, 1988(sp)
	sw	s10, 1984(sp)
	sw	s11, 1980(sp)
	lui	a0, 1
	addi	a0, a0, -1904
	sub	sp, sp, a0
	addi	a0, sp, 1128
	addi	s0, a0, 1024
	lui	a0, 985088
	addi	a1, a0, 128
	lui	a0, 1
	addi	a0, a0, 72
	add	a0, sp, a0
	mv	s1, a0
	mv	a0, s1
	call	xorio_
	lui	a0, 990216
	addi	a1, a0, 64
	mv	a0, s1
	call	xorio_
	lui	a0, 994308
	addi	a1, a0, 128
	mv	a0, s1
	call	xorio_
	lui	a0, 997384
	addi	a1, a0, 64
	mv	a0, s1
	call	xorio_
	lui	a0, 1002504
	addi	a1, a0, 128
	mv	a0, s1
	call	xorio_
	lui	a0, 1006596
	addi	a1, a0, 64
	mv	a0, s1
	call	xorio_
	lui	a0, 1009668
	addi	a1, a0, 64
	mv	a0, s1
	call	xorio_
	lui	a0, 1013768
	addi	a1, a0, 128
	mv	a0, s1
	call	xorio_
	lui	a0, 1017860
	addi	a1, a0, 128
	mv	a0, s1
	call	xorio_
	lui	a1, 1022988
	mv	a0, s1
	call	xorio_
	lui	a0, 1026048
	addi	a1, a0, 64
	mv	a0, s1
	call	xorio_
	lui	a0, 1029124
	addi	a1, a0, 64
	mv	a0, s1
	call	xorio_
	lui	a0, 1033216
	addi	a1, a0, 64
	mv	a0, s1
	call	xorio_
	lui	a1, 1037312
	mv	a0, s1
	call	xorio_
	lui	a1, 1042436
	mv	a0, s1
	call	xorio_
	lui	a1, 1045508
	mv	a0, s1
	call	xorio_
	addi	a0, zero, 9
	call	color_octupler
	mv	s4, a0
	mv	a0, zero
	call	color_octupler
	sh	zero, 1128(sp)
	sh	zero, 1130(sp)
	sh	zero, 1132(sp)
	sh	zero, 1134(sp)
	sh	zero, 1136(sp)
	sh	zero, 1138(sp)
	sh	zero, 1140(sp)
	sh	zero, 1142(sp)
	sh	zero, 1144(sp)
	sh	zero, 1146(sp)
	sh	zero, 1148(sp)
	sh	zero, 1150(sp)
	sh	zero, 1152(sp)
	sh	zero, 1154(sp)
	sh	zero, 1156(sp)
	sh	zero, 1158(sp)
	sh	zero, 1160(sp)
	sh	zero, 1162(sp)
	addi	s1, zero, 48
	sh	s1, 1164(sp)
	sh	s1, 1166(sp)
	sh	s1, 1168(sp)
	addi	a1, zero, 16
	sh	a1, 1170(sp)
	sh	a1, 1172(sp)
	sh	a1, 1174(sp)
	sh	a1, 1176(sp)
	sh	zero, 1178(sp)
	sh	zero, 1180(sp)
	sh	s1, 1182(sp)
	sh	s1, 1184(sp)
	sh	zero, 1186(sp)
	sh	zero, 1188(sp)
	sh	zero, 1190(sp)
	sh	zero, 1192(sp)
	addi	a2, zero, 76
	sh	a2, 1194(sp)
	sh	a2, 1196(sp)
	sh	a2, 1198(sp)
	sh	a2, 1200(sp)
	sh	a2, 1202(sp)
	addi	a6, zero, 76
	sh	zero, 1204(sp)
	sh	zero, 1206(sp)
	sh	zero, 1208(sp)
	sh	zero, 1210(sp)
	sh	zero, 1212(sp)
	sh	zero, 1214(sp)
	sh	zero, 1216(sp)
	sh	zero, 1218(sp)
	sh	zero, 1220(sp)
	sh	zero, 1222(sp)
	sh	zero, 1224(sp)
	sh	zero, 1226(sp)
	addi	a2, zero, 136
	sh	a2, 1228(sp)
	sh	a2, 1230(sp)
	sh	a2, 1232(sp)
	addi	a7, zero, 510
	sh	a7, 1234(sp)
	addi	a2, zero, 72
	sh	a2, 1236(sp)
	addi	t1, zero, 72
	addi	a2, zero, 68
	sh	a2, 1238(sp)
	sh	a2, 1240(sp)
	addi	a4, zero, 68
	addi	a2, zero, 511
	sh	a2, 1242(sp)
	addi	s10, zero, 4
	sh	s10, 1244(sp)
	addi	a2, zero, 36
	sh	a2, 1246(sp)
	addi	a2, zero, 34
	sh	a2, 1248(sp)
	sh	zero, 1250(sp)
	sh	zero, 1252(sp)
	sh	zero, 1254(sp)
	sh	zero, 1256(sp)
	sh	a1, 1258(sp)
	addi	a2, zero, 124
	sh	a2, 1260(sp)
	addi	a5, zero, 124
	addi	a2, zero, 150
	sh	a2, 1262(sp)
	addi	a2, zero, 402
	sh	a2, 1264(sp)
	addi	a2, zero, 18
	sh	a2, 1266(sp)
	addi	a3, zero, 20
	sh	a3, 1268(sp)
	addi	s2, zero, 120
	sh	s2, 1270(sp)
	addi	a2, zero, 240
	sh	a2, 1272(sp)
	addi	a2, zero, 400
	sh	a2, 1274(sp)
	addi	a2, zero, 274
	sh	a2, 1276(sp)
	addi	a2, zero, 406
	sh	a2, 1278(sp)
	sh	a5, 1280(sp)
	sh	a1, 1282(sp)
	sh	zero, 1284(sp)
	sh	zero, 1286(sp)
	sh	zero, 1288(sp)
	sh	zero, 1290(sp)
	addi	a2, zero, 270
	sh	a2, 1292(sp)
	addi	a2, zero, 137
	sh	a2, 1294(sp)
	addi	a2, zero, 73
	sh	a2, 1296(sp)
	sh	a2, 1298(sp)
	addi	a2, zero, 46
	sh	a2, 1300(sp)
	sh	a1, 1302(sp)
	addi	a2, zero, 464
	sh	a2, 1304(sp)
	addi	a2, zero, 360
	sh	a2, 1306(sp)
	addi	a2, zero, 804
	sh	a2, 1308(sp)
	addi	a2, zero, 356
	sh	a2, 1310(sp)
	addi	a2, zero, 450
	sh	a2, 1312(sp)
	sh	zero, 1314(sp)
	sh	zero, 1316(sp)
	sh	zero, 1318(sp)
	sh	zero, 1320(sp)
	sh	zero, 1322(sp)
	addi	t6, zero, 56
	sh	t6, 1324(sp)
	sh	a6, 1326(sp)
	sh	a4, 1328(sp)
	addi	a2, zero, 100
	sh	a2, 1330(sp)
	addi	a2, zero, 28
	sh	a2, 1332(sp)
	addi	a2, zero, 142
	sh	a2, 1334(sp)
	addi	a2, zero, 146
	sh	a2, 1336(sp)
	addi	a2, zero, 147
	sh	a2, 1338(sp)
	addi	a2, zero, 99
	sh	a2, 1340(sp)
	addi	a2, zero, 226
	sh	a2, 1342(sp)
	addi	a2, zero, 412
	sh	a2, 1344(sp)
	sh	zero, 1346(sp)
	sh	zero, 1348(sp)
	sh	zero, 1350(sp)
	sh	zero, 1352(sp)
	sh	s1, 1354(sp)
	sh	s1, 1356(sp)
	sh	s1, 1358(sp)
	sh	a1, 1360(sp)
	sh	a1, 1362(sp)
	sh	zero, 1364(sp)
	sh	zero, 1366(sp)
	sh	zero, 1368(sp)
	sh	zero, 1370(sp)
	sh	zero, 1372(sp)
	sh	zero, 1374(sp)
	sh	zero, 1376(sp)
	sh	zero, 1378(sp)
	sh	zero, 1380(sp)
	sh	zero, 1382(sp)
	sh	zero, 1384(sp)
	addi	ra, zero, 96
	sh	ra, 1386(sp)
	sh	s1, 1388(sp)
	sh	a1, 1390(sp)
	addi	t0, zero, 24
	sh	t0, 1392(sp)
	sh	t0, 1394(sp)
	addi	t5, zero, 8
	sh	t5, 1396(sp)
	sh	t5, 1398(sp)
	sh	t5, 1400(sp)
	sh	t5, 1402(sp)
	sh	t5, 1404(sp)
	sh	t0, 1406(sp)
	sh	t0, 1408(sp)
	sh	a1, 1410(sp)
	sh	s1, 1412(sp)
	sh	ra, 1414(sp)
	sh	zero, 1416(sp)
	sh	t5, 1418(sp)
	sh	a1, 1420(sp)
	sh	s1, 1422(sp)
	addi	s6, zero, 32
	sh	s6, 1424(sp)
	sh	ra, 1426(sp)
	sh	ra, 1428(sp)
	sh	ra, 1430(sp)
	sh	ra, 1432(sp)
	sh	ra, 1434(sp)
	sh	ra, 1436(sp)
	sh	ra, 1438(sp)
	sh	s6, 1440(sp)
	sh	s1, 1442(sp)
	sh	a1, 1444(sp)
	sh	t5, 1446(sp)
	sh	zero, 1448(sp)
	sh	a1, 1450(sp)
	sh	a3, 1452(sp)
	addi	a3, zero, 92
	sh	a3, 1454(sp)
	sh	s1, 1456(sp)
	sh	s1, 1458(sp)
	addi	a2, zero, 40
	sh	a2, 1460(sp)
	sh	t1, 1462(sp)
	addi	a4, zero, 72
	sh	zero, 1464(sp)
	sh	zero, 1466(sp)
	sh	zero, 1468(sp)
	sh	zero, 1470(sp)
	sh	zero, 1472(sp)
	sh	zero, 1474(sp)
	sh	zero, 1476(sp)
	sh	zero, 1478(sp)
	sh	zero, 1480(sp)
	sh	zero, 1482(sp)
	sh	zero, 1484(sp)
	sh	zero, 1486(sp)
	sh	a1, 1488(sp)
	sh	a1, 1490(sp)
	sh	a1, 1492(sp)
	sh	a7, 1494(sp)
	sh	a1, 1496(sp)
	sh	a1, 1498(sp)
	sh	a1, 1500(sp)
	sh	zero, 1502(sp)
	sh	zero, 1504(sp)
	sh	zero, 1506(sp)
	sh	zero, 1508(sp)
	sh	zero, 1510(sp)
	sh	zero, 1512(sp)
	sh	zero, 1514(sp)
	sh	zero, 1516(sp)
	sh	zero, 1518(sp)
	sh	zero, 1520(sp)
	sh	zero, 1522(sp)
	sh	zero, 1524(sp)
	sh	zero, 1526(sp)
	sh	zero, 1528(sp)
	sh	zero, 1530(sp)
	sh	zero, 1532(sp)
	sh	t0, 1534(sp)
	sh	t0, 1536(sp)
	sh	t5, 1538(sp)
	sh	s10, 1540(sp)
	sh	zero, 1542(sp)
	sh	zero, 1544(sp)
	sh	zero, 1546(sp)
	sh	zero, 1548(sp)
	sh	zero, 1550(sp)
	sh	zero, 1552(sp)
	sh	zero, 1554(sp)
	sh	zero, 1556(sp)
	sh	zero, 1558(sp)
	sh	s2, 1560(sp)
	sh	zero, 1562(sp)
	sh	zero, 1564(sp)
	sh	zero, 1566(sp)
	sh	zero, 1568(sp)
	sh	zero, 1570(sp)
	sh	zero, 1572(sp)
	sh	zero, 1574(sp)
	sh	zero, 1576(sp)
	sh	zero, 1578(sp)
	sh	zero, 1580(sp)
	sh	zero, 1582(sp)
	sh	zero, 1584(sp)
	sh	zero, 1586(sp)
	sh	zero, 1588(sp)
	sh	zero, 1590(sp)
	sh	zero, 1592(sp)
	sh	zero, 1594(sp)
	sh	zero, 1596(sp)
	sh	s1, 1598(sp)
	sh	s1, 1600(sp)
	sh	zero, 1602(sp)
	sh	zero, 1604(sp)
	sh	zero, 1606(sp)
	sh	zero, 1608(sp)
	addi	t1, zero, 128
	sh	t1, 1610(sp)
	sh	t1, 1612(sp)
	addi	a2, zero, 64
	sh	a2, 1614(sp)
	sh	ra, 1616(sp)
	sh	s6, 1618(sp)
	sh	s1, 1620(sp)
	sh	a1, 1622(sp)
	sh	t0, 1624(sp)
	sh	t5, 1626(sp)
	addi	a2, zero, 12
	sh	a2, 1628(sp)
	addi	a3, zero, 12
	sh	s10, 1630(sp)
	addi	s9, zero, 6
	sh	s9, 1632(sp)
	sh	zero, 1634(sp)
	sh	zero, 1636(sp)
	sh	zero, 1638(sp)
	sh	zero, 1640(sp)
	sh	zero, 1642(sp)
	sh	s2, 1644(sp)
	addi	t2, zero, 196
	sh	t2, 1646(sp)
	addi	a5, zero, 134
	sh	a5, 1648(sp)
	sh	a5, 1650(sp)
	addi	a6, zero, 386
	sh	a6, 1652(sp)
	addi	a2, zero, 434
	sh	a2, 1654(sp)
	sh	a2, 1656(sp)
	sh	a5, 1658(sp)
	sh	a5, 1660(sp)
	sh	t2, 1662(sp)
	sh	s2, 1664(sp)
	sh	zero, 1666(sp)
	sh	zero, 1668(sp)
	sh	zero, 1670(sp)
	sh	zero, 1672(sp)
	sh	zero, 1674(sp)
	sh	s1, 1676(sp)
	sh	t6, 1678(sp)
	addi	a2, zero, 38
	sh	a2, 1680(sp)
	sh	s6, 1682(sp)
	sh	s6, 1684(sp)
	sh	s6, 1686(sp)
	sh	s6, 1688(sp)
	sh	s6, 1690(sp)
	sh	s6, 1692(sp)
	sh	s6, 1694(sp)
	sh	a7, 1696(sp)
	sh	zero, 1698(sp)
	sh	zero, 1700(sp)
	sh	zero, 1702(sp)
	sh	zero, 1704(sp)
	sh	zero, 1706(sp)
	sh	s2, 1708(sp)
	sh	t2, 1710(sp)
	sh	a5, 1712(sp)
	sh	t1, 1714(sp)
	addi	s8, zero, 192
	sh	s8, 1716(sp)
	sh	ra, 1718(sp)
	sh	s1, 1720(sp)
	sh	t0, 1722(sp)
	sh	a3, 1724(sp)
	sh	s10, 1726(sp)
	addi	s3, zero, 254
	sh	s3, 1728(sp)
	sh	zero, 1730(sp)
	sh	zero, 1732(sp)
	sh	zero, 1734(sp)
	sh	zero, 1736(sp)
	sh	zero, 1738(sp)
	sh	s2, 1740(sp)
	sh	t2, 1742(sp)
	sh	a5, 1744(sp)
	sh	t1, 1746(sp)
	sh	s8, 1748(sp)
	addi	a3, zero, 112
	sh	a3, 1750(sp)
	sh	s8, 1752(sp)
	sh	t1, 1754(sp)
	sh	a5, 1756(sp)
	sh	t2, 1758(sp)
	sh	s2, 1760(sp)
	sh	zero, 1762(sp)
	sh	zero, 1764(sp)
	sh	zero, 1766(sp)
	sh	zero, 1768(sp)
	sh	zero, 1770(sp)
	sh	ra, 1772(sp)
	sh	ra, 1774(sp)
	addi	a3, zero, 80
	sh	a3, 1776(sp)
	addi	a3, zero, 88
	sh	a3, 1778(sp)
	sh	a4, 1780(sp)
	addi	t3, zero, 68
	sh	t3, 1782(sp)
	addi	a3, zero, 66
	sh	a3, 1784(sp)
	sh	a7, 1786(sp)
	addi	a3, zero, 64
	sh	a3, 1788(sp)
	sh	a3, 1790(sp)
	sh	a3, 1792(sp)
	addi	a2, zero, 64
	sh	zero, 1794(sp)
	sh	zero, 1796(sp)
	sh	zero, 1798(sp)
	sh	zero, 1800(sp)
	sh	zero, 1802(sp)
	addi	a3, zero, 252
	sh	a3, 1804(sp)
	sh	s10, 1806(sp)
	sh	s10, 1808(sp)
	sh	s9, 1810(sp)
	addi	a7, zero, 126
	sh	a7, 1812(sp)
	addi	s5, zero, 198
	sh	s5, 1814(sp)
	sh	t1, 1816(sp)
	addi	a3, zero, 384
	sh	a3, 1818(sp)
	sh	a5, 1820(sp)
	sh	t2, 1822(sp)
	sh	s2, 1824(sp)
	sh	zero, 1826(sp)
	sh	zero, 1828(sp)
	sh	zero, 1830(sp)
	sh	zero, 1832(sp)
	sh	zero, 1834(sp)
	sh	s2, 1836(sp)
	addi	s7, zero, 204
	sh	s7, 1838(sp)
	sh	s10, 1840(sp)
	sh	s9, 1842(sp)
	addi	a3, zero, 118
	sh	a3, 1844(sp)
	addi	a3, zero, 206
	sh	a3, 1846(sp)
	sh	a5, 1848(sp)
	addi	t4, zero, 390
	sh	t4, 1850(sp)
	addi	a3, zero, 132
	sh	a3, 1852(sp)
	sh	s7, 1854(sp)
	sh	s2, 1856(sp)
	sh	zero, 1858(sp)
	sh	zero, 1860(sp)
	sh	zero, 1862(sp)
	sh	zero, 1864(sp)
	sh	zero, 1866(sp)
	sh	s3, 1868(sp)
	sh	t1, 1870(sp)
	sh	a2, 1872(sp)
	sh	ra, 1874(sp)
	sh	s6, 1876(sp)
	sh	s1, 1878(sp)
	sh	s1, 1880(sp)
	sh	a1, 1882(sp)
	sh	t0, 1884(sp)
	sh	t0, 1886(sp)
	sh	t0, 1888(sp)
	sh	zero, 1890(sp)
	sh	zero, 1892(sp)
	sh	zero, 1894(sp)
	sh	zero, 1896(sp)
	sh	zero, 1898(sp)
	sh	t6, 1900(sp)
	sh	t2, 1902(sp)
	sh	a5, 1904(sp)
	sh	a5, 1906(sp)
	sh	t2, 1908(sp)
	sh	s2, 1910(sp)
	sh	t2, 1912(sp)
	sh	a5, 1914(sp)
	sh	a5, 1916(sp)
	sh	t2, 1918(sp)
	sh	t6, 1920(sp)
	sh	zero, 1922(sp)
	sh	zero, 1924(sp)
	sh	zero, 1926(sp)
	sh	zero, 1928(sp)
	sh	zero, 1930(sp)
	sh	s2, 1932(sp)
	sh	t2, 1934(sp)
	sh	a5, 1936(sp)
	sh	a5, 1938(sp)
	sh	a5, 1940(sp)
	sh	t2, 1942(sp)
	addi	a3, zero, 184
	sh	a3, 1944(sp)
	sh	t1, 1946(sp)
	sh	t1, 1948(sp)
	sh	t3, 1950(sp)
	addi	a2, zero, 60
	sh	a2, 1952(sp)
	sh	zero, 1954(sp)
	sh	zero, 1956(sp)
	sh	zero, 1958(sp)
	sh	zero, 1960(sp)
	sh	zero, 1962(sp)
	sh	zero, 1964(sp)
	sh	zero, 1966(sp)
	sh	s1, 1968(sp)
	sh	s1, 1970(sp)
	sh	zero, 1972(sp)
	sh	zero, 1974(sp)
	sh	zero, 1976(sp)
	sh	zero, 1978(sp)
	sh	zero, 1980(sp)
	sh	s1, 1982(sp)
	sh	s1, 1984(sp)
	sh	zero, 1986(sp)
	sh	zero, 1988(sp)
	sh	zero, 1990(sp)
	sh	zero, 1992(sp)
	sh	zero, 1994(sp)
	sh	zero, 1996(sp)
	sh	zero, 1998(sp)
	sh	s1, 2000(sp)
	sh	s1, 2002(sp)
	sh	zero, 2004(sp)
	sh	zero, 2006(sp)
	sh	zero, 2008(sp)
	sh	zero, 2010(sp)
	sh	zero, 2012(sp)
	sh	s1, 2014(sp)
	sh	a1, 2016(sp)
	sh	t0, 2018(sp)
	sh	t5, 2020(sp)
	sh	zero, 2022(sp)
	sh	zero, 2024(sp)
	sh	zero, 2026(sp)
	sh	zero, 2028(sp)
	sh	zero, 2030(sp)
	addi	a3, zero, 384
	sh	a3, 2032(sp)
	sh	ra, 2034(sp)
	addi	a2, zero, 28
	sh	a2, 2036(sp)
	sh	s9, 2038(sp)
	sh	a2, 2040(sp)
	addi	s11, zero, 28
	sh	ra, 2042(sp)
	sh	a3, 2044(sp)
	addi	a4, zero, 384
	sh	zero, 2046(sp)
	sw	a0, 4(sp)
	lui	a0, 1
	addi	a0, a0, -2048
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -2046
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -2044
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -2042
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -2040
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -2038
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -2036
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -2034
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -2032
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -2030
	add	a0, sp, a0
	sh	zero, 0(a0)
	addi	a2, zero, 510
	lui	a0, 1
	addi	a0, a0, -2028
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -2026
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -2024
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -2022
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -2020
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -2018
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -2016
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -2014
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -2012
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -2010
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -2008
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -2006
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -2004
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -2002
	add	a0, sp, a0
	sh	zero, 0(a0)
	addi	a3, zero, 2
	lui	a0, 1
	addi	a0, a0, -2000
	add	a0, sp, a0
	sh	a3, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1998
	add	a0, sp, a0
	sh	s11, 0(a0)
	addi	a2, zero, 224
	lui	a0, 1
	addi	a0, a0, -1996
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1994
	add	a0, sp, a0
	sh	a4, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1992
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1990
	add	a0, sp, a0
	sh	s11, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1988
	add	a0, sp, a0
	sh	a3, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1986
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1984
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1982
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1980
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1978
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1976
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1974
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1972
	add	a0, sp, a0
	sh	t6, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1970
	add	a0, sp, a0
	sh	s5, 0(a0)
	addi	a2, zero, 130
	lui	a0, 1
	addi	a0, a0, -1968
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1966
	add	a0, sp, a0
	sh	t1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1964
	add	a0, sp, a0
	sh	ra, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1962
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1960
	add	a0, sp, a0
	sh	a1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1958
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1956
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1954
	add	a0, sp, a0
	sh	t0, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1952
	add	a0, sp, a0
	sh	t0, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1950
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1948
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1946
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1944
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1942
	add	a0, sp, a0
	sh	s2, 0(a0)
	addi	a4, zero, 132
	lui	a0, 1
	addi	a0, a0, -1940
	add	a0, sp, a0
	sh	a4, 0(a0)
	addi	a2, zero, 260
	lui	a0, 1
	addi	a0, a0, -1938
	add	a0, sp, a0
	sh	a2, 0(a0)
	addi	a2, zero, 434
	lui	a0, 1
	addi	a0, a0, -1936
	add	a0, sp, a0
	sh	a2, 0(a0)
	addi	a2, zero, 330
	lui	a0, 1
	addi	a0, a0, -1934
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1932
	add	a0, sp, a0
	sh	a2, 0(a0)
	addi	a2, zero, 325
	lui	a0, 1
	addi	a0, a0, -1930
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1928
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1926
	add	a0, sp, a0
	sh	a2, 0(a0)
	addi	a2, zero, 357
	lui	a0, 1
	addi	a0, a0, -1924
	add	a0, sp, a0
	sh	a2, 0(a0)
	addi	a2, zero, 366
	lui	a0, 1
	addi	a0, a0, -1922
	add	a0, sp, a0
	sh	a2, 0(a0)
	addi	a2, zero, 218
	lui	a0, 1
	addi	a0, a0, -1920
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1918
	add	a0, sp, a0
	sh	a3, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1916
	add	a0, sp, a0
	sh	a4, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1914
	add	a0, sp, a0
	sh	s2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1912
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1910
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1908
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1906
	add	a0, sp, a0
	sh	t6, 0(a0)
	addi	a2, zero, 104
	lui	a0, 1
	addi	a0, a0, -1904
	add	a0, sp, a0
	sh	a2, 0(a0)
	addi	a2, zero, 72
	lui	a0, 1
	addi	a0, a0, -1902
	add	a0, sp, a0
	sh	a2, 0(a0)
	addi	t3, zero, 76
	lui	a0, 1
	addi	a0, a0, -1900
	add	a0, sp, a0
	sh	t3, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1898
	add	a0, sp, a0
	sh	t2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1896
	add	a0, sp, a0
	sh	t2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1894
	add	a0, sp, a0
	sh	s3, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1892
	add	a0, sp, a0
	sh	a6, 0(a0)
	addi	a2, zero, 258
	lui	a0, 1
	addi	a0, a0, -1890
	add	a0, sp, a0
	sh	a2, 0(a0)
	addi	a2, zero, 259
	lui	a0, 1
	addi	a0, a0, -1888
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1886
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1884
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1882
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1880
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1878
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1876
	add	a0, sp, a0
	sh	a7, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1874
	add	a0, sp, a0
	sh	s5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1872
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1870
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1868
	add	a0, sp, a0
	sh	s5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1866
	add	a0, sp, a0
	sh	a7, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1864
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1862
	add	a0, sp, a0
	sh	t4, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1860
	add	a0, sp, a0
	sh	t4, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1858
	add	a0, sp, a0
	sh	t4, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1856
	add	a0, sp, a0
	sh	a7, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1854
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1852
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1850
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1848
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1846
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1844
	add	a0, sp, a0
	sh	s2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1842
	add	a0, sp, a0
	sh	s7, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1840
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1838
	add	a0, sp, a0
	sh	s9, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1836
	add	a0, sp, a0
	sh	a3, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1834
	add	a0, sp, a0
	sh	a3, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1832
	add	a0, sp, a0
	sh	s9, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1830
	add	a0, sp, a0
	sh	s9, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1828
	add	a0, sp, a0
	sh	t4, 0(a0)
	addi	a2, zero, 140
	lui	a0, 1
	addi	a0, a0, -1826
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1824
	add	a0, sp, a0
	sh	s2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1822
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1820
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1818
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1816
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1814
	add	a0, sp, a0
	sh	zero, 0(a0)
	addi	s11, zero, 62
	lui	a0, 1
	addi	a0, a0, -1812
	add	a0, sp, a0
	sh	s11, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1810
	add	a0, sp, a0
	sh	s5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1808
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1806
	add	a0, sp, a0
	sh	t4, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1804
	add	a0, sp, a0
	sh	t4, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1802
	add	a0, sp, a0
	sh	t4, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1800
	add	a0, sp, a0
	sh	t4, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1798
	add	a0, sp, a0
	sh	t4, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1796
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1794
	add	a0, sp, a0
	sh	s5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1792
	add	a0, sp, a0
	sh	s11, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1790
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1788
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1786
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1784
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1782
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1780
	add	a0, sp, a0
	sh	s3, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1778
	add	a0, sp, a0
	sh	s9, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1776
	add	a0, sp, a0
	sh	s9, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1774
	add	a0, sp, a0
	sh	s9, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1772
	add	a0, sp, a0
	sh	s9, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1770
	add	a0, sp, a0
	sh	s3, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1768
	add	a0, sp, a0
	sh	s9, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1766
	add	a0, sp, a0
	sh	s9, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1764
	add	a0, sp, a0
	sh	s9, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1762
	add	a0, sp, a0
	sh	s9, 0(a0)
	addi	a2, zero, 510
	lui	a0, 1
	addi	a0, a0, -1760
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1758
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1756
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1754
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1752
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1750
	add	a0, sp, a0
	sh	zero, 0(a0)
	addi	a2, zero, 252
	lui	a0, 1
	addi	a0, a0, -1748
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1746
	add	a0, sp, a0
	sh	s10, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1744
	add	a0, sp, a0
	sh	s10, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1742
	add	a0, sp, a0
	sh	s10, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1740
	add	a0, sp, a0
	sh	s10, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1738
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1736
	add	a0, sp, a0
	sh	s10, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1734
	add	a0, sp, a0
	sh	s10, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1732
	add	a0, sp, a0
	sh	s10, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1730
	add	a0, sp, a0
	sh	s10, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1728
	add	a0, sp, a0
	sh	s10, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1726
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1724
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1722
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1720
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1718
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1716
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1714
	add	a0, sp, a0
	sh	s7, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1712
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1710
	add	a0, sp, a0
	sh	s9, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1708
	add	a0, sp, a0
	sh	a3, 0(a0)
	addi	a2, zero, 482
	lui	a0, 1
	addi	a0, a0, -1706
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1704
	add	a0, sp, a0
	sh	t4, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1702
	add	a0, sp, a0
	sh	t4, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1700
	add	a0, sp, a0
	sh	t4, 0(a0)
	addi	a2, zero, 396
	lui	a0, 1
	addi	a0, a0, -1698
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1696
	add	a0, sp, a0
	sh	s2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1694
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1692
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1690
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1688
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1686
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1684
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1682
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1680
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1678
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1676
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1674
	add	a0, sp, a0
	sh	s3, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1672
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1670
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1668
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1666
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1664
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1662
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1660
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1658
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1656
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1654
	add	a0, sp, a0
	sh	zero, 0(a0)
	addi	a2, zero, 252
	lui	a0, 1
	addi	a0, a0, -1652
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1650
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1648
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1646
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1644
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1642
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1640
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1638
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1636
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1634
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1632
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1630
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1628
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1626
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1624
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1622
	add	a0, sp, a0
	sh	zero, 0(a0)
	addi	a2, zero, 240
	lui	a0, 1
	addi	a0, a0, -1620
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1618
	add	a0, sp, a0
	sh	s8, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1616
	add	a0, sp, a0
	sh	s8, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1614
	add	a0, sp, a0
	sh	s8, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1612
	add	a0, sp, a0
	sh	s8, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1610
	add	a0, sp, a0
	sh	s8, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1608
	add	a0, sp, a0
	sh	s8, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1606
	add	a0, sp, a0
	sh	s8, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1604
	add	a0, sp, a0
	sh	t2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1602
	add	a0, sp, a0
	sh	t3, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1600
	add	a0, sp, a0
	sh	t6, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1598
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1596
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1594
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1592
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1590
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1588
	add	a0, sp, a0
	sh	t4, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1586
	add	a0, sp, a0
	sh	s5, 0(a0)
	addi	a3, zero, 102
	lui	a0, 1
	addi	a0, a0, -1584
	add	a0, sp, a0
	sh	a3, 0(a0)
	addi	a4, zero, 38
	lui	a0, 1
	addi	a0, a0, -1582
	add	a0, sp, a0
	sh	a4, 0(a0)
	addi	a2, zero, 22
	lui	a0, 1
	addi	a0, a0, -1580
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1578
	add	a0, sp, a0
	sh	s11, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1576
	add	a0, sp, a0
	sh	a4, 0(a0)
	addi	a4, zero, 38
	addi	a2, zero, 70
	lui	a0, 1
	addi	a0, a0, -1574
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1572
	add	a0, sp, a0
	sh	s5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1570
	add	a0, sp, a0
	sh	t4, 0(a0)
	addi	a2, zero, 262
	lui	a0, 1
	addi	a0, a0, -1568
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1566
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1564
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1562
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1560
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1558
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1556
	add	a0, sp, a0
	sh	s10, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1554
	add	a0, sp, a0
	sh	s10, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1552
	add	a0, sp, a0
	sh	s10, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1550
	add	a0, sp, a0
	sh	s10, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1548
	add	a0, sp, a0
	sh	s10, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1546
	add	a0, sp, a0
	sh	s10, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1544
	add	a0, sp, a0
	sh	s10, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1542
	add	a0, sp, a0
	sh	s10, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1540
	add	a0, sp, a0
	sh	s10, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1538
	add	a0, sp, a0
	sh	s10, 0(a0)
	addi	a2, zero, 508
	lui	a0, 1
	addi	a0, a0, -1536
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1534
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1532
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1530
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1528
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1526
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1524
	add	a0, sp, a0
	sh	t4, 0(a0)
	addi	a2, zero, 454
	lui	a0, 1
	addi	a0, a0, -1522
	add	a0, sp, a0
	sh	a2, 0(a0)
	addi	a2, zero, 462
	lui	a0, 1
	addi	a0, a0, -1520
	add	a0, sp, a0
	sh	a2, 0(a0)
	addi	a2, zero, 458
	lui	a0, 1
	addi	a0, a0, -1518
	add	a0, sp, a0
	sh	a2, 0(a0)
	addi	a2, zero, 426
	lui	a0, 1
	addi	a0, a0, -1516
	add	a0, sp, a0
	sh	a2, 0(a0)
	addi	s3, zero, 426
	addi	a2, zero, 434
	lui	a0, 1
	addi	a0, a0, -1514
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1512
	add	a0, sp, a0
	sh	a2, 0(a0)
	addi	t3, zero, 434
	lui	a0, 1
	addi	a0, a0, -1510
	add	a0, sp, a0
	sh	a6, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1508
	add	a0, sp, a0
	sh	a6, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1506
	add	a0, sp, a0
	sh	a6, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1504
	add	a0, sp, a0
	sh	a6, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1502
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1500
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1498
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1496
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1494
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1492
	add	a0, sp, a0
	sh	a5, 0(a0)
	addi	a2, zero, 142
	lui	a0, 1
	addi	a0, a0, -1490
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1488
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1486
	add	a0, sp, a0
	sh	a2, 0(a0)
	addi	a2, zero, 150
	lui	a0, 1
	addi	a0, a0, -1484
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1482
	add	a0, sp, a0
	sh	a2, 0(a0)
	addi	a2, zero, 166
	lui	a0, 1
	addi	a0, a0, -1480
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1478
	add	a0, sp, a0
	sh	a2, 0(a0)
	addi	s11, zero, 230
	lui	a0, 1
	addi	a0, a0, -1476
	add	a0, sp, a0
	sh	s11, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1474
	add	a0, sp, a0
	sh	s5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1472
	add	a0, sp, a0
	sh	s5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1470
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1468
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1466
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1464
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1462
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1460
	add	a0, sp, a0
	sh	s2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1458
	add	a0, sp, a0
	sh	s7, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1456
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1454
	add	a0, sp, a0
	sh	t4, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1452
	add	a0, sp, a0
	sh	a6, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1450
	add	a0, sp, a0
	sh	a6, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1448
	add	a0, sp, a0
	sh	a6, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1446
	add	a0, sp, a0
	sh	t4, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1444
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1442
	add	a0, sp, a0
	sh	s7, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1440
	add	a0, sp, a0
	sh	s2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1438
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1436
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1434
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1432
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1430
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1428
	add	a0, sp, a0
	sh	a7, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1426
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1424
	add	a0, sp, a0
	sh	t4, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1422
	add	a0, sp, a0
	sh	t4, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1420
	add	a0, sp, a0
	sh	s5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1418
	add	a0, sp, a0
	sh	a7, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1416
	add	a0, sp, a0
	sh	s9, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1414
	add	a0, sp, a0
	sh	s9, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1412
	add	a0, sp, a0
	sh	s9, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1410
	add	a0, sp, a0
	sh	s9, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1408
	add	a0, sp, a0
	sh	s9, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1406
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1404
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1402
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1400
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1398
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1396
	add	a0, sp, a0
	sh	s2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1394
	add	a0, sp, a0
	sh	s7, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1392
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1390
	add	a0, sp, a0
	sh	t4, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1388
	add	a0, sp, a0
	sh	a6, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1386
	add	a0, sp, a0
	sh	a6, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1384
	add	a0, sp, a0
	sh	a6, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1382
	add	a0, sp, a0
	sh	t4, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1380
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1378
	add	a0, sp, a0
	sh	s7, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1376
	add	a0, sp, a0
	sh	s2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1374
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1372
	add	a0, sp, a0
	sh	ra, 0(a0)
	addi	a2, zero, 448
	lui	a0, 1
	addi	a0, a0, -1370
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1368
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1366
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1364
	add	a0, sp, a0
	sh	a7, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1362
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1360
	add	a0, sp, a0
	sh	t4, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1358
	add	a0, sp, a0
	sh	t4, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1356
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1354
	add	a0, sp, a0
	sh	a7, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1352
	add	a0, sp, a0
	sh	a4, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1350
	add	a0, sp, a0
	sh	a3, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1348
	add	a0, sp, a0
	sh	s5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1346
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1344
	add	a0, sp, a0
	sh	t4, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1342
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1340
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1338
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1336
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1334
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1332
	add	a0, sp, a0
	sh	s2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1330
	add	a0, sp, a0
	sh	t2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1328
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1326
	add	a0, sp, a0
	sh	s9, 0(a0)
	addi	a7, zero, 28
	lui	a0, 1
	addi	a0, a0, -1324
	add	a0, sp, a0
	sh	a7, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1322
	add	a0, sp, a0
	sh	s2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1320
	add	a0, sp, a0
	sh	s8, 0(a0)
	addi	a2, zero, 384
	lui	a0, 1
	addi	a0, a0, -1318
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1316
	add	a0, sp, a0
	sh	a6, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1314
	add	a0, sp, a0
	sh	a5, 0(a0)
	addi	a2, zero, 124
	lui	a0, 1
	addi	a0, a0, -1312
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1310
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1308
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1306
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1304
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1302
	add	a0, sp, a0
	sh	zero, 0(a0)
	addi	a2, zero, 510
	lui	a0, 1
	addi	a0, a0, -1300
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1298
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1296
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1294
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1292
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1290
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1288
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1286
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1284
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1282
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1280
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1278
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1276
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1274
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1272
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1270
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1268
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1266
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1264
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1262
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1260
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1258
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1256
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1254
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1252
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1250
	add	a0, sp, a0
	sh	t2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1248
	add	a0, sp, a0
	sh	s2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1246
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1244
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1242
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1240
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1238
	add	a0, sp, a0
	sh	zero, 0(a0)
	addi	a2, zero, 259
	lui	a0, 1
	addi	a0, a0, -1236
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1234
	add	a0, sp, a0
	sh	a6, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1232
	add	a0, sp, a0
	sh	a6, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1230
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1228
	add	a0, sp, a0
	sh	t2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1226
	add	a0, sp, a0
	sh	s7, 0(a0)
	addi	a2, zero, 76
	lui	a0, 1
	addi	a0, a0, -1224
	add	a0, sp, a0
	sh	a2, 0(a0)
	addi	a4, zero, 104
	lui	a0, 1
	addi	a0, a0, -1222
	add	a0, sp, a0
	sh	a4, 0(a0)
	addi	a2, zero, 40
	lui	a0, 1
	addi	a0, a0, -1220
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1218
	add	a0, sp, a0
	sh	t6, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1216
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1214
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1212
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1210
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1208
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1206
	add	a0, sp, a0
	sh	zero, 0(a0)
	addi	a2, zero, 771
	lui	a0, 1
	addi	a0, a0, -1204
	add	a0, sp, a0
	sh	a2, 0(a0)
	addi	a2, zero, 259
	lui	a0, 1
	addi	a0, a0, -1202
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1200
	add	a0, sp, a0
	sh	a2, 0(a0)
	addi	a2, zero, 306
	lui	a0, 1
	addi	a0, a0, -1198
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1196
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1194
	add	a0, sp, a0
	sh	t3, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1192
	add	a0, sp, a0
	sh	s3, 0(a0)
	addi	t3, zero, 426
	addi	a2, zero, 234
	lui	a0, 1
	addi	a0, a0, -1190
	add	a0, sp, a0
	sh	a2, 0(a0)
	addi	a2, zero, 206
	lui	a0, 1
	addi	a0, a0, -1188
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1186
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1184
	add	a0, sp, a0
	sh	t2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1182
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1180
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1178
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1176
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1174
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1172
	add	a0, sp, a0
	sh	t4, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1170
	add	a0, sp, a0
	sh	t2, 0(a0)
	addi	a3, zero, 76
	lui	a0, 1
	addi	a0, a0, -1168
	add	a0, sp, a0
	sh	a3, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1166
	add	a0, sp, a0
	sh	a4, 0(a0)
	addi	s3, zero, 104
	lui	a0, 1
	addi	a0, a0, -1164
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1162
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1160
	add	a0, sp, a0
	sh	t6, 0(a0)
	addi	a2, zero, 72
	lui	a0, 1
	addi	a0, a0, -1158
	add	a0, sp, a0
	sh	a2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1156
	add	a0, sp, a0
	sh	t2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1154
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1152
	add	a0, sp, a0
	sh	a6, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1150
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1148
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1146
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1144
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1142
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1140
	add	a0, sp, a0
	sh	a6, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1138
	add	a0, sp, a0
	sh	a5, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1136
	add	a0, sp, a0
	sh	t2, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1134
	add	a0, sp, a0
	sh	a3, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1132
	add	a0, sp, a0
	sh	t6, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1130
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1128
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1126
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1124
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1122
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1120
	add	a0, sp, a0
	sh	s1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1118
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1116
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1114
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1112
	add	a0, sp, a0
	sh	zero, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1110
	add	a0, sp, a0
	sh	zero, 0(a0)
	addi	a4, zero, 510
	lui	a0, 1
	addi	a0, a0, -1108
	add	a0, sp, a0
	sh	a4, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1106
	add	a0, sp, a0
	sh	t1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1104
	add	a0, sp, a0
	sh	s8, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1102
	add	a0, sp, a0
	sh	ra, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1100
	add	a0, sp, a0
	sh	s6, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1098
	add	a0, sp, a0
	sh	a1, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1096
	add	a0, sp, a0
	sh	t0, 0(a0)
	addi	a3, zero, 12
	lui	a0, 1
	addi	a0, a0, -1094
	add	a0, sp, a0
	sh	a3, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1092
	add	a0, sp, a0
	sh	s10, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1090
	add	a0, sp, a0
	sh	s9, 0(a0)
	lui	a0, 1
	addi	a0, a0, -1088
	add	a0, sp, a0
	sh	a4, 0(a0)
	lw	a0, 4(sp)
	lui	a4, 1
	addi	a4, a4, -1086
	add	a4, sp, a4
	sh	zero, 0(a4)
	lui	a4, 1
	addi	a4, a4, -1084
	add	a4, sp, a4
	sh	zero, 0(a4)
	lui	a4, 1
	addi	a4, a4, -1082
	add	a4, sp, a4
	sh	zero, 0(a4)
	lui	a4, 1
	addi	a4, a4, -1080
	add	a4, sp, a4
	sh	zero, 0(a4)
	lui	a4, 1
	addi	a4, a4, -1078
	add	a4, sp, a4
	sh	s2, 0(a4)
	lui	a4, 1
	addi	a4, a4, -1076
	add	a4, sp, a4
	sh	t0, 0(a4)
	lui	a4, 1
	addi	a4, a4, -1074
	add	a4, sp, a4
	sh	t0, 0(a4)
	lui	a4, 1
	addi	a4, a4, -1072
	add	a4, sp, a4
	sh	t0, 0(a4)
	lui	a4, 1
	addi	a4, a4, -1070
	add	a4, sp, a4
	sh	t0, 0(a4)
	lui	a4, 1
	addi	a4, a4, -1068
	add	a4, sp, a4
	sh	t0, 0(a4)
	lui	a4, 1
	addi	a4, a4, -1066
	add	a4, sp, a4
	sh	t0, 0(a4)
	lui	a4, 1
	addi	a4, a4, -1064
	add	a4, sp, a4
	sh	t0, 0(a4)
	lui	a4, 1
	addi	a4, a4, -1062
	add	a4, sp, a4
	sh	t0, 0(a4)
	lui	a4, 1
	addi	a4, a4, -1060
	add	a4, sp, a4
	sh	t0, 0(a4)
	lui	a4, 1
	addi	a4, a4, -1058
	add	a4, sp, a4
	sh	t0, 0(a4)
	lui	a4, 1
	addi	a4, a4, -1056
	add	a4, sp, a4
	sh	t0, 0(a4)
	lui	a4, 1
	addi	a4, a4, -1054
	add	a4, sp, a4
	sh	t0, 0(a4)
	lui	a4, 1
	addi	a4, a4, -1052
	add	a4, sp, a4
	sh	t0, 0(a4)
	lui	a4, 1
	addi	a4, a4, -1050
	add	a4, sp, a4
	sh	s2, 0(a4)
	lui	a4, 1
	addi	a4, a4, -1048
	add	a4, sp, a4
	sh	zero, 0(a4)
	lui	a4, 1
	addi	a4, a4, -1046
	add	a4, sp, a4
	sh	s9, 0(a4)
	lui	a4, 1
	addi	a4, a4, -1044
	add	a4, sp, a4
	sh	s10, 0(a4)
	lui	a4, 1
	addi	a4, a4, -1042
	add	a4, sp, a4
	sh	a3, 0(a4)
	lui	a3, 1
	addi	a3, a3, -1040
	add	a3, sp, a3
	sh	t5, 0(a3)
	lui	a3, 1
	addi	a3, a3, -1038
	add	a3, sp, a3
	sh	t0, 0(a3)
	lui	a3, 1
	addi	a3, a3, -1036
	add	a3, sp, a3
	sh	a1, 0(a3)
	lui	a3, 1
	addi	a3, a3, -1034
	add	a3, sp, a3
	sh	s1, 0(a3)
	lui	a3, 1
	addi	a3, a3, -1032
	add	a3, sp, a3
	sh	s6, 0(a3)
	lui	a3, 1
	addi	a3, a3, -1030
	add	a3, sp, a3
	sh	ra, 0(a3)
	addi	a3, zero, 64
	lui	a4, 1
	addi	a4, a4, -1028
	add	a4, sp, a4
	sh	a3, 0(a4)
	lui	a3, 1
	addi	a3, a3, -1026
	add	a3, sp, a3
	sh	s8, 0(a3)
	lui	a3, 1
	addi	a3, a3, -1024
	add	a3, sp, a3
	sh	t1, 0(a3)
	lui	a3, 1
	addi	a3, a3, -1022
	add	a3, sp, a3
	sh	zero, 0(a3)
	lui	a3, 1
	addi	a3, a3, -1020
	add	a3, sp, a3
	sh	zero, 0(a3)
	lui	a3, 1
	addi	a3, a3, -1018
	add	a3, sp, a3
	sh	zero, 0(a3)
	lui	a3, 1
	addi	a3, a3, -1016
	add	a3, sp, a3
	sh	zero, 0(a3)
	addi	a3, zero, 60
	lui	a4, 1
	addi	a4, a4, -1014
	add	a4, sp, a4
	sh	a3, 0(a4)
	lui	a4, 1
	addi	a4, a4, -1012
	add	a4, sp, a4
	sh	s6, 0(a4)
	lui	a4, 1
	addi	a4, a4, -1010
	add	a4, sp, a4
	sh	s6, 0(a4)
	lui	a4, 1
	addi	a4, a4, -1008
	add	a4, sp, a4
	sh	s6, 0(a4)
	lui	a4, 1
	addi	a4, a4, -1006
	add	a4, sp, a4
	sh	s6, 0(a4)
	lui	a4, 1
	addi	a4, a4, -1004
	add	a4, sp, a4
	sh	s6, 0(a4)
	lui	a4, 1
	addi	a4, a4, -1002
	add	a4, sp, a4
	sh	s6, 0(a4)
	lui	a4, 1
	addi	a4, a4, -1000
	add	a4, sp, a4
	sh	s6, 0(a4)
	lui	a4, 1
	addi	a4, a4, -998
	add	a4, sp, a4
	sh	s6, 0(a4)
	lui	a4, 1
	addi	a4, a4, -996
	add	a4, sp, a4
	sh	s6, 0(a4)
	lui	a4, 1
	addi	a4, a4, -994
	add	a4, sp, a4
	sh	s6, 0(a4)
	lui	a4, 1
	addi	a4, a4, -992
	add	a4, sp, a4
	sh	s6, 0(a4)
	lui	a4, 1
	addi	a4, a4, -990
	add	a4, sp, a4
	sh	s6, 0(a4)
	lui	a4, 1
	addi	a4, a4, -988
	add	a4, sp, a4
	sh	s6, 0(a4)
	lui	a4, 1
	addi	a4, a4, -986
	add	a4, sp, a4
	sh	a3, 0(a4)
	lui	a3, 1
	addi	a3, a3, -984
	add	a3, sp, a3
	sh	zero, 0(a3)
	lui	a3, 1
	addi	a3, a3, -982
	add	a3, sp, a3
	sh	zero, 0(a3)
	lui	a3, 1
	addi	a3, a3, -980
	add	a3, sp, a3
	sh	s1, 0(a3)
	lui	a3, 1
	addi	a3, a3, -978
	add	a3, sp, a3
	sh	t6, 0(a3)
	lui	a3, 1
	addi	a3, a3, -976
	add	a3, sp, a3
	sh	s3, 0(a3)
	lui	a3, 1
	addi	a3, a3, -974
	add	a3, sp, a3
	sh	a2, 0(a3)
	addi	a3, zero, 72
	addi	a2, zero, 68
	lui	a4, 1
	addi	a4, a4, -972
	add	a4, sp, a4
	sh	a2, 0(a4)
	addi	a2, zero, 132
	lui	a4, 1
	addi	a4, a4, -970
	add	a4, sp, a4
	sh	a2, 0(a4)
	addi	a2, zero, 130
	lui	a4, 1
	addi	a4, a4, -968
	add	a4, sp, a4
	sh	a2, 0(a4)
	lui	a2, 1
	addi	a2, a2, -966
	add	a2, sp, a2
	sh	zero, 0(a2)
	lui	a2, 1
	addi	a2, a2, -964
	add	a2, sp, a2
	sh	zero, 0(a2)
	lui	a2, 1
	addi	a2, a2, -962
	add	a2, sp, a2
	sh	zero, 0(a2)
	lui	a2, 1
	addi	a2, a2, -960
	add	a2, sp, a2
	sh	zero, 0(a2)
	lui	a2, 1
	addi	a2, a2, -958
	add	a2, sp, a2
	sh	zero, 0(a2)
	lui	a2, 1
	addi	a2, a2, -956
	add	a2, sp, a2
	sh	zero, 0(a2)
	lui	a2, 1
	addi	a2, a2, -954
	add	a2, sp, a2
	sh	zero, 0(a2)
	lui	a2, 1
	addi	a2, a2, -952
	add	a2, sp, a2
	sh	zero, 0(a2)
	lui	a2, 1
	addi	a2, a2, -950
	add	a2, sp, a2
	sh	zero, 0(a2)
	lui	a2, 1
	addi	a2, a2, -948
	add	a2, sp, a2
	sh	zero, 0(a2)
	lui	a2, 1
	addi	a2, a2, -946
	add	a2, sp, a2
	sh	zero, 0(a2)
	lui	a2, 1
	addi	a2, a2, -944
	add	a2, sp, a2
	sh	zero, 0(a2)
	lui	a2, 1
	addi	a2, a2, -942
	add	a2, sp, a2
	sh	zero, 0(a2)
	lui	a2, 1
	addi	a2, a2, -940
	add	a2, sp, a2
	sh	zero, 0(a2)
	lui	a2, 1
	addi	a2, a2, -938
	add	a2, sp, a2
	sh	zero, 0(a2)
	lui	a2, 1
	addi	a2, a2, -936
	add	a2, sp, a2
	sh	zero, 0(a2)
	lui	a2, 1
	addi	a2, a2, -934
	add	a2, sp, a2
	sh	zero, 0(a2)
	lui	a2, 1
	addi	a2, a2, -932
	add	a2, sp, a2
	sh	zero, 0(a2)
	lui	a2, 1
	addi	a2, a2, -930
	add	a2, sp, a2
	sh	zero, 0(a2)
	lui	a2, 1
	addi	a2, a2, -928
	add	a2, sp, a2
	sh	zero, 0(a2)
	lui	a2, 1
	addi	a2, a2, -926
	add	a2, sp, a2
	sh	zero, 0(a2)
	addi	a2, zero, 1023
	lui	a4, 1
	addi	a4, a4, -924
	add	a4, sp, a4
	sh	a2, 0(a4)
	lui	a2, 1
	addi	a2, a2, -922
	add	a2, sp, a2
	sh	zero, 0(a2)
	addi	a2, zero, 252
	sh	a2, 1072(s0)
	sh	s11, 1078(s0)
	addi	a2, zero, 412
	sh	a2, 1080(s0)
	sh	s7, 1194(s0)
	addi	a2, zero, 140
	sh	a2, 1206(s0)
	addi	a2, zero, 496
	sh	a2, 1218(s0)
	addi	a2, zero, 68
	sh	a2, 1066(s0)
	sh	a2, 1386(s0)
	addi	a2, zero, 52
	sh	a2, 1390(s0)
	addi	a2, zero, 36
	sh	a2, 1388(s0)
	sh	a2, 1394(s0)
	addi	a2, zero, 100
	sh	a2, 1396(s0)
	addi	a2, zero, 510
	sh	a2, 1200(s0)
	sh	a2, 1336(s0)
	addi	a2, zero, 510
	sh	a2, 1432(s0)
	addi	a2, zero, 218
	sh	a2, 1448(s0)
	addi	a2, zero, 438
	sh	a2, 1450(s0)
	addi	a2, zero, 402
	sh	a2, 1454(s0)
	sh	a2, 1456(s0)
	sh	a2, 1458(s0)
	sh	a2, 1460(s0)
	sh	a2, 1462(s0)
	sh	a2, 1464(s0)
	addi	a2, zero, 118
	sh	a2, 1096(s0)
	sh	a2, 1112(s0)
	sh	a2, 1288(s0)
	addi	a2, zero, 118
	sh	a2, 1480(s0)
	sh	s2, 1528(s0)
	sh	a2, 1544(s0)
	sh	a2, 1560(s0)
	addi	a2, zero, 184
	sh	a2, 1160(s0)
	sh	a2, 1256(s0)
	addi	a2, zero, 184
	sh	a2, 1576(s0)
	sh	s5, 1074(s0)
	sh	s5, 1076(s0)
	sh	s5, 1164(s0)
	sh	s5, 1172(s0)
	sh	s5, 1260(s0)
	sh	s5, 1268(s0)
	sh	s5, 1588(s0)
	addi	a2, zero, 228
	sh	a2, 1608(s0)
	sh	a7, 1610(s0)
	sh	s2, 1128(s0)
	sh	s2, 1144(s0)
	sh	s2, 1192(s0)
	sh	s2, 1208(s0)
	sh	s2, 1278(s0)
	sh	s2, 1512(s0)
	sh	s2, 1640(s0)
	sh	s2, 1648(s0)
	sh	t1, 1154(s0)
	sh	t1, 1156(s0)
	sh	t1, 1158(s0)
	sh	t1, 1274(s0)
	sh	t1, 1594(s0)
	sh	t1, 1596(s0)
	sh	t1, 1598(s0)
	sh	t1, 1652(s0)
	addi	a2, zero, 124
	sh	a2, 1064(s0)
	sh	a2, 1352(s0)
	sh	a2, 1656(s0)
	sh	t5, 1668(s0)
	sh	t5, 1670(s0)
	sh	t5, 1674(s0)
	sh	t5, 1676(s0)
	sh	t5, 1678(s0)
	sh	t5, 1680(s0)
	sh	t5, 1682(s0)
	sh	t5, 1684(s0)
	addi	a2, zero, 188
	sh	a2, 1176(s0)
	sh	a2, 1272(s0)
	sh	a2, 1592(s0)
	sh	a2, 1720(s0)
	addi	a2, zero, 259
	sh	a2, 1768(s0)
	sh	a2, 1770(s0)
	addi	a2, zero, 258
	sh	a2, 1772(s0)
	addi	a2, zero, 306
	sh	a2, 1774(s0)
	addi	a2, zero, 434
	sh	a2, 1452(s0)
	sh	a2, 1776(s0)
	sh	t3, 1778(s0)
	addi	a2, zero, 202
	sh	a2, 1780(s0)
	addi	a2, zero, 206
	sh	a2, 1098(s0)
	sh	a2, 1110(s0)
	sh	a2, 1290(s0)
	sh	a2, 1482(s0)
	sh	a2, 1546(s0)
	sh	a2, 1558(s0)
	sh	a2, 1782(s0)
	addi	a2, zero, 104
	sh	a2, 1746(s0)
	sh	a2, 1748(s0)
	addi	a2, zero, 104
	sh	a2, 1804(s0)
	addi	a2, zero, 56
	sh	a2, 1750(s0)
	sh	a2, 1806(s0)
	addi	a2, zero, 56
	sh	a2, 1810(s0)
	sh	t4, 1104(s0)
	sh	t4, 1518(s0)
	sh	t4, 1520(s0)
	sh	t4, 1816(s0)
	sh	a6, 1736(s0)
	sh	a6, 1832(s0)
	sh	a5, 1100(s0)
	sh	a5, 1102(s0)
	sh	a5, 1106(s0)
	sh	a5, 1108(s0)
	sh	a5, 1166(s0)
	sh	a5, 1168(s0)
	sh	a5, 1170(s0)
	sh	a5, 1196(s0)
	sh	a5, 1198(s0)
	sh	a5, 1262(s0)
	sh	a5, 1264(s0)
	sh	a5, 1266(s0)
	sh	a5, 1292(s0)
	sh	a5, 1294(s0)
	sh	a5, 1296(s0)
	sh	a5, 1298(s0)
	sh	a5, 1300(s0)
	sh	a5, 1302(s0)
	sh	a5, 1304(s0)
	sh	a5, 1484(s0)
	sh	a5, 1486(s0)
	sh	a5, 1488(s0)
	sh	a5, 1490(s0)
	sh	a5, 1492(s0)
	sh	a5, 1494(s0)
	sh	a5, 1496(s0)
	sh	a5, 1516(s0)
	sh	a5, 1522(s0)
	sh	a5, 1524(s0)
	sh	a5, 1548(s0)
	sh	a5, 1550(s0)
	sh	a5, 1552(s0)
	sh	a5, 1554(s0)
	sh	a5, 1556(s0)
	sh	a5, 1580(s0)
	sh	a5, 1582(s0)
	sh	a5, 1584(s0)
	sh	a5, 1586(s0)
	sh	a5, 1704(s0)
	sh	a5, 1706(s0)
	sh	a5, 1708(s0)
	sh	a5, 1710(s0)
	sh	a5, 1712(s0)
	sh	a5, 1714(s0)
	sh	a5, 1738(s0)
	sh	a5, 1740(s0)
	sh	a5, 1800(s0)
	sh	a5, 1834(s0)
	addi	a2, zero, 132
	sh	a2, 1130(s0)
	sh	a2, 1142(s0)
	sh	a2, 1384(s0)
	sh	a2, 1400(s0)
	sh	a2, 1716(s0)
	sh	a2, 1836(s0)
	sh	t2, 1162(s0)
	sh	t2, 1174(s0)
	sh	t2, 1258(s0)
	sh	t2, 1270(s0)
	sh	t2, 1276(s0)
	sh	t2, 1398(s0)
	sh	t2, 1514(s0)
	sh	t2, 1526(s0)
	sh	t2, 1578(s0)
	sh	t2, 1590(s0)
	sh	t2, 1642(s0)
	sh	t2, 1654(s0)
	sh	t2, 1718(s0)
	sh	t2, 1742(s0)
	sh	t2, 1784(s0)
	sh	t2, 1802(s0)
	sh	t2, 1814(s0)
	sh	t2, 1838(s0)
	addi	a2, zero, 76
	sh	a2, 1744(s0)
	sh	a2, 1812(s0)
	sh	a2, 1840(s0)
	sh	a3, 1842(s0)
	addi	a2, zero, 40
	sh	a2, 1844(s0)
	addi	a2, zero, 14
	sh	a2, 1854(s0)
	sh	ra, 1346(s0)
	sh	ra, 1348(s0)
	sh	ra, 1354(s0)
	sh	ra, 1356(s0)
	sh	ra, 1358(s0)
	sh	ra, 1360(s0)
	sh	ra, 1362(s0)
	sh	ra, 1364(s0)
	sh	ra, 1366(s0)
	sh	ra, 1368(s0)
	sh	ra, 1868(s0)
	sh	s10, 1378(s0)
	sh	s10, 1380(s0)
	sh	s10, 1382(s0)
	sh	s10, 1644(s0)
	sh	s10, 1878(s0)
	addi	a2, zero, 254
	sh	a2, 1224(s0)
	sh	a2, 1672(s0)
	sh	a2, 1864(s0)
	sh	a2, 1880(s0)
	sh	t0, 1024(s0)
	sh	t0, 1220(s0)
	sh	t0, 1222(s0)
	sh	t0, 1226(s0)
	sh	t0, 1228(s0)
	sh	t0, 1230(s0)
	sh	t0, 1232(s0)
	sh	t0, 1234(s0)
	sh	t0, 1236(s0)
	sh	t0, 1238(s0)
	sh	t0, 1240(s0)
	sh	t0, 1686(s0)
	sh	t0, 1852(s0)
	sh	t0, 1874(s0)
	sh	t0, 1902(s0)
	sh	t0, 1906(s0)
	sh	s8, 1068(s0)
	sh	s8, 1070(s0)
	sh	s8, 1650(s0)
	sh	s8, 1866(s0)
	sh	s8, 1968(s0)
	sh	a1, 1850(s0)
	sh	a1, 1872(s0)
	sh	a1, 1894(s0)
	sh	a1, 1896(s0)
	sh	a1, 1898(s0)
	sh	a1, 1900(s0)
	sh	a1, 1908(s0)
	sh	a1, 1910(s0)
	sh	a1, 1912(s0)
	sh	a1, 1914(s0)
	sh	a1, 1922(s0)
	sh	a1, 1924(s0)
	sh	a1, 1926(s0)
	sh	a1, 1928(s0)
	sh	a1, 1930(s0)
	sh	a1, 1932(s0)
	sh	a1, 1934(s0)
	sh	a1, 1936(s0)
	sh	a1, 1938(s0)
	sh	a1, 1940(s0)
	sh	a1, 1942(s0)
	sh	a1, 1944(s0)
	sh	a1, 1946(s0)
	sh	a1, 1948(s0)
	sh	a1, 1950(s0)
	sh	a1, 1956(s0)
	sh	a1, 1980(s0)
	addi	a1, zero, 30
	sh	a1, 1374(s0)
	sh	a1, 1954(s0)
	sh	a1, 1982(s0)
	sh	a1, 1998(s0)
	addi	a2, zero, 240
	sh	a2, 1688(s0)
	addi	a1, zero, 224
	sh	a1, 1890(s0)
	sh	a1, 1918(s0)
	sh	a1, 2000(s0)
	sw	zero, 104(sp)
	addi	a1, zero, 15
	sw	a1, 108(sp)
	sw	a2, 112(sp)
	addi	a1, zero, 255
	sw	a1, 116(sp)
	lui	a1, 1
	addi	a2, a1, -256
	sw	a2, 120(sp)
	addi	a2, a1, -241
	sw	a2, 124(sp)
	addi	a2, a1, -16
	sw	a2, 128(sp)
	addi	a1, a1, -1
	sw	a1, 132(sp)
	lui	a1, 15
	sw	a1, 136(sp)
	addi	a2, a1, 15
	sw	a2, 140(sp)
	addi	a2, a1, 240
	sw	a2, 144(sp)
	addi	a1, a1, 255
	sw	a1, 148(sp)
	lui	a1, 16
	addi	a2, a1, -256
	sw	a2, 152(sp)
	addi	a2, a1, -241
	sw	a2, 156(sp)
	addi	a2, a1, -16
	sw	a2, 160(sp)
	addi	a1, a1, -1
	sw	a1, 164(sp)
	lui	a1, 240
	sw	a1, 168(sp)
	addi	a2, a1, 15
	sw	a2, 172(sp)
	addi	a2, a1, 240
	sw	a2, 176(sp)
	addi	a1, a1, 255
	sw	a1, 180(sp)
	lui	a1, 241
	addi	a2, a1, -256
	sw	a2, 184(sp)
	addi	a2, a1, -241
	sw	a2, 188(sp)
	addi	a2, a1, -16
	sw	a2, 192(sp)
	addi	a1, a1, -1
	sw	a1, 196(sp)
	lui	a1, 255
	sw	a1, 200(sp)
	addi	a2, a1, 15
	sw	a2, 204(sp)
	addi	a2, a1, 240
	sw	a2, 208(sp)
	addi	a1, a1, 255
	sw	a1, 212(sp)
	lui	a1, 256
	addi	a2, a1, -256
	sw	a2, 216(sp)
	addi	a2, a1, -241
	sw	a2, 220(sp)
	addi	a2, a1, -16
	sw	a2, 224(sp)
	addi	a1, a1, -1
	sw	a1, 228(sp)
	lui	a1, 3840
	sw	a1, 232(sp)
	addi	a2, a1, 15
	sw	a2, 236(sp)
	addi	a2, a1, 240
	sw	a2, 240(sp)
	addi	a1, a1, 255
	sw	a1, 244(sp)
	lui	a1, 3841
	addi	a2, a1, -256
	sw	a2, 248(sp)
	addi	a2, a1, -241
	sw	a2, 252(sp)
	addi	a2, a1, -16
	sw	a2, 256(sp)
	addi	a1, a1, -1
	sw	a1, 260(sp)
	lui	a1, 3855
	sw	a1, 264(sp)
	addi	a2, a1, 15
	sw	a2, 268(sp)
	addi	a2, a1, 240
	sw	a2, 272(sp)
	addi	a1, a1, 255
	sw	a1, 276(sp)
	lui	a1, 3856
	addi	a2, a1, -256
	sw	a2, 280(sp)
	addi	a2, a1, -241
	sw	a2, 284(sp)
	addi	a2, a1, -16
	sw	a2, 288(sp)
	addi	a1, a1, -1
	sw	a1, 292(sp)
	lui	a1, 4080
	sw	a1, 296(sp)
	addi	a2, a1, 15
	sw	a2, 300(sp)
	addi	a2, a1, 240
	sw	a2, 304(sp)
	addi	a1, a1, 255
	sw	a1, 308(sp)
	lui	a1, 4081
	addi	a2, a1, -256
	sw	a2, 312(sp)
	addi	a2, a1, -241
	sw	a2, 316(sp)
	addi	a2, a1, -16
	sw	a2, 320(sp)
	addi	a1, a1, -1
	sw	a1, 324(sp)
	lui	a1, 4095
	sw	a1, 328(sp)
	addi	a2, a1, 15
	sw	a2, 332(sp)
	addi	a2, a1, 240
	sw	a2, 336(sp)
	addi	a1, a1, 255
	sw	a1, 340(sp)
	lui	a1, 4096
	addi	a2, a1, -256
	sw	a2, 344(sp)
	addi	a2, a1, -241
	sw	a2, 348(sp)
	addi	a2, a1, -16
	sw	a2, 352(sp)
	addi	a1, a1, -1
	sw	a1, 356(sp)
	lui	a1, 61440
	sw	a1, 360(sp)
	addi	a2, a1, 15
	sw	a2, 364(sp)
	addi	a2, a1, 240
	sw	a2, 368(sp)
	addi	a1, a1, 255
	sw	a1, 372(sp)
	lui	a1, 61441
	addi	a2, a1, -256
	sw	a2, 376(sp)
	addi	a2, a1, -241
	sw	a2, 380(sp)
	addi	a2, a1, -16
	sw	a2, 384(sp)
	addi	a1, a1, -1
	sw	a1, 388(sp)
	lui	a1, 61455
	sw	a1, 392(sp)
	addi	a2, a1, 15
	sw	a2, 396(sp)
	addi	a2, a1, 240
	sw	a2, 400(sp)
	addi	a1, a1, 255
	sw	a1, 404(sp)
	lui	a1, 61456
	addi	a2, a1, -256
	sw	a2, 408(sp)
	addi	a2, a1, -241
	sw	a2, 412(sp)
	addi	a2, a1, -16
	sw	a2, 416(sp)
	addi	a1, a1, -1
	sw	a1, 420(sp)
	lui	a1, 61680
	sw	a1, 424(sp)
	addi	a2, a1, 15
	sw	a2, 428(sp)
	addi	a2, a1, 240
	sw	a2, 432(sp)
	addi	a1, a1, 255
	sw	a1, 436(sp)
	lui	a1, 61681
	addi	a2, a1, -256
	sw	a2, 440(sp)
	addi	a2, a1, -241
	sw	a2, 444(sp)
	addi	a2, a1, -16
	sw	a2, 448(sp)
	addi	a1, a1, -1
	sw	a1, 452(sp)
	lui	a1, 61695
	sw	a1, 456(sp)
	addi	a2, a1, 15
	sw	a2, 460(sp)
	addi	a2, a1, 240
	sw	a2, 464(sp)
	addi	a1, a1, 255
	sw	a1, 468(sp)
	lui	a1, 61696
	addi	a2, a1, -256
	sw	a2, 472(sp)
	addi	a2, a1, -241
	sw	a2, 476(sp)
	addi	a2, a1, -16
	sw	a2, 480(sp)
	addi	a1, a1, -1
	sw	a1, 484(sp)
	lui	a1, 65280
	sw	a1, 488(sp)
	addi	a2, a1, 15
	sw	a2, 492(sp)
	addi	a2, a1, 240
	sw	a2, 496(sp)
	addi	a1, a1, 255
	sw	a1, 500(sp)
	lui	a1, 65281
	addi	a2, a1, -256
	sw	a2, 504(sp)
	addi	a2, a1, -241
	sw	a2, 508(sp)
	addi	a2, a1, -16
	sw	a2, 512(sp)
	addi	a1, a1, -1
	sw	a1, 516(sp)
	lui	a1, 65295
	sw	a1, 520(sp)
	addi	a2, a1, 15
	sw	a2, 524(sp)
	addi	a2, a1, 240
	sw	a2, 528(sp)
	addi	a1, a1, 255
	sw	a1, 532(sp)
	lui	a1, 65296
	addi	a2, a1, -256
	sw	a2, 536(sp)
	addi	a2, a1, -241
	sw	a2, 540(sp)
	addi	a2, a1, -16
	sw	a2, 544(sp)
	addi	a1, a1, -1
	sw	a1, 548(sp)
	lui	a1, 65520
	sw	a1, 552(sp)
	addi	a2, a1, 15
	sw	a2, 556(sp)
	addi	a2, a1, 240
	sw	a2, 560(sp)
	addi	a1, a1, 255
	sw	a1, 564(sp)
	lui	a1, 65521
	addi	a2, a1, -256
	sw	a2, 568(sp)
	addi	a2, a1, -241
	sw	a2, 572(sp)
	addi	a2, a1, -16
	sw	a2, 576(sp)
	addi	a1, a1, -1
	sw	a1, 580(sp)
	lui	a1, 65535
	sw	a1, 584(sp)
	addi	a2, a1, 15
	sw	a2, 588(sp)
	addi	a2, a1, 240
	sw	a2, 592(sp)
	addi	a1, a1, 255
	sw	a1, 596(sp)
	lui	a1, 65536
	addi	a2, a1, -256
	sw	a2, 600(sp)
	addi	a2, a1, -241
	sw	a2, 604(sp)
	addi	a2, a1, -16
	sw	a2, 608(sp)
	addi	a1, a1, -1
	sw	a1, 612(sp)
	lui	a1, 983040
	sw	a1, 616(sp)
	addi	a2, a1, 15
	sw	a2, 620(sp)
	addi	a2, a1, 240
	sw	a2, 624(sp)
	addi	a1, a1, 255
	sw	a1, 628(sp)
	lui	a1, 983041
	addi	a2, a1, -256
	sw	a2, 632(sp)
	addi	a2, a1, -241
	sw	a2, 636(sp)
	addi	a2, a1, -16
	sw	a2, 640(sp)
	addi	a1, a1, -1
	sw	a1, 644(sp)
	lui	a1, 983055
	sw	a1, 648(sp)
	addi	a2, a1, 15
	sw	a2, 652(sp)
	addi	a2, a1, 240
	sw	a2, 656(sp)
	addi	a1, a1, 255
	sw	a1, 660(sp)
	lui	a1, 983056
	addi	a2, a1, -256
	sw	a2, 664(sp)
	addi	a2, a1, -241
	sw	a2, 668(sp)
	addi	a2, a1, -16
	sw	a2, 672(sp)
	addi	a1, a1, -1
	sw	a1, 676(sp)
	lui	a1, 983280
	sw	a1, 680(sp)
	addi	a2, a1, 15
	sw	a2, 684(sp)
	addi	a2, a1, 240
	sw	a2, 688(sp)
	addi	a1, a1, 255
	sw	a1, 692(sp)
	lui	a1, 983281
	addi	a2, a1, -256
	sw	a2, 696(sp)
	addi	a2, a1, -241
	sw	a2, 700(sp)
	addi	a2, a1, -16
	sw	a2, 704(sp)
	addi	a1, a1, -1
	sw	a1, 708(sp)
	lui	a1, 983295
	sw	a1, 712(sp)
	addi	a2, a1, 15
	sw	a2, 716(sp)
	addi	a2, a1, 240
	sw	a2, 720(sp)
	addi	a1, a1, 255
	sw	a1, 724(sp)
	lui	a1, 983296
	addi	a2, a1, -256
	sw	a2, 728(sp)
	addi	a2, a1, -241
	sw	a2, 732(sp)
	addi	a2, a1, -16
	sw	a2, 736(sp)
	addi	a1, a1, -1
	sw	a1, 740(sp)
	lui	a1, 986880
	sw	a1, 744(sp)
	addi	a2, a1, 15
	sw	a2, 748(sp)
	addi	a2, a1, 240
	sw	a2, 752(sp)
	addi	a1, a1, 255
	sw	a1, 756(sp)
	lui	a1, 986881
	addi	a2, a1, -256
	sw	a2, 760(sp)
	addi	a2, a1, -241
	sw	a2, 764(sp)
	addi	a2, a1, -16
	sw	a2, 768(sp)
	addi	a1, a1, -1
	sw	a1, 772(sp)
	lui	a1, 986895
	sw	a1, 776(sp)
	addi	a2, a1, 15
	sw	a2, 780(sp)
	addi	a2, a1, 240
	sw	a2, 784(sp)
	addi	a1, a1, 255
	sw	a1, 788(sp)
	lui	a1, 986896
	addi	a2, a1, -256
	sw	a2, 792(sp)
	addi	a2, a1, -241
	sw	a2, 796(sp)
	addi	a2, a1, -16
	sw	a2, 800(sp)
	addi	a1, a1, -1
	sw	a1, 804(sp)
	lui	a1, 987120
	sw	a1, 808(sp)
	addi	a2, a1, 15
	sw	a2, 812(sp)
	addi	a2, a1, 240
	sw	a2, 816(sp)
	addi	a1, a1, 255
	sw	a1, 820(sp)
	lui	a1, 987121
	addi	a2, a1, -256
	sw	a2, 824(sp)
	addi	a2, a1, -241
	sw	a2, 828(sp)
	addi	a2, a1, -16
	sw	a2, 832(sp)
	addi	a1, a1, -1
	sw	a1, 836(sp)
	lui	a1, 987135
	sw	a1, 840(sp)
	addi	a2, a1, 15
	sw	a2, 844(sp)
	addi	a2, a1, 240
	sw	a2, 848(sp)
	addi	a1, a1, 255
	sw	a1, 852(sp)
	lui	a1, 987136
	addi	a2, a1, -256
	sw	a2, 856(sp)
	addi	a2, a1, -241
	sw	a2, 860(sp)
	addi	a2, a1, -16
	sw	a2, 864(sp)
	addi	a1, a1, -1
	sw	a1, 868(sp)
	lui	a1, 1044480
	sw	a1, 872(sp)
	addi	a2, a1, 15
	sw	a2, 876(sp)
	addi	a2, a1, 240
	sw	a2, 880(sp)
	addi	a1, a1, 255
	sw	a1, 884(sp)
	lui	a1, 1044481
	addi	a2, a1, -256
	sw	a2, 888(sp)
	addi	a2, a1, -241
	sw	a2, 892(sp)
	addi	a2, a1, -16
	sw	a2, 896(sp)
	addi	a1, a1, -1
	sw	a1, 900(sp)
	lui	a1, 1044495
	sw	a1, 904(sp)
	addi	a2, a1, 15
	sw	a2, 908(sp)
	addi	a2, a1, 240
	sw	a2, 912(sp)
	addi	a1, a1, 255
	sw	a1, 916(sp)
	lui	a1, 1044496
	addi	a2, a1, -256
	sw	a2, 920(sp)
	addi	a2, a1, -241
	sw	a2, 924(sp)
	addi	a2, a1, -16
	sw	a2, 928(sp)
	addi	a1, a1, -1
	sw	a1, 932(sp)
	lui	a1, 1044720
	sw	a1, 936(sp)
	addi	a2, a1, 15
	sw	a2, 940(sp)
	addi	a2, a1, 240
	sw	a2, 944(sp)
	addi	a1, a1, 255
	sw	a1, 948(sp)
	lui	a1, 1044721
	addi	a2, a1, -256
	sw	a2, 952(sp)
	addi	a2, a1, -241
	sw	a2, 956(sp)
	addi	a2, a1, -16
	sw	a2, 960(sp)
	addi	a1, a1, -1
	sw	a1, 964(sp)
	lui	a1, 1044735
	sw	a1, 968(sp)
	addi	a2, a1, 15
	sw	a2, 972(sp)
	addi	a2, a1, 240
	sw	a2, 976(sp)
	addi	a1, a1, 255
	sw	a1, 980(sp)
	lui	a1, 1044736
	addi	a2, a1, -256
	sw	a2, 984(sp)
	addi	a2, a1, -241
	sw	a2, 988(sp)
	addi	a2, a1, -16
	sw	a2, 992(sp)
	addi	a1, a1, -1
	sw	a1, 996(sp)
	lui	a1, 1048320
	sw	a1, 1000(sp)
	addi	a2, a1, 15
	sw	a2, 1004(sp)
	addi	a2, a1, 240
	sw	a2, 1008(sp)
	addi	a1, a1, 255
	sw	a1, 1012(sp)
	lui	a1, 1048321
	addi	a2, a1, -256
	sw	a2, 1016(sp)
	addi	a2, a1, -241
	sw	a2, 1020(sp)
	addi	a2, a1, -16
	sw	a2, 1024(sp)
	addi	a1, a1, -1
	sw	a1, 1028(sp)
	lui	a1, 1048335
	sw	a1, 1032(sp)
	addi	a2, a1, 15
	sw	a2, 1036(sp)
	addi	a2, a1, 240
	sw	a2, 1040(sp)
	addi	a1, a1, 255
	sw	a1, 1044(sp)
	lui	a1, 1048336
	addi	a2, a1, -256
	sw	a2, 1048(sp)
	addi	a2, a1, -241
	sw	a2, 1052(sp)
	addi	a2, a1, -16
	sw	a2, 1056(sp)
	addi	a1, a1, -1
	sw	a1, 1060(sp)
	lui	a1, 1048560
	sw	a1, 1064(sp)
	addi	a2, a1, 15
	sw	a2, 1068(sp)
	addi	a2, a1, 240
	sw	a2, 1072(sp)
	addi	a1, a1, 255
	sw	a1, 1076(sp)
	lui	a1, 1048561
	addi	a2, a1, -256
	sw	a2, 1080(sp)
	addi	a2, a1, -241
	sw	a2, 1084(sp)
	addi	a2, a1, -16
	sw	a2, 1088(sp)
	addi	a1, a1, -1
	sw	a1, 1092(sp)
	lui	a1, 1048575
	sw	a1, 1096(sp)
	addi	a2, a1, 15
	sw	a2, 1100(sp)
	addi	a2, a1, 240
	sw	a2, 1104(sp)
	addi	a1, a1, 255
	sw	a1, 1108(sp)
	addi	a1, zero, -256
	sw	a1, 1112(sp)
	addi	a1, zero, -241
	sw	a1, 1116(sp)
	addi	a1, zero, -16
	sw	a1, 1120(sp)
	sh	s1, 1026(s0)
	sh	zero, 1500(s0)
	sh	zero, 1498(s0)
	sh	s6, 1028(s0)
	sh	zero, 1030(s0)
	sh	zero, 1032(s0)
	sh	zero, 1034(s0)
	sh	zero, 1036(s0)
	sh	zero, 1038(s0)
	sh	zero, 1040(s0)
	sh	zero, 1042(s0)
	sh	zero, 1044(s0)
	sh	zero, 1478(s0)
	sh	zero, 1476(s0)
	sh	zero, 1474(s0)
	sh	zero, 1472(s0)
	sh	zero, 1470(s0)
	sh	zero, 1468(s0)
	sh	zero, 1466(s0)
	sh	zero, 1046(s0)
	sh	zero, 1048(s0)
	sh	zero, 1050(s0)
	sh	zero, 1052(s0)
	sh	zero, 1054(s0)
	sh	zero, 1056(s0)
	sh	zero, 1058(s0)
	sh	zero, 1060(s0)
	sh	zero, 1062(s0)
	sh	zero, 1446(s0)
	sh	zero, 1444(s0)
	sh	zero, 1442(s0)
	sh	zero, 1440(s0)
	sh	zero, 1438(s0)
	sh	zero, 1436(s0)
	sh	zero, 1434(s0)
	sh	zero, 1082(s0)
	sh	s1, 1430(s0)
	sh	s1, 1428(s0)
	sh	s1, 1426(s0)
	sh	s1, 1424(s0)
	sh	s1, 1422(s0)
	sh	s1, 1420(s0)
	sh	s1, 1418(s0)
	sh	s1, 1416(s0)
	sh	s1, 1414(s0)
	sh	s1, 1412(s0)
	addi	a1, zero, 60
	sh	a1, 1410(s0)
	sh	zero, 1408(s0)
	sh	zero, 1406(s0)
	sh	zero, 1404(s0)
	sh	zero, 1402(s0)
	sh	zero, 1084(s0)
	sh	zero, 1086(s0)
	sh	zero, 1088(s0)
	sh	s9, 1090(s0)
	sh	a1, 1392(s0)
	addi	s2, zero, 60
	sh	s9, 1092(s0)
	sh	s9, 1094(s0)
	sh	zero, 1114(s0)
	sh	zero, 1116(s0)
	sh	zero, 1118(s0)
	sh	zero, 1120(s0)
	sh	zero, 1122(s0)
	sh	zero, 1376(s0)
	sh	zero, 1124(s0)
	sh	s1, 1372(s0)
	sh	s6, 1370(s0)
	sh	zero, 1126(s0)
	sh	s9, 1132(s0)
	sh	s9, 1134(s0)
	sh	s9, 1136(s0)
	sh	s9, 1138(s0)
	sh	s9, 1140(s0)
	sh	zero, 1146(s0)
	sh	zero, 1148(s0)
	sh	zero, 1150(s0)
	sh	zero, 1350(s0)
	sh	zero, 1152(s0)
	sh	zero, 1178(s0)
	sh	zero, 1344(s0)
	sh	zero, 1342(s0)
	sh	zero, 1340(s0)
	sh	zero, 1338(s0)
	sh	zero, 1180(s0)
	sh	s1, 1334(s0)
	sh	s1, 1332(s0)
	sh	s1, 1330(s0)
	sh	s1, 1328(s0)
	sh	s1, 1326(s0)
	sh	s1, 1324(s0)
	sh	s1, 1322(s0)
	sh	s2, 1320(s0)
	sh	zero, 1318(s0)
	sh	s1, 1316(s0)
	sh	s1, 1314(s0)
	sh	zero, 1312(s0)
	sh	zero, 1310(s0)
	sh	zero, 1308(s0)
	sh	zero, 1306(s0)
	sh	zero, 1182(s0)
	sh	zero, 1184(s0)
	sh	zero, 1186(s0)
	sh	zero, 1188(s0)
	sh	zero, 1190(s0)
	sh	s9, 1202(s0)
	sh	s9, 1204(s0)
	sh	zero, 1210(s0)
	sh	zero, 1212(s0)
	sh	s9, 1286(s0)
	sh	s9, 1284(s0)
	sh	s9, 1282(s0)
	sh	zero, 1280(s0)
	sh	zero, 1214(s0)
	sh	zero, 1216(s0)
	sh	zero, 1242(s0)
	sh	zero, 1244(s0)
	sh	zero, 1246(s0)
	sh	zero, 1248(s0)
	sh	zero, 1250(s0)
	sh	zero, 1252(s0)
	sh	zero, 1254(s0)
	sh	zero, 1502(s0)
	sh	zero, 1504(s0)
	sh	zero, 1506(s0)
	sh	zero, 1508(s0)
	sh	zero, 1510(s0)
	sh	zero, 1530(s0)
	sh	zero, 1532(s0)
	sh	zero, 1534(s0)
	sh	zero, 1536(s0)
	sh	zero, 1538(s0)
	sh	zero, 1540(s0)
	sh	zero, 1542(s0)
	sh	s9, 1562(s0)
	sh	s9, 1564(s0)
	sh	s9, 1566(s0)
	sh	zero, 1568(s0)
	sh	zero, 1570(s0)
	sh	zero, 1572(s0)
	sh	zero, 1574(s0)
	sh	zero, 1600(s0)
	sh	zero, 1602(s0)
	sh	zero, 1604(s0)
	sh	zero, 1606(s0)
	addi	a1, zero, 12
	sh	a1, 1612(s0)
	sh	a1, 1614(s0)
	sh	a1, 1616(s0)
	sh	a1, 1618(s0)
	sh	a1, 1620(s0)
	sh	a1, 1622(s0)
	sh	a1, 1624(s0)
	sh	zero, 1626(s0)
	sh	zero, 1628(s0)
	sh	zero, 1630(s0)
	sh	zero, 1632(s0)
	sh	zero, 1634(s0)
	sh	zero, 1636(s0)
	sh	zero, 1638(s0)
	sh	a1, 1646(s0)
	addi	a1, zero, 12
	sh	zero, 1658(s0)
	sh	zero, 1660(s0)
	sh	zero, 1662(s0)
	sh	zero, 1664(s0)
	sh	zero, 1666(s0)
	sh	zero, 1690(s0)
	sh	zero, 1692(s0)
	sh	zero, 1694(s0)
	sh	zero, 1696(s0)
	sh	zero, 1698(s0)
	sh	zero, 1700(s0)
	sh	zero, 1702(s0)
	sh	zero, 1722(s0)
	sh	zero, 1724(s0)
	sh	zero, 1726(s0)
	sh	zero, 1728(s0)
	sh	zero, 1730(s0)
	sh	zero, 1732(s0)
	sh	zero, 1734(s0)
	sh	s1, 1752(s0)
	sh	zero, 1754(s0)
	sh	zero, 1756(s0)
	sh	zero, 1758(s0)
	sh	zero, 1760(s0)
	sh	zero, 1762(s0)
	sh	zero, 1764(s0)
	sh	zero, 1766(s0)
	sh	zero, 1786(s0)
	sh	zero, 1788(s0)
	sh	zero, 1790(s0)
	sh	zero, 1792(s0)
	sh	zero, 1794(s0)
	sh	zero, 1796(s0)
	sh	zero, 1798(s0)
	sh	s1, 1808(s0)
	sh	zero, 1818(s0)
	sh	zero, 1820(s0)
	sh	zero, 1822(s0)
	sh	zero, 1824(s0)
	sh	zero, 1826(s0)
	sh	zero, 1828(s0)
	sh	zero, 1830(s0)
	sh	s1, 1846(s0)
	sh	s1, 1848(s0)
	sh	zero, 1856(s0)
	sh	zero, 1858(s0)
	sh	zero, 1860(s0)
	sh	zero, 1862(s0)
	sh	s6, 1870(s0)
	sh	a1, 1876(s0)
	sh	zero, 1882(s0)
	sh	zero, 1884(s0)
	sh	zero, 1886(s0)
	sh	zero, 1888(s0)
	sh	s1, 1892(s0)
	sh	a1, 1904(s0)
	sh	s1, 1916(s0)
	sh	zero, 1920(s0)
	sh	zero, 1952(s0)
	sh	s1, 1958(s0)
	sh	s1, 1960(s0)
	sh	s1, 1962(s0)
	sh	s1, 1964(s0)
	sh	s6, 1966(s0)
	sh	s6, 1970(s0)
	sh	s1, 1972(s0)
	sh	s1, 1974(s0)
	sh	s1, 1976(s0)
	sh	s1, 1978(s0)
	sh	zero, 1984(s0)
	sh	zero, 1986(s0)
	sh	zero, 1988(s0)
	sh	zero, 1990(s0)
	sh	zero, 1992(s0)
	sh	zero, 1994(s0)
	sh	zero, 1996(s0)
	sh	zero, 2002(s0)
	sh	zero, 2004(s0)
	sh	zero, 2006(s0)
	sh	zero, 2008(s0)
	sh	zero, 2010(s0)
	sh	zero, 2012(s0)
	sh	zero, 2014(s0)
	addi	a1, zero, -1
	sw	a1, 1124(sp)
	sw	zero, 100(sp)
	sw	zero, 96(sp)
	lw	s9, 100(sp)
	mv	s0, a0
	mv	a0, zero
	mv	a1, zero
	mv	a2, zero
	mv	a3, zero
	mv	a4, zero
	call	month_length
	sw	a0, 40(sp)
	addi	a0, zero, 73
	sb	a0, 80(sp)
	addi	a0, zero, 116
	sb	a0, 81(sp)
	sb	s6, 82(sp)
	addi	a0, zero, 119
	sb	a0, 83(sp)
	addi	a0, zero, 111
	sb	a0, 84(sp)
	addi	a0, zero, 114
	sb	a0, 85(sp)
	addi	a0, zero, 107
	sb	a0, 86(sp)
	addi	a0, zero, 115
	sb	a0, 87(sp)
	addi	a0, zero, 33
	sb	a0, 88(sp)
	sb	s6, 89(sp)
	sb	s2, 90(sp)
	addi	a0, zero, 51
	sb	a0, 91(sp)
	sb	zero, 92(sp)
	sb	s1, 56(sp)
	sb	s1, 57(sp)
	addi	a0, zero, 58
	sb	a0, 58(sp)
	sb	s1, 59(sp)
	sb	s1, 60(sp)
	sb	a0, 61(sp)
	sb	s1, 62(sp)
	sb	s1, 63(sp)
	sb	s6, 64(sp)
	sb	s1, 65(sp)
	addi	a0, zero, 49
	sb	a0, 66(sp)
	addi	a1, zero, 47
	sb	a1, 67(sp)
	sb	s1, 68(sp)
	sb	a0, 69(sp)
	sb	a1, 70(sp)
	sb	s1, 71(sp)
	sb	s1, 72(sp)
	sb	s1, 73(sp)
	sb	s1, 74(sp)
	sb	zero, 75(sp)
	addi	s3, sp, 104
	addi	a4, sp, 80
	addi	a2, zero, 28
	addi	a3, zero, 20
	addi	s1, sp, 1128
	mv	a0, s1
	mv	a1, s3
	mv	a5, s4
	mv	a6, s0
	call	display_text
	addi	a4, sp, 56
	addi	a2, zero, 20
	addi	a3, zero, 460
	mv	a0, s1
	mv	a1, s3
	mv	a5, s4
	mv	a6, s0
	call	display_text
	mv	s5, zero
	mv	s2, zero
	mv	s6, zero
	mv	s8, zero
	mv	s11, zero
	mv	s10, zero
	sw	zero, 52(sp)
	sw	zero, 44(sp)
	sw	zero, 48(sp)
	sw	zero, 32(sp)
	sw	zero, 36(sp)
	sw	zero, 16(sp)
	sw	zero, 8(sp)
	sw	zero, 28(sp)
	sw	zero, 24(sp)
	sw	zero, 12(sp)
	sw	zero, 20(sp)
	addi	s7, zero, 10
	j	.LBB5_9
.LBB5_1:                                #   in Loop: Header=BB5_9 Depth=1
	lw	a0, 52(sp)
	addi	a1, a0, 1
	addi	a0, a0, 49
	sw	a1, 52(sp)
.LBB5_2:                                #   in Loop: Header=BB5_9 Depth=1
	andi	a4, a0, 255
	addi	a2, zero, 20
	addi	a3, zero, 460
	mv	a0, s1
	mv	a1, s3
	mv	a5, s4
	mv	a6, s0
	call	display_char
	mv	s10, zero
.LBB5_3:                                #   in Loop: Header=BB5_9 Depth=1
	addi	a0, s10, 48
	andi	a4, a0, 255
	addi	a2, zero, 22
	addi	a3, zero, 460
	mv	a0, s1
	mv	a1, s3
	mv	a5, s4
	mv	a6, s0
	call	display_char
	mv	s11, zero
.LBB5_4:                                #   in Loop: Header=BB5_9 Depth=1
	addi	a0, s11, 48
	andi	a4, a0, 255
	addi	a2, zero, 26
	addi	a3, zero, 460
	mv	a0, s1
	mv	a1, s3
	mv	a5, s4
	mv	a6, s0
	call	display_char
	mv	s8, zero
.LBB5_5:                                #   in Loop: Header=BB5_9 Depth=1
	addi	a0, s8, 48
	andi	a4, a0, 255
	addi	a2, zero, 28
	addi	a3, zero, 460
	mv	a0, s1
	mv	a1, s3
	mv	a5, s4
	mv	a6, s0
	call	display_char
	mv	s6, zero
.LBB5_6:                                #   in Loop: Header=BB5_9 Depth=1
	addi	a0, s6, 48
	andi	a4, a0, 255
	addi	a2, zero, 32
	addi	a3, zero, 460
	mv	a0, s1
	mv	a1, s3
	mv	a5, s4
	mv	a6, s0
	call	display_char
	mv	s2, zero
.LBB5_7:                                #   in Loop: Header=BB5_9 Depth=1
	addi	a0, s2, 48
	andi	a4, a0, 255
	addi	a2, zero, 34
	addi	a3, zero, 460
	mv	a0, s1
	mv	a1, s3
	mv	a5, s4
	mv	a6, s0
	call	display_char
.LBB5_8:                                #   in Loop: Header=BB5_9 Depth=1
	lw	s9, 100(sp)
.LBB5_9:                                # =>This Inner Loop Header: Depth=1
	lui	a0, 1
	addi	a0, a0, 72
	add	a0, sp, a0
	mv	a0, a0
	mv	a1, zero
	call	xorio_
	lui	a0, 1
	addi	a0, a0, 72
	add	a0, sp, a0
	lw	a0, 0(a0)
	andi	a1, a0, 2
	sw	a1, 96(sp)
	lw	a1, 96(sp)
	beqz	a1, .LBB5_22
.LBB5_10:                               #   in Loop: Header=BB5_9 Depth=1
	addi	s2, s2, 1
	addi	s1, sp, 1128
	bne	s2, s7, .LBB5_7
# %bb.11:                               #   in Loop: Header=BB5_9 Depth=1
	addi	s6, s6, 1
	addi	a0, zero, 6
	bne	s6, a0, .LBB5_6
# %bb.12:                               #   in Loop: Header=BB5_9 Depth=1
	addi	s8, s8, 1
	bne	s8, s7, .LBB5_5
# %bb.13:                               #   in Loop: Header=BB5_9 Depth=1
	addi	s11, s11, 1
	addi	a0, zero, 6
	bne	s11, a0, .LBB5_4
# %bb.14:                               #   in Loop: Header=BB5_9 Depth=1
	addi	s10, s10, 1
	beq	s10, s7, .LBB5_1
# %bb.15:                               #   in Loop: Header=BB5_9 Depth=1
	lw	a0, 52(sp)
	xori	a0, a0, 2
	xori	a1, s10, 4
	or	a0, a0, a1
	bnez	a0, .LBB5_3
# %bb.16:                               #   in Loop: Header=BB5_9 Depth=1
	lw	a1, 44(sp)
	addi	a1, a1, 1
	lw	a2, 48(sp)
	addi	a2, a2, 1
	lw	a0, 40(sp)
	bne	a1, a0, .LBB5_25
# %bb.17:                               #   in Loop: Header=BB5_9 Depth=1
	lw	s9, 36(sp)
	addi	s9, s9, 1
	addi	a0, zero, 12
	bne	s9, a0, .LBB5_27
# %bb.18:                               #   in Loop: Header=BB5_9 Depth=1
	lw	s8, 28(sp)
	addi	s8, s8, 1
	bne	s8, s7, .LBB5_29
# %bb.19:                               #   in Loop: Header=BB5_9 Depth=1
	lw	s6, 24(sp)
	addi	s6, s6, 1
	bne	s6, s7, .LBB5_31
# %bb.20:                               #   in Loop: Header=BB5_9 Depth=1
	lw	a0, 12(sp)
	addi	a0, a0, 1
	sw	a0, 12(sp)
	beq	a0, s7, .LBB5_32
# %bb.21:                               #   in Loop: Header=BB5_9 Depth=1
	lw	s2, 20(sp)
	j	.LBB5_35
.LBB5_22:                               #   in Loop: Header=BB5_9 Depth=1
	andi	a0, a0, 1
	sw	a0, 100(sp)
	lw	a0, 100(sp)
	seqz	a0, a0
	snez	a1, s9
	or	a0, a1, a0
	bnez	a0, .LBB5_8
# %bb.23:                               #   in Loop: Header=BB5_9 Depth=1
	addi	s5, s5, 1
	addi	a0, zero, 1000
	bne	s5, a0, .LBB5_8
# %bb.24:                               #   in Loop: Header=BB5_9 Depth=1
	mv	s5, zero
	j	.LBB5_10
.LBB5_25:                               #   in Loop: Header=BB5_9 Depth=1
	sw	a1, 44(sp)
	bne	a2, s7, .LBB5_41
# %bb.26:                               #   in Loop: Header=BB5_9 Depth=1
	lw	a0, 32(sp)
	addi	a1, a0, 1
	addi	a0, a0, 49
	sw	a1, 32(sp)
	j	.LBB5_40
.LBB5_27:                               #   in Loop: Header=BB5_9 Depth=1
	lw	s10, 16(sp)
	addi	s10, s10, 1
	bne	s10, s7, .LBB5_30
# %bb.28:                               #   in Loop: Header=BB5_9 Depth=1
	lw	a0, 8(sp)
	addi	a1, a0, 1
	addi	a0, a0, 49
	sw	a1, 8(sp)
	lw	s2, 20(sp)
	lw	s6, 24(sp)
	lw	s8, 28(sp)
	j	.LBB5_38
.LBB5_29:                               #   in Loop: Header=BB5_9 Depth=1
	lw	s2, 20(sp)
	lw	s6, 24(sp)
	j	.LBB5_37
.LBB5_30:                               #   in Loop: Header=BB5_9 Depth=1
	lw	s2, 20(sp)
	lw	s6, 24(sp)
	lw	s8, 28(sp)
	j	.LBB5_39
.LBB5_31:                               #   in Loop: Header=BB5_9 Depth=1
	lw	s2, 20(sp)
	j	.LBB5_36
.LBB5_32:                               #   in Loop: Header=BB5_9 Depth=1
	lw	a0, 20(sp)
	addi	a0, a0, 1
	mv	s2, zero
	beq	a0, s7, .LBB5_34
# %bb.33:                               #   in Loop: Header=BB5_9 Depth=1
	mv	s2, a0
.LBB5_34:                               #   in Loop: Header=BB5_9 Depth=1
	addi	a0, s2, 48
	andi	a4, a0, 255
	addi	a2, zero, 50
	addi	a3, zero, 460
	mv	a0, s1
	mv	a1, s3
	mv	a5, s4
	mv	a6, s0
	call	display_char
	sw	zero, 12(sp)
.LBB5_35:                               #   in Loop: Header=BB5_9 Depth=1
	lw	a0, 12(sp)
	addi	a0, a0, 48
	andi	a4, a0, 255
	addi	a2, zero, 52
	addi	a3, zero, 460
	mv	a0, s1
	mv	a1, s3
	mv	a5, s4
	mv	a6, s0
	call	display_char
	mv	s6, zero
.LBB5_36:                               #   in Loop: Header=BB5_9 Depth=1
	addi	a0, s6, 48
	andi	a4, a0, 255
	addi	a2, zero, 54
	addi	a3, zero, 460
	mv	a0, s1
	mv	a1, s3
	mv	a5, s4
	mv	a6, s0
	call	display_char
	mv	s8, zero
.LBB5_37:                               #   in Loop: Header=BB5_9 Depth=1
	addi	a0, s8, 48
	andi	a4, a0, 255
	addi	a2, zero, 56
	addi	a3, zero, 460
	mv	a0, s1
	mv	a1, s3
	mv	a5, s4
	mv	a6, s0
	call	display_char
	mv	s9, zero
	sw	zero, 8(sp)
	addi	a0, zero, 48
.LBB5_38:                               #   in Loop: Header=BB5_9 Depth=1
	andi	a4, a0, 255
	addi	a2, zero, 44
	addi	a3, zero, 460
	mv	a0, s1
	mv	a1, s3
	mv	a5, s4
	mv	a6, s0
	call	display_char
	mv	s10, zero
.LBB5_39:                               #   in Loop: Header=BB5_9 Depth=1
	sw	s9, 36(sp)
	mv	a0, s9
	sw	s8, 28(sp)
	mv	a1, s8
	sw	s6, 24(sp)
	mv	a2, s6
	lw	a3, 12(sp)
	sw	s2, 20(sp)
	mv	a4, s2
	call	month_length
	sw	a0, 40(sp)
	sw	s10, 16(sp)
	addi	a0, s10, 48
	andi	a4, a0, 255
	addi	a2, zero, 46
	addi	a3, zero, 460
	mv	a0, s1
	mv	a1, s3
	mv	a5, s4
	mv	a6, s0
	call	display_char
	sw	zero, 44(sp)
	sw	zero, 32(sp)
	addi	a0, zero, 48
.LBB5_40:                               #   in Loop: Header=BB5_9 Depth=1
	andi	a4, a0, 255
	addi	a2, zero, 38
	addi	a3, zero, 460
	mv	a0, s1
	mv	a1, s3
	mv	a5, s4
	mv	a6, s0
	call	display_char
	mv	a2, zero
.LBB5_41:                               #   in Loop: Header=BB5_9 Depth=1
	sw	a2, 48(sp)
	addi	a0, a2, 48
	andi	a4, a0, 255
	addi	a2, zero, 40
	addi	a3, zero, 460
	mv	a0, s1
	mv	a1, s3
	mv	a5, s4
	mv	a6, s0
	call	display_char
	sw	zero, 52(sp)
	addi	a0, zero, 48
	j	.LBB5_2
.Lfunc_end5:
	.size	main, .Lfunc_end5-main
                                        # -- End function
	.type	ram,@object                     # @ram
	.bss
	.globl	ram
	.p2align	2
ram:
	.zero	153600
	.size	ram, 153600

	.ident	"Debian clang version 11.0.1-2"
	.section	".note.GNU-stack","",@progbits
	.addrsig
	.addrsig_sym ram
