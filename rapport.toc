\contentsline {section}{\numberline {1}Instruction Set Architecture}{1}%
\contentsline {subsection}{\numberline {1.1}RV32I Base}{1}%
\contentsline {subsection}{\numberline {1.2}Inputs and Outputs}{1}%
\contentsline {section}{\numberline {2}Schematic}{2}%
\contentsline {subsection}{\numberline {2.1}Programm Counter}{2}%
\contentsline {subsection}{\numberline {2.2}RAM Manager}{2}%
\contentsline {subsection}{\numberline {2.3}Register Manager}{2}%
\contentsline {subsection}{\numberline {2.4}Critical Path}{2}%
\contentsline {section}{\numberline {3}VGA Display}{2}%
\contentsline {section}{\numberline {4}Clock}{2}%
\contentsline {subsection}{\numberline {4.1}Compiling}{4}%
\contentsline {subsubsection}{\numberline {4.1.1}To RISC-V assembly}{4}%
\contentsline {subsubsection}{\numberline {4.1.2}To machine code}{4}%
\contentsline {subsection}{\numberline {4.2}Inputs}{4}%
\contentsline {subsection}{\numberline {4.3}Char display}{4}%
\contentsline {subsubsection}{\numberline {4.3.1}Font in memory}{4}%
\contentsline {subsubsection}{\numberline {4.3.2}Setting the palette}{4}%
\contentsline {subsubsection}{\numberline {4.3.3}Setting the RAM}{4}%
