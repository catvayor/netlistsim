all:
	make C
	make mv

C:
	make -C clang-riscv
	cat prelude.s clang-riscv/clock.s > clang-assembler/test.s
mv:
	make -C clang-assembler
	cat clang-assembler/out > vivado_prj/vivado_prj.srcs/memory.coe
	rm -fr vivado_prj/vivado_prj.cache vivado_prj/vivado_prj.gen
