
-- elementary block
entity bit_reg is
    port ( outD : out bit;
           inD, clk : in bit );
end bit_reg;

architecture basic of bit_reg is
    signal reg : bit;
begin
    P : process(clk) is
        begin
            if clk'event and clk = '1' then
                reg <= inD;
            end if;
        end process;
        
    outD <= reg;
end basic;

entity bit_vect_reg is
    generic ( width : positive );
    port ( outD : out bit_vector (0 to width - 1);
           inD : in bit_vector(0 to width - 1);
           clk : in bit );
end bit_vect_reg;

architecture basic of bit_vect_reg is
    signal reg : bit_vector(0 to width - 1);
begin
    P : process(clk) is
        begin
            if clk'event and clk = '1' then
                reg <= inD;
            end if;
        end process;
        
    outD <= reg;
end basic;

entity bit_treg is
    port ( outD : out bit;
           inD, we, clk : in bit );
end bit_treg;

architecture basic of bit_treg is
    signal reg : bit;
begin
    P : process(clk) is
        begin
            if clk'event and clk = '1' and we = '1' then
                reg <= inD;
            end if;
        end process;
        
    outD <= reg;
end basic;

entity bit_vect_treg is
    generic ( width : positive );
    port ( outD : out bit_vector (0 to width - 1);
           inD : in bit_vector(0 to width - 1);
           clk, we : in bit );
end bit_vect_treg;

architecture basic of bit_vect_treg is
    signal reg : bit_vector(0 to width - 1);
begin
    P : process(clk) is
        begin
            if clk'event and clk = '1' and we = '1' then
                reg <= inD;
            end if;
        end process;
        
    outD <= reg;
end basic;
