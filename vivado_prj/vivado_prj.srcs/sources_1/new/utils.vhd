----------------------------------------------------------------------------------
--
-- Create Date: 11/20/2021 12:09:33 PM
--
-- Module Name: UTILS
-- Project Name: Processeur SysNum
--
----------------------------------------------------------------------------------

entity exists is
    generic (busSize: natural);
    port ( input : in bit_vector (0 to busSize - 1);
           result : out bit );
end exists;

entity forall is
    generic (busSize: natural);
    port ( input : in bit_vector (0 to busSize - 1);
           result : out bit );
end forall;

entity mux is
    port ( in0, in1 : in bit;
           chooser : in bit;
           result : out bit );
end mux;

entity bmux is
    generic ( busSize : positive );
    port ( in0, in1 : in bit_vector (0 to busSize - 1);
           chooser : in bit;
           result : out bit_vector (0 to busSize - 1) );
end bmux;

entity lt is
    generic ( wordRank : positive );
    port ( a : in bit_vector (0 to 2**wordRank - 1);
           b : in bit_vector (0 to 2**wordRank - 1);
           result : out bit);
end lt;

entity ltu is
    generic ( wordRank : positive );
    port ( a : in bit_vector (0 to 2**wordRank - 1);
           b : in bit_vector (0 to 2**wordRank - 1);
           result : out bit);
end ltu;

entity incr is
    generic ( wordRank : positive );
    port ( a : in bit_vector (0 to 2**wordRank - 1);
           result : out bit_vector (0 to 2**wordRank - 1);
           overflow : out bit );
end incr;

entity dac is
    generic ( wordRank : positive );
    port ( clk_dac : in bit;
           data : in bit_vector(0 to 2**wordRank - 1);
           result : out bit);
end dac;


architecture basic of exists is
    signal a, b: bit;
begin
    init0: if (busSize = 0) generate
        result <= '0';
    end generate;
    init: if (busSize = 1) generate
        result <= input(0);
    end generate;
    rec: if (busSize > 1) generate
        part_a: entity work.exists
            generic map (busSize => busSize/2)
            port map ( input => input(0 to busSize/2-1),
                       result => a );
        part_b: entity work.exists
            generic map (busSize => busSize-busSize/2)
            port map ( input => input(busSize/2 to busSize-1),
                       result => b );
        result <= a or b;
    end generate;
end basic;


architecture basic of forall is
    signal a, b: bit;
begin
    init0: if (busSize = 0) generate
        result <= '1';
    end generate;
    init: if (busSize = 1) generate
        result <= input(0);
    end generate;
    rec: if (busSize > 1) generate
        part_a: entity work.forall
            generic map (busSize => busSize/2)
            port map ( input => input(0 to busSize/2-1),
                       result => a );
        part_b: entity work.forall
            generic map (busSize => busSize-busSize/2)
            port map ( input => input(busSize/2 to busSize-1),
                       result => b );
        result <= a and b;
    end generate;
end basic;



architecture basic of mux is
begin
    
    result <= ( in0 and (not chooser) ) or ( in1 and chooser );

end basic;

architecture basic of bmux is
begin

    MuxList : for i in 0 to busSize - 1 generate
        result(i) <= ( in0(i) and (not chooser) ) or ( in1(i) and chooser );
    end generate;

end basic;


architecture basic of lt is
    signal tmp : bit_vector (0 to 2**wordRank - 1);
begin
    sub: entity work.arithUnit
        generic map ( wordRank => wordRank )
        port map (inA => a, inB => b,
                  sub => '1',
                  result => tmp );
    result <= tmp(2**wordRank - 1);
end basic;


architecture basic of ltu is
    signal nret : bit;
begin
    sub: entity work.arithUnit
        generic map ( wordRank => wordRank )
        port map (inA => a, inB => b,
                  sub => '1',
                  carryOut => nret );
    result <= not nret;
end basic;


architecture basic of incr is
begin
    sub: entity work.adder
        generic map ( wordRank => wordRank )
        port map ( inA => a,
                   inB => (0 => '1', others => '0'),
                   carryIn => '0',
                   carryOut => overflow,
                   result => result);
end basic;


        
-- note: "00...00" -> always '0', BUT "11...11" -> mostly '1'
architecture basic of dac is
    constant wordSize : positive := 2**wordRank;
    
    signal cnt, cnt_incr, next_cnt, rev_cnt : bit_vector(0 to wordSize - 1);
    
    signal cmp_ret, cnt_rst : bit;
begin
    counter: entity work.bit_vect_reg
        generic map ( width => wordSize )
        port map ( outD => cnt, inD => next_cnt, clk => clk_dac );
        
    cnt_reset: entity work.forall
        generic map ( busSize => 2**wordRank )
        port map ( input => "1"&cnt(1 to wordSize - 1), result => cnt_rst );
    
    increment: entity work.incr
        generic map ( wordRank => wordRank )
        port map ( a => cnt,
                   result => cnt_incr);
    
    cnt_mux: entity work.bmux
        generic map ( busSize => wordSize )
        port map (
            in0 => cnt_incr,
            in1 => (others => '0'),
            chooser => cnt_rst,
            result => next_cnt );
    
    -- permutation
    Reverse : for i in 0 to wordSize - 1 generate
        rev_cnt(wordSize - 1 - i) <= cnt(i);
    end generate;

    -- calc
    cmp : entity work.ltu
        generic map (wordRank => wordRank )
        port map ( a => rev_cnt, b => data, result => cmp_ret);
        
    -- output buffer
    output: entity work.bit_reg
        port map ( outD => result, inD => cmp_ret, clk => clk_dac );
end basic;