----------------------------------------------------------------------------------
-- 
-- Create Date: 11/18/2021 06:29:46 PM
-- 
-- Module Name: Main - Behavioral
-- Project Name: Processeur SysNum 
-- 
----------------------------------------------------------------------------------

use work.global.all;

entity Processor is
  Port (
    clk : in bit;
    inputs : in bit_vector(0 to wordSize - 1);
    outputs : out bit_vector(0 to wordSize - 1);
    
    ram_rdata : in bit_vector (0 to wordSize - 1);
    ram_addr : inout bit_vector (0 to wordSize - 1);
    ram_wdata : out bit_vector (0 to wordSize - 1);
    ram_we, ram_re, ram_sign_extend : out bit;
    ram_word_size : out bit_vector(0 to 1) );
end Processor;

architecture Behavioral of Processor is
    --decoder
    signal instr : bit_vector (0 to 31);
    signal REG_rs1, REG_rs2, REG_rd : bit_vector (0 to 4);
    signal REG_write, REG_IO, REG_LUI : bit;
    signal ALU_op : bit_vector (0 to 4); -- 0-2 : op, 3 : comp, 4 : special
    signal ALU_isnt_imm, ALU_pc_in : bit;
    signal ALU_imm : bit_vector (0 to 31);
    signal PC_imm  : bit_vector(0 to 31);
    signal PC_is_JALR : bit;
    signal PC_is_JAL_JALR : bit;
    signal RAM_read, RAM_write : bit;
    signal RAM_re_sig, RAM_re_delay : bit;
    signal RAM_addr_delay : bit_vector (0 to wordSize - 1);
    --signal RAM_word_size : bit_vector(0 to 1);
    
    --ALU
    signal inA : bit_vector (0 to wordSize - 1);
    signal inB : bit_vector (0 to wordSize - 1);
    signal op_type : bit_vector (0 to 3);
    signal special : bit; -- bit 30 for non immediate function
    signal alu_result : bit_vector (0 to wordSize - 1);
    signal comp : bit;
    
    --regManager
    --signal raddr0, raddr1, waddr : bit_vector (0 to regNumRank - 1); -- REG_rs1 REG_rs2 REG_rd
--    signal we: bit; reg_write
    signal rd, rd_lui_pcincr : bit_vector (0 to wordSize - 1);
    signal rs1, rs2 : bit_vector (0 to wordSize - 1);
    signal REG_we, is_JAL_JALR_LUI : bit;
    --signal reg_io : bit;
    
--    signal inputs : bit_vector(0 to wordSize - 1);
--    signal outputs : bit_vector(0 to wordSize - 1);
    
    --rom
    signal ROM_addr : bit_vector (0 to 31 );
    --signal data : bit_vector (0 to prgm_wordSize - 1)  == instr
    
    --ram
    --signal ram_addr : bit_vector (0 to ram_addrSize - 1);
    --signal we : bit; RAM_write
    --signal ram_out : bit_vector (0 to wordSize - 1);
    --signal wdata : bit_vector (0 to wordSize - 1); == rs2
    
    --pc
    signal PC_instr, PC_incr : bit_vector (0 to wordSize - 1);
    signal jump, nop, notnop : bit;

begin

    prgm : entity work.rom
        port map (clk => clk, raddr => ROM_addr(2 to prgm_addrSize + 1), data => instr);
    
    jump <= (PC_is_JAL_JALR or (ALU_op(3) and comp) or ram_re_sig) and notnop;
    nop_reg : entity work.bit_reg
        port map (outD => nop, inD => jump, clk => clk);
    notnop <= not nop;
    
    pc_block : entity work.PC
        generic map (wordRank, wordSize)
        port map ( jump => jump, is_JALR => PC_is_JALR,
                   clk => clk, imm => PC_imm, rs1 => rs1, PC_addr => ROM_addr,
                   PC_instr => PC_instr, PC_delay => PC_incr);
    
    decoder_block : entity work.decoder
        Port map ( instr => instr,
               REG_rs1 => REG_rs1, REG_rs2 => REG_rs2, REG_rd => REG_rd,
               REG_write => REG_write, REG_IO => REG_IO, REG_LUI => REG_LUI,
               ALU_op => ALU_op,
               ALU_isnt_imm => ALU_isnt_imm, ALU_pc_in => ALU_pc_in,
               ALU_imm => ALU_imm,
               PC_imm => PC_imm,
               PC_is_JALR => PC_is_JALR,
               PC_is_JAL_JALR => PC_is_JAL_JALR,
               RAM_read=> RAM_read, RAM_write => RAM_write, RAM_sign_extend => RAM_sign_extend,
               RAM_word_size => RAM_word_size
             );
     
    ALU_block: entity work.ALU
        Port map ( inA => inA,
             inB => inB,
             op_type => ALU_op(0 to 3),
             special => ALU_op(4),
             result => alu_result,
             comp => comp
             );
     
    REG_we <= REG_write and notnop;
    register_manager: entity work.regManager 
        Port map ( raddr0 => REG_rs1, raddr1 => REG_rs2 , waddr=> REG_rd,
               we => REG_we ,clk => clk,
               data => rd,
               R0 => rs1, R1 => rs2,
               reg_io => REG_IO,
               inputs => inputs, outputs => outputs,
               ram_read => ram_re_sig, ram_rd => ram_rdata);
     
    ram_re_sig <= RAM_read and notnop;
    ram_re_delay_reg: entity work.bit_reg
        port map (clk => clk, inD => ram_re_sig, outD => ram_re_delay);
    ram_re <= ram_re_sig or ram_re_delay;
    ram_we <= RAM_write and notnop;
    
    ram_addr_delay_reg: entity work.bit_vect_reg
        generic map(wordSize)
        port map(inD => ram_addr, outD => ram_addr_delay, clk => clk);
    ram_addr_mux: entity work.bmux
        generic map(wordSize)
        port map(in0 => alu_result, in1 => ram_addr_delay, chooser => ram_re_delay, result => ram_addr);
    ram_wdata <= rs2;
              
    mux_inA : entity work.bmux
        generic map (wordSize)
        port map (in0 => rs1, in1 => PC_instr,
                  chooser => ALU_pc_in, result => inA);
     
    mux_inB : entity work.bmux
        generic map (wordSize)
        port map (in0 => ALU_imm, in1 => rs2,
                  chooser => ALU_isnt_imm, result => inB);
     
    mux_lui_pcincr : entity work.bmux
        generic map (wordSize)
        port map (in0 => ALU_imm, in1 => PC_incr,
                  chooser => PC_is_JAL_JALR, result => rd_lui_pcincr);
     
    is_JAL_JALR_LUI <= PC_is_JAL_JALR or REG_LUI;
    mux_rd : entity work.bmux
        generic map (wordSize)
        port map (chooser => is_JAL_JALR_LUI,
                  in0 => alu_result, in1 => rd_lui_pcincr,
                  result => rd);

--    Test: ALU
--        port map(inA => A, inB => B, result => LED, op_type => "00");

end Behavioral;


use work.global.all;

entity Main is
  Port (
         CLK, fast_forward : in bit;
         VGA_R, VGA_G, VGA_B : out bit_vector(0 to 1);
         VGA_HS, VGA_VS : out bit );
end Main;

architecture Behavioral of Main is
    component clk_wiz_0
        port (clk50MHz, clk25MHz, clk_proc : out bit; clk_in: in bit);
    end component;

    signal inputs : bit_vector(0 to wordSize - 1);
    signal outputs : bit_vector(0 to wordSize - 1);
    signal clk20ns: bit;
    signal clk40ns: bit;
    signal clk_proc: bit;
    
    signal rd_color : bit_vector(0 to 23);
    signal pixel_container : bit_vector(0 to wordSize - 1);
    signal shifted_pixel_container : bit_vector(0 to wordSize - 1);
    signal rd_address, rd_address_nxt : bit_vector(0 to 18);
    
    -- proc/ram
    signal pram_rdata, pram_addr, pram_wdata : bit_vector (0 to wordSize - 1);
    signal pram_we, pram_re, pram_sign_extend : bit;
    signal pram_word_size : bit_vector(0 to 1);
    
    signal clk_sys, clk25_cnt_max : bit;
    signal clk_25_cnt, clk_25_cnt_incr, clk_25_cnt_nxt : bit_vector(0 to 15);
begin
    clk_div: clk_wiz_0
        port map(clk_in => clk, clk50MHz => clk20ns, clk25MHz => clk40ns, clk_proc => clk_proc);

    clk25_cnt_reg: entity work.bit_vect_reg
        generic map ( width => 16 )
        port map ( outD => clk_25_cnt, inD => clk_25_cnt_nxt, clk => clk40ns );
    clk25_cnt_calc: entity work.incr
        generic map ( wordRank => 4 )
        port map (a => clk_25_cnt, result => clk_25_cnt_incr);
    clk25_cnt_mux: entity work.bmux
        generic map ( busSize => 16 )
        port map (
            in0 => clk_25_cnt_incr,
            in1 => (others => '0'),
            chooser => clk25_cnt_max,
            result => clk_25_cnt_nxt
        );
    max_clk25_cnt: entity work.forall -- clk_25_cnt == 12499 (0b0011000011010011)
        generic map ( busSize => 16 )
        port map ( input => clk_25_cnt xor "0011010011110011", result => clk25_cnt_max );
    
    clk_sys_gen: entity work.bit_treg 
        port map (inD => not clk_sys, outD => clk_sys, we => clk25_cnt_max, clk => clk40ns );
    
    inputs(0) <= clk_sys;
    inputs(1) <= fast_forward;

    proc : entity work.processor
        port map (
            clk => clk_proc,
            inputs => inputs, outputs => outputs,
            
            ram_rdata => pram_rdata,
            ram_addr => pram_addr,
            ram_wdata => pram_wdata,
            ram_we => pram_we,
            ram_re => pram_re,
            ram_sign_extend => pram_sign_extend,
            ram_word_size => pram_word_size
        );
    
    ram_block: entity work.ram
        port map (
            clk => clk_proc,
            clk_vid => clk20ns,
            addr => pram_addr,
            we => pram_we,
            re => pram_re,
            wdata => pram_wdata,
            rdata => pram_rdata,
            dataws => pram_word_size,
            sign_extends => pram_sign_extend,
            
            pixel_pack_addr => rd_address_nxt (3 to 18),
            pixel_pack => pixel_container
        );
    
    palette_code_reading: entity work.shiftRight
        generic map(logInputSize => wordRank, countSize => 5, logScale => 0)
        port map(input => pixel_container, count => "00"&rd_address(0 to 2), padWith => '0', result => shifted_pixel_container);
    
    color_palette : entity work.Palette
        generic map ( 4 )
        port map ( 
            raddr => shifted_pixel_container(0 to 3),
            clk => clk_proc,
            col => rd_color,
            input => outputs);
    
    video: entity work.video
        port map (
            clk_40ns => clk40ns,
            read_offset => rd_address,
            read_offset_nxt => rd_address_nxt,
            pixel_data => rd_color,
--            pixel_data => (others => '1'),
            vga_r => VGA_R,
            vga_g => VGA_G,
            vga_b => VGA_B,
            vga_hs => VGA_HS,
            vga_vs => VGA_VS );
end Behavioral;
