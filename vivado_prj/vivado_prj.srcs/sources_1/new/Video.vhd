----------------------------------------------------------------------------------
--
-- Create Date: 01/04/2022
--
-- Module Name: Video
-- Project Name: Processeur SysNum
--
----------------------------------------------------------------------------------

use work.global.all;

entity video is
    port (
        read_offset : inout bit_vector(0 to 18);
        read_offset_nxt : inout bit_vector(0 to 18);
        pixel_data : in bit_vector(0 to 23);
        clk_40ns : in bit;
        vga_hs, vga_vs : out bit;
        vga_r, vga_g, vga_b : out bit_vector(0 to 1)
    );
end video;

-- VGA timing
-- Line : Pixel data | Front proch |  HSYNC  | Back porch |
-- (pixels)   640   640     16    656  96   752  48      800
--
-- Vertical : Video  | Front proch |  VSYNC  | Back porch |
-- (lines)     480  480    10     490   2   492    33    525


architecture basic of video is
    signal pos_x, pos_x_next, pos_x_incr : bit_vector(0 to 15);
    signal pos_y, pos_y_next, pos_y_incr : bit_vector(0 to 15);
    signal pos_x_maxed, pos_y_maxed : bit;
    signal in_x, in_y, in_xy : bit;
    signal hs_begin, hs_end : bit;
    
    signal pixel_r, pixel_g, pixel_b : bit_vector(0 to 7);
    
    signal vga_r_pre, vga_g_pre, vga_b_pre : bit_vector(0 to 1);
    signal vga_hs_pre, vga_vs_pre : bit;
    
    signal read_offset_incr : bit_vector(0 to 31);
begin
    -- position counters
    pos_x_reg: entity work.bit_vect_reg
        generic map ( width => 16 )
        port map ( outD => pos_x, inD => pos_x_next, clk => clk_40ns );
    pos_y_reg: entity work.bit_vect_reg
        generic map ( width => 16 )
        port map ( outD => pos_y, inD => pos_y_next, clk => clk_40ns );
    
    -- position increment
    pos_x_calc: entity work.incr
        generic map ( wordRank => 4 )
        port map (a => pos_x, result => pos_x_incr);
    pos_y_calc: entity work.adder
        generic map ( wordRank => 4 )
        port map (inA => pos_y, inB => (others => '0'), carryIn => pos_x_maxed, result => pos_y_incr);
        
    -- position loop
    pos_x_max: entity work.forall -- pos_x_incr == 800 (0b0000001100100000)
        generic map ( busSize => 16 )
        port map ( input => pos_x_incr xor "1111101100111111", result => pos_x_maxed );
    pos_x_loop: entity work.bmux
        generic map ( busSize => 16 )
        port map (
            in0 => pos_x_incr,
            in1 => (others => '0'),
            chooser => pos_x_maxed,
            result => pos_x_next
        );

    pos_y_max: entity work.forall -- pos_y_incr == 525 (0b0000001000001101)
        generic map ( busSize => 16 )
        port map ( input => pos_y_incr xor "0100111110111111", result => pos_y_maxed );
    pos_y_loop: entity work.bmux
        generic map ( busSize => 16 )
        port map (
            in0 => pos_y_incr,
            in1 => (others => '0'),
            chooser => pos_y_maxed,
            result => pos_y_next
        );

    -- Display range
    in_x_cmp : entity work.lt -- pos_x < 640 (0b1010000000)
        generic map ( wordRank => 4 )
        port map (a => pos_x,
                  b => bit_vector'("0000000101000000"), 
                  result => in_x );
    in_y_cmp : entity work.lt -- pos_y < 480 (0b111100000)
        generic map ( wordRank => 4 )
        port map ( a => pos_y,
                   b => bit_vector'("0000011110000000"),
                   result => in_y );

    in_xy <= in_x and in_y;


    -- Horizontal and vertical sync
    vga_hs_begin : entity work.lt -- pos_x < 656 (0b1010010000)
        generic map ( wordRank => 4 )
        port map (a => pos_x,
                  b => bit_vector'("0000100101000000"), 
                  result => hs_begin );
    vga_hs_end : entity work.lt -- pos_x < 752 (0b1011110000)
        generic map ( wordRank => 4 )
        port map (a => pos_x,
                  b => bit_vector'("0000111101000000"), 
                  result => hs_end );
    vga_hs_pre <= not(hs_begin xor hs_end); -- negative polarity

    vga_vs_gen : entity work.exists -- pos_y != 490/491 (0b11110101.) -- negative polarity
        generic map ( busSize => 8 )
        port map ( input => pos_y(1 to 8) xor "10101111", result => vga_vs_pre );

    -- Ram position
    read_offset_calc: entity work.adder
        generic map ( wordRank => 5 )
        port map (inA => read_offset & "0000000000000", inB => (others => '0'), carryIn => in_xy, result => read_offset_incr);
    read_offset_reg: entity work.bit_vect_reg
        generic map ( width => 19 )
        port map ( outD => read_offset, inD => read_offset_nxt, clk => clk_40ns );
    read_offset_mux: entity work.bmux
        generic map ( busSize => 19 )
        port map (
            in0 => (others => '0'),
            in1 => read_offset_incr(0 to 18),
            chooser => in_y,
            result => read_offset_nxt
        );

    -- Pixel data
    pixel_b <= pixel_data(0 to 7);
    pixel_g <= pixel_data(8 to 15);
    pixel_r <= pixel_data(16 to 23);
        
    vga_r_pre(0) <= pixel_r(7) and in_xy;
    vga_r_pre(1) <= pixel_r(6) and in_xy;
    vga_g_pre(0) <= pixel_g(7) and in_xy;
    vga_g_pre(1) <= pixel_g(6) and in_xy;
    vga_b_pre(0) <= pixel_b(7) and in_xy;
    vga_b_pre(1) <= pixel_b(6) and in_xy;
    
    -- Buffers
    vga_r_reg: entity work.bit_vect_reg
        generic map ( width => 2 )
        port map ( outD => vga_r, inD => vga_r_pre, clk => clk_40ns );
    vga_g_reg: entity work.bit_vect_reg
        generic map ( width => 2 )
        port map ( outD => vga_g, inD => vga_g_pre, clk => clk_40ns );
    vga_b_reg: entity work.bit_vect_reg
        generic map ( width => 2 )
        port map ( outD => vga_b, inD => vga_b_pre, clk => clk_40ns );
    vga_hs_reg: entity work.bit_reg
        port map ( outD => vga_hs, inD => vga_hs_pre, clk => clk_40ns );
    vga_vs_reg: entity work.bit_reg
        port map ( outD => vga_vs, inD => vga_vs_pre, clk => clk_40ns );
end basic;