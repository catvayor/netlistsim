
-----------------------------------------------
-- Adder
--
-- Deux implémentations de l'additionneur :
-- Ripple Carry Adder (classique)
-- Carry Look Ahead Adder (generate/propagate)
-----------------------------------------------

entity adder is
    generic ( wordRank : positive );
    Port ( inA : in bit_vector (0 to 2**wordRank - 1);
         inB : in bit_vector (0 to 2**wordRank - 1);
         carryIn : in bit;
         result : out bit_vector (0 to 2**wordRank - 1);
         carryOut : out bit);
end adder;

-----------------------------------------------
-- Ripple Carry Adder

entity fullAdder is
    Port ( A, B, inC : in bit;
         R, outC : out bit);
end fullAdder;

architecture basic of fullAdder is
    signal halfR, halfC : bit;
begin
    halfR <= A xor B;
    halfC <= A and B;

    R <= halfR xor inC;
    outC <= (halfR and inC) or halfC;
end basic;

architecture ripple of adder is
    constant wordSize : positive := 2**wordRank;
    signal carr : bit_vector (0 to wordSize);
begin
    carr(0) <= carryIn;

    for_rc_adder: for i in 0 to wordSize - 1 generate
        fa: entity work.fullAdder
            port map ( A => inA(i), B => inB(i), inC => carr(i),
                     R => result(i), outC => carr(i+1) );
    end generate;

    carryOut <= carr(wordSize);
end ripple;

-----------------------------------------------
-- Carry Look Ahead Adder

entity fullCarryLookaheadAdder is
    Port ( A, B, C : in bit;
        R, G, P : out bit);
end fullCarryLookaheadAdder;

architecture basic of fullCarryLookaheadAdder is
    signal X: bit;
begin
    X <= A xor B;
    G <= A and B;
    R <= X xor C;
    P <= X;
end basic;

entity carryLookaheadAdder is
    generic ( logwordsize : natural );
    Port ( inA, inB : in bit_vector (0 to 2**logwordsize - 1);
        inC : in bit;
        res : out bit_vector (0 to 2**logwordsize - 1);
        P,G : out bit );
end carryLookaheadAdder;

architecture basic of carryLookaheadAdder is
    signal P0, G0, P1, G1, inC1 : bit;
begin
    initCLA:if ( logwordsize = 0 ) generate
        -- implémenter fullCarryLookaheadAdder
        adder : entity work.fullCarryLookaheadAdder
            port map (  A => inA(0), B => inB(0), C => inC,
                        R => res(0), P => P, G => G );
    end generate;
    hereCLA:if (logwordsize /= 0) generate
        -- implémenter récursivement 2 carryLookaheadAdder de taille inférieure
        adder0 : entity work.carryLookaheadAdder
            generic map ( logwordsize => logwordsize - 1)
            port map(   inA => inA(0 to 2**(logwordsize-1)-1),
                        inB => inB(0 to 2**(logwordsize-1)-1),
                        inC => inC,
                        res => res(0 to 2**(logwordsize-1)-1),
                        P => P0, G => G0
                    );
        inC1 <= (inC and P0) or G0;
        adder1 : entity work.carryLookaheadAdder
            generic map ( logwordsize => logwordsize - 1)
            port map(   inA => inA(2**(logwordsize-1) to 2**logwordsize-1),
                        inB => inB(2**(logwordsize-1) to 2**logwordsize-1),
                        inC => inC1,
                        res => res(2**(logwordsize-1) to 2**logwordsize-1),
                        P => P1, G => G1
                    );
        P <= P0 and P1;
        G <= (G0 and P1) or G1;
    end generate;
end basic;

-- wrapper pour carryLookaheadAdder, renvoie la retenue plutôt que G,P
architecture carrylookahead of adder is
    signal P,G : bit;
begin
    claadder : entity work.carryLookaheadAdder
        generic map ( logwordsize => wordRank)
        port map (
            inA => inA, inB => inB,
            inC => carryIn,
            res => result,
            P => P,
            G => G );
    carryOut <= (carryIn and P) or G;
end carrylookahead;


-----------------------------------------------
-- Unité arithmétique
-----------------------------------------------

entity arithUnit is
    generic ( wordRank : positive );
    Port ( inA : in bit_vector (0 to 2**wordRank - 1);
         inB : in bit_vector (0 to 2**wordRank - 1);
         sub : in bit;
         result : out bit_vector (0 to 2**wordRank - 1);
         carryOut : out bit);
end arithUnit;

architecture arith of arithUnit is
    constant wordSize : positive := 2**wordRank;
    signal effectiveB : bit_vector (0 to wordSize - 1);
begin

    eff_B : for i in 0 to wordSize - 1 generate
        effectiveB(i) <= inB(i) xor sub;
    end generate;

    Add : entity work.adder(carrylookahead)
        generic map ( wordRank )
        port map ( inA => inA, inB => effectiveB, carryIn => sub,
                 result => result, carryOut => carryOut);

end arith;
