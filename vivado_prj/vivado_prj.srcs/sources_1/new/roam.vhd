----------------------------------------------------------------------------------
-- 
-- Create Date: 11/20/2021 09:56:22 PM
--
-- Module Name: rom - Behavioral
-- Project Name: Processeur SysNum 
-- 
----------------------------------------------------------------------------------

use work.global.all;
entity rom is
    Port ( clk : in bit;
           raddr : in bit_vector (0 to prgm_addrSize - 1 );
           data : out bit_vector (0 to wordSize - 1) );
end rom;

use work.global.all;
entity ram is
    port (clk : in bit;
          clk_vid : in bit;
          addr : in bit_vector (0 to wordSize - 1);
          we, re : in bit;
          wdata : in bit_vector (0 to wordSize - 1);
          rdata : out bit_vector (0 to wordSize - 1);
          dataws : in bit_vector(0 to 1);
          sign_extends : in bit;
          pixel_pack_addr : in bit_vector(0 to 15);
          pixel_pack : out bit_vector(0 to wordSize - 1) );
end ram;

architecture Behavioral of rom is
    signal addraRev : bit_vector (prgm_addrSize - 1 downto 0);
    signal doutaRev : bit_vector (wordSize - 1 downto 0);

begin

    RevAddrA : for i in 0 to prgm_addrSize - 1 generate
        addraRev(i) <= raddr(i);
    end generate;
        
--    RevDataA : for i in 0 to wordSize - 1 generate
--        data(i) <= doutaRev(i);
--    end generate;
    
    Rom_module : entity work.prgm_rom
        port map (clka => clk, addra => addraRev, douta => data );

end Behavioral;

architecture Behavioral of ram is
    component ram_intf
      port (
        clka : in bit;
        ena : in bit;
        wea : in bit_vector(3 downto 0);
        addra : in bit_vector(ram_addrSize - 1 downto 0);
        dina : in bit_vector(wordSize - 1 downto 0);
        douta : out bit_vector(wordSize - 1 downto 0);
        
        clkb : in bit;
        web : in bit_vector(3 downto 0);
        addrb : in bit_vector(ram_addrSize - 1 downto 0);
        dinb : in bit_vector(wordSize - 1 downto 0);
        doutb : out bit_vector(wordSize - 1 downto 0)
      );
    end component;
    
    signal addraRev : bit_vector (ram_addrSize - 1 downto 0);
    
    signal dinaRev : bit_vector (wordSize - 1 downto 0);
    signal doutaRev : bit_vector (wordSize - 1 downto 0);
    signal dina, douta, doutaShift : bit_vector (0 to wordSize - 1);

    signal weaShiftRev : bit_vector (3 downto 0);
    signal wea, weaShift : bit_vector(0 to 3);
    signal upper_we, ena : bit;
    
    signal addrbRev : bit_vector (ram_addrSize - 1 downto 0);
    signal doutbRev : bit_vector (wordSize - 1 downto 0);
    
    signal ws_byte, ws_half, ws_not_word, ws_word : bit;
    signal ws_byte_prev, ws_not_word_prev : bit;
    signal ws_byte_prev_prev, ws_not_word_prev_prev : bit;
    signal sign_extends_prev, sign_extends_prev_prev : bit;
    signal low_addr_prev, low_addr_prev_prev : bit_vector(0 to 1);
    signal SL_count, SR_count : bit_vector (0 to 1);
begin

    RevAddrA : for i in 0 to ram_addrSize - 1 generate
        addraRev(i) <= addr(i+2);
    end generate;
    
    ws_half <= dataws(0);
    ws_word <= dataws(1);
    ws_not_word <= not ws_word;
    ws_byte <= ws_half xor ws_not_word;
    
    shift_wdata : entity work.shiftLeft
        generic map( logInputSize => wordRank, countSize => 2, logScale => 3 )
        port map( input => wdata, count => addr(0 to 1), result => dina );
    
    RevDataInA : for i in 0 to wordSize - 1 generate
        dinaRev(i) <= dina(i);
    end generate;
    
    low_addr_delay_1: entity work.bit_vect_reg
        generic map(width => 2)
        port map(outD => low_addr_prev, inD => addr(0 to 1), clk => clk);
    low_addr_delay_2: entity work.bit_vect_reg
        generic map(width => 2)
        port map(outD => low_addr_prev_prev, inD => low_addr_prev, clk => clk);
    
    ws_byte_delay_1: entity work.bit_reg
        port map(outD => ws_byte_prev, inD => ws_byte, clk => clk);
    ws_byte_delay_2: entity work.bit_reg
        port map(outD => ws_byte_prev_prev, inD => ws_byte_prev, clk => clk);
    
    ws_not_word_delay_1: entity work.bit_reg
        port map(outD => ws_not_word_prev, inD => ws_not_word, clk => clk);
    ws_not_word_delay_2: entity work.bit_reg
        port map(outD => ws_not_word_prev_prev, inD => ws_not_word_prev, clk => clk);
        
    ws_sign_extends_delay_1: entity work.bit_reg
        port map(outD => sign_extends_prev, inD => sign_extends, clk => clk);
    ws_sign_extends_delay_2: entity work.bit_reg
        port map(outD => sign_extends_prev_prev, inD => sign_extends_prev, clk => clk);
        
    -- shift count
    SL_count <= (low_addr_prev_prev(0) xor ws_byte_prev_prev) & (low_addr_prev_prev(1) xor ws_not_word_prev_prev);
    SR_count <= ws_byte_prev_prev & ws_not_word_prev_prev;
    
    RevDataOutA : for i in 0 to wordSize - 1 generate
        douta(i) <= doutaRev(i);
    end generate;
    
    DataOutA_SL : entity work.shiftLeft
        generic map( logInputSize => wordRank, countSize => 2, logScale => 3 )
        port map( input => douta, count => SL_count, result => doutaShift );
    
    DataOutA_SR : entity work.shiftRight
        generic map( logInputSize => wordRank, countSize => 2, logScale => 3 )
        port map( input => doutaShift, count => SR_count, padWith => sign_extends_prev_prev and doutaShift(wordSize - 1), result => rdata );
    
    wea(0) <= we;
    wea(1) <= we and (not ws_byte);
    upper_we <= we and ws_word;
    wea(2) <= upper_we;
    wea(3) <= upper_we;
    
    shift_wea : entity work.shiftLeft
        generic map( logInputSize => 2, countSize => 2, logScale => 0 )
        port map( input => wea, count => addr(0 to 1), result => weaShift );
    
    rev_wea : for i in 0 to 3 generate
        weaShiftRev(i) <= weaShift(i);
    end generate;
    
    ena <= we or re or '1';
    
    RevAddrB : for i in 0 to ram_addrSize - 1 generate
        addrbRev(i) <= pixel_pack_addr(i);
    end generate;
    RevDataOutB : for i in 0 to wordSize - 1 generate
        pixel_pack(i) <= doutbRev(i);
    end generate;
    
    Ram_module : ram_intf 
        port map ( clka => clk, wea => weaShiftRev, ena => ena,
                   dina => dinaRev, douta => doutaRev,
                   addra => addraRev,
                   
                   clkb => clk_vid,
                   web => (others => '0'),
                   addrb => addrbRev,
                   dinb => (others => '0'),
                   doutb => doutbRev );

end Behavioral;