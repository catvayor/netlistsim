----------------------------------------------------------------------------------
-- 
-- Create Date: 11/20/2021 09:53:35 AM
-- 
-- Module Name: ALU - Behavioral
-- Project Name: Processeur SysNum 
-- 
----------------------------------------------------------------------------------

-----------------------------------------------
-- SLL

-- result = input << (count << logScale)
entity shiftLeft is
    generic ( logInputSize: natural; countSize: natural; logScale: natural );
    port ( input: in bit_vector(0 to 2**logInputsize - 1);
           count: in bit_vector(0 to countSize - 1);
           result: out bit_vector(0 to 2**logInputsize - 1) );
end shiftLeft;

architecture basic of shiftLeft is
    constant inputSize: positive := 2**logInputSize;
    constant scale: positive := 2**logScale;

    signal forcezero: bit;
    signal tmp_result: bit_vector(0 to inputSize - 1);
    signal shifted: bit_vector(0 to inputSize - 1);
begin
    init: if (countSize = 0) generate
        result <= input;
    end generate;
    rec: if (countSize > 0) generate
        toobig: if (inputsize <= scale) generate
            forcezero0: entity work.exists generic map(busSize => countSize) port map(input => count, result => forcezero);
            mux: entity work.bmux generic map (busSize => inputSize) port map(in0 => input, in1 => bit_vector'(others => '0'), chooser => forcezero, result => tmp_result);
        end generate;
        ok: if (inputSize > scale) generate
            shifted(scale to inputSize - 1) <= input(0 to inputSize - scale - 1);
            shifted(0 to scale - 1) <= (others => '0');
            
            mux: entity work.bmux
                generic map (busSize => inputSize) port map(in0 => input, in1 => shifted, chooser => count(0), result => tmp_result);
        end generate;
        call: entity work.shiftLeft
            generic map(logInputSize => logInputSize, countSize => countSize - 1, logScale => logScale + 1)
            port map(input => tmp_result, count => count(1 to countSize - 1), result => result);
    end generate;
end basic;

-----------------------------------------------
-- SRL/SRA

-- result = input >> (count << logScale)
entity shiftRight is
    generic ( logInputSize: natural; countSize: natural; logScale: natural );
    port ( input: in bit_vector(0 to 2**logInputsize - 1);
           count: in bit_vector(0 to countSize - 1);
           padWith: in bit;
           result: out bit_vector(0 to 2**logInputsize - 1) );
end shiftRight;

architecture basic of shiftRight is
    constant inputSize: positive := 2**logInputSize;
    constant scale: positive := 2**logScale;

    signal forcezero: bit;
    signal tmp_result: bit_vector(0 to inputSize - 1);
    signal shifted: bit_vector(0 to inputSize - 1);
begin
    init: if (countSize = 0) generate
        result <= input;
    end generate;
    rec: if (countSize > 0) generate
        toobig: if (inputsize <= scale) generate
            forcezero0: entity work.exists generic map(busSize => countSize) port map(input => count, result => forcezero);
            mux: entity work.bmux generic map (busSize => inputSize) port map(in0 => input, in1 => bit_vector'(others => padWith), chooser => forcezero, result => result);
        end generate;
        ok: if (inputSize > scale) generate
            shifted(0 to inputSize - scale - 1) <= input(scale to inputSize - 1);
            shifted(inputSize - scale to inputSize - 1) <= (others => padWith);
            mux: entity work.bmux generic map (busSize => inputSize) port map(in0 => input, in1 => shifted, chooser => count(0), result => tmp_result);
            call: entity work.shiftRight
                generic map(logInputSize => logInputSize, countSize => countSize - 1, logScale => logScale + 1)
                port map(input => tmp_result, count => count(1 to countSize - 1), padWith => padWith, result => result);
        end generate;
    end generate;
end basic;


-----------------------------------------------
-- ALU
-----------------------------------------------

use work.global.all;
entity ALU is
    Port ( inA : in bit_vector (0 to wordSize - 1);
         inB : in bit_vector (0 to wordSize - 1);
         op_type : in bit_vector (0 to 3);
         special : in bit; -- bit 30 for non immediate function
         result : out bit_vector (0 to wordSize - 1);
         comp : out bit
         );
end ALU;

architecture Behavioral of ALU is
    signal LT_op : bit;
    signal sub_bit: bit;
    signal arithCarry : bit;

    signal SR_padWith : bit;

    -- chooser and result
    signal ADD_ret, SLL_ret, SLT_ret, SLTU_ret, XOR_ret, SR_ret, OR_ret, AND_ret : bit_vector(0 to wordSize-1);
    signal ADD_SLL, SLT_SLTU, XOR_SR, OR_AND : bit_vector(0 to wordSize - 1); -- op_type(0) choice
    signal ADD_SLL_SLT_SLTU, XOR_SR_OR_AND : bit_vector(0 to wordSize - 1); -- op_type(1) choice
    -- cmp
    signal CMP_ret, CMP_NEQ, CMP_INEQ, CMP_LT, CMP_LTU : bit;
    -- op_type(2) directly in result
begin

    -- ADD/SUB
    sub_bit <= special or LT_op;
    Arith : entity work.arithUnit
        generic map (wordRank)
        port map (inA => inA, inB => inB, carryOut => arithCarry,
                  sub => sub_bit,
                  result => ADD_ret );

    -- SLL
    SLL_entity: entity work.shiftLeft
        generic map(logInputSize => wordRank, countSize => 5, logScale => 0)
        port map(input => inA, count => inB(0 to 4), result => SLL_ret);

    -- SLT
    LT_op <= ((not op_type(2)) and op_type(1)) or op_type(3); -- op_type(3) for comparison
    CMP_LT <= ADD_ret(31);
    SLT_ret <= (0 => CMP_LT, others => '0');

    -- SLTU
    CMP_LTU <= not arithCarry;
    SLTU_ret <= (0 => CMP_LTU, others => '0');

    -- XOR
    XOR_loop: for i in 0 to wordSize - 1 generate
        XOR_ret(i) <= inA(i) xor inB(i);
    end generate;

    -- SRL/SRA
    SR_PAD: entity work.mux port map (in0 => bit'('0'), in1 => inA(wordSize-1), chooser => special, result => SR_padWith);

    SR_entity: entity work.shiftRight
        generic map(logInputSize => wordRank, countSize => 5, logScale => 0)
        port map(input => inA, count => inB(0 to 4), padWith => SR_padWith, result => SR_ret);

    -- OR
    OR_loop: for i in 0 to wordSize - 1 generate
        OR_ret(i) <= inA(i) or inB(i);
    end generate;

    -- AND
    AND_loop: for i in 0 to wordSize - 1 generate
        AND_ret(i) <= inA(i) and inB(i);
    end generate;

    -- CHOOSERS

    ADD_SLL_MUX: entity work.bmux generic map (wordSize)
            port map (in0 => ADD_ret, in1 => SLL_ret, chooser => op_type(0), result => ADD_SLL);
    SLT_SLTU_MUX: entity work.bmux generic map (wordSize)
            port map (in0 => SLT_ret, in1 => SLTU_ret, chooser => op_type(0), result => SLT_SLTU);
    XOR_SR_MUX: entity work.bmux generic map (wordSize)
            port map (in0 => XOR_ret, in1 => SR_ret, chooser => op_type(0), result => XOR_SR);
    OR_AND_MUX: entity work.bmux generic map (wordSize)
            port map (in0 => OR_ret, in1 => AND_ret, chooser => op_type(0), result => OR_AND);

    ADD_SLL_SLT_SLTU_MUX: entity work.bmux generic map (wordSize)
            port map (in0 => ADD_SLL, in1 => SLT_SLTU, chooser => op_type(1), result => ADD_SLL_SLT_SLTU);
    XOR_SR_OR_AND_MUX: entity work.bmux generic map (wordSize)
            port map (in0 => XOR_SR, in1 => OR_AND, chooser => op_type(1), result => XOR_SR_OR_AND);

    RESULT_MUX:  entity work.bmux generic map (wordSize)
            port map (in0 => ADD_SLL_SLT_SLTU, in1 => XOR_SR_OR_AND, chooser => op_type(2), result => result);

    -- CMP
    NEQ: entity work.exists
        generic map (busSize => wordSize)
        port map (input => XOR_ret, result => CMP_NEQ);

    EQ_INEQ: entity work.mux
        port map (in0 => not CMP_NEQ, in1 => CMP_INEQ, chooser => op_type(2), result => CMP_ret);

    INEQ: entity work.mux
        port map (in0 => CMP_LT, in1 => CMP_LTU, chooser => op_type(1), result => CMP_INEQ);

    CMP_NEG: entity work.mux
        port map (in0 => CMP_ret, in1 => not CMP_ret, chooser => op_type(0), result => comp);
end Behavioral;
