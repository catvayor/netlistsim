----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/14/2021 02:21:22 PM
-- Design Name: 
-- Module Name: PC - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

entity PC is
    generic (wordRank, wordSize: positive);
    Port ( jump, is_JALR, clk : in bit;
           imm, rs1 : in bit_vector (0 to 31);
           PC_instr, PC_delay, PC_addr : inout bit_vector (0 to 31));
end PC;

architecture Behavioral of PC is
    signal rs1_or_PC, immadder_result, incr_result : bit_vector (0 to wordSize - 1);
    signal dont_use_incr : bit;
begin
    reg_instr: entity work.bit_vect_reg
        generic map(wordSize)
        port map(outD => PC_instr, inD => PC_delay, clk => clk);
    reg_delay: entity work.bit_vect_reg
        generic map(wordSize)
        port map(outD => PC_delay, inD => PC_addr, clk => clk);
    
    MUX_rs1_PC: entity work.bmux
        generic map(wordSize)
        port map(in0 => PC_instr, in1 => rs1, chooser => is_JALR, result => rs1_or_PC);
    
    immadder: entity work.adder
        generic map(wordRank)
        port map(inA => imm, inB => rs1_or_PC, carryIn => '0', result => immadder_result);
    
    pcincr: entity work.adder
        generic map(wordRank)
        port map(inA => PC_delay, inB => (2 => '1', others => '0'), carryIn => '0', result => incr_result);

    result: entity work.bmux
        generic map(wordSize)
        port map(in0 => incr_result, in1 => immadder_result, chooser => jump, result => PC_addr);
end Behavioral;
