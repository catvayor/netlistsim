----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/08/2021 02:28:43 PM
-- Design Name: 
-- Module Name: decoder - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

entity decoder is
    Port ( instr : in bit_vector (0 to 31);
           REG_rs1, REG_rs2, REG_rd : out bit_vector (0 to 4);
           REG_write, REG_IO, REG_LUI : out bit;
           ALU_op : out bit_vector (0 to 4); -- 0-2 : op, 3 : comp, 4 : special
           ALU_isnt_imm, ALU_pc_in : out bit;
           ALU_imm : out bit_vector (0 to 31);
           PC_imm  : out bit_vector(0 to 31);
           PC_is_JALR : out bit;
           PC_is_JAL_JALR :out bit;
           RAM_read, RAM_write, RAM_sign_extend : out bit;
           RAM_word_size : out bit_vector(0 to 1);
           error : out bit
         );
end decoder;

architecture Behavioral of decoder is
    signal opcode : bit_vector(0 to 6);
    signal funct3 : bit_vector(0 to 2);
    signal funct7 : bit_vector(0 to 6);
    signal imm20  : bit_vector(12 to 31);
    signal imm12  : bit_vector(0 to 11);
    signal imm21  : bit_vector(0 to 20);
    signal imm13  : bit_vector(0 to 12);
    signal imm12_sliced : bit_vector(0 to 11);
    signal imm12_const  : bit_vector(0 to 11);
    signal imm12_var    : bit_vector(0 to 11);
    signal auipc_or_binop, comp, upper_imm_non_zero : bit;
    signal is_JAL, bit12_imm_pc : bit;
    signal bit11_imm_pc, bit11_imm_pc_JAL_JALR, bit2_imm_pc : bit;
    signal pc_1_3_4 : bit_vector(1 to 4);
    signal sign_extends_or_zero : bit_vector(13 to 31);
    signal binop_ALU_op, binop_AUIPC_ALU_op, jump_ram_ALU_op, comp_ALU_op : bit_vector(0 to 4);
    signal sign_extended_upper_imm : bit_vector(12 to 31);
begin
    ------------------------------------------------------------
    -- TRADUCTION DIFFERENTS FORMATS INSTRUCTIONS
    
    -- Types d'operations
    opcode <= instr(0 to 6);
    funct3 <= instr(12 to 14);
    funct7 <= instr(25 to 31);
    
    -- Gestion des immediats
    imm20(12 to 31) <= instr(12 to 31);
    imm12(0 to 11)  <= instr(20 to 31);
    
    imm12_sliced(0 to 4)   <= instr(7 to 11);
    imm12_sliced(5 to 11)  <= instr(25 to 31);
    
    imm21(20)       <= instr(31);
    imm21(1 to 10)  <= instr(21 to 30);
    imm21(11)       <= instr(20);
    imm21(12 to 19) <= instr(12 to 19);
    imm21(0)        <= '0';
    
    imm13(12)      <= instr(31);
    imm13(5 to 10) <= instr(25 to 30);
    imm13(1 to 4)  <= instr(8 to 11);
    imm13(11)      <= instr(7);
    imm13(0)       <= '0';
    
    error <= opcode(0);
    
    ----------------------------------------------------------
    -- SORTIES CONCERANT LE GESTIO REG
    
    REG_write <= ((not opcode(6)) or opcode(2)) and (not (opcode(5) and (not (opcode(4) or opcode(6)))));
    REG_IO    <= opcode(3) and not opcode(6);
    REG_LUI   <= opcode(2) and opcode(4) and opcode(5);
    
    -- Ids registres
    REG_rs1 <= instr(15 to 19);
    REG_rs2 <= instr(20 to 24);
    REG_rd  <= instr(7 to 11);
    
    ----------------------------------------------------------
    -- SORTIES CONCERNANT L'ALU
    
    ALU_isnt_imm <= not ((opcode(4) and (not opcode(5))) or not (opcode(4) or opcode(6)));
    ALU_pc_in <= opcode(2);
    
    -- Operation effectuee par l'ALU
    binop_ALU_op(0 to 2) <= funct3;
    binop_ALU_op(3) <= '0';
    binop_ALU_op(4) <= funct7(5) and (opcode(5) or funct3(0));
    AUIPC_other_MUX: entity work.bmux generic map (5)
        port map(in0 => binop_ALU_op, in1 => "00000", chooser => opcode(2), result => binop_AUIPC_ALU_op);
    
    comp <= opcode(6) and (not opcode(3));
    comp_ALU_op(0 to 2) <= funct3;
    comp_ALU_op(3) <= '1';
    comp_ALU_op(4) <= '0';
    jump_ram_MUX: entity work.bmux generic map(5)
        port map(in0 => "00000", in1 => comp_alu_op, chooser => comp, result => jump_ram_ALU_op);
    
    auipc_or_binop <= opcode(3) or opcode(4);
    ALU_op_MUX: entity work.bmux generic map (5)
        port map(in0 => jump_ram_ALU_op, in1 => binop_AUIPC_ALU_op, chooser => auipc_or_binop, result => ALU_op);
    
    -- Immediat en entree d'ALU
    upper_imm_non_zero <= opcode(2) and opcode(4);
    sign_extended_upper_imm <= (others => instr(31)); 
    upper_imm_alu: entity work.bmux generic map (20)
        port map(in0 => sign_extended_upper_imm, in1 => imm20, chooser => upper_imm_non_zero, result => ALU_imm(12 to 31));
    
    imm12_const <= (2 => opcode(6), others => '0');
    imm12_var(5 to 11) <= imm12(5 to 11);
    lower_imm_var: entity work.bmux generic map(5)
        port map(in0 => imm12(0 to 4), in1 => imm12_sliced(0 to 4), chooser => opcode(5), result => imm12_var(0 to 4));
    lower_imm_alu: entity work.bmux generic map(12)
        port map(in0 => imm12_var, in1 => imm12_const, chooser => opcode(2), result => ALU_imm(0 to 11));
    
    ----------------------------------------------------------
    -- SORTIES CONCERNANT LE GESTIO PC
    
    PC_is_JALR     <= opcode(6) and opcode(2) and (not opcode(3));
    PC_is_JAL_JALR <= opcode(6) and opcode(2);
    is_JAL     <= opcode(6) and opcode(2) and opcode(3);
    
    -- Immediat en etree de PC-adderis_JAL <= opcode(6) and opcode(3);
    sign_extends_or_zero <= (others => instr(31) and opcode(6));
    
    PC_imm(21 to 31) <= sign_extends_or_zero(21 to 31); -- Extension de signe
    
    middle_imm_pc: entity work.bmux generic map(8)
        port map(chooser => is_JAL, in0 => sign_extends_or_zero(13 to 20), in1 => imm21(13 to 20), result => PC_imm(13 to 20));
    
    lbl_bit12_imm_pc: entity work.mux port map(chooser => opcode(3), in0 => instr(31), in1 => imm21(12), result => bit12_imm_pc);
    PC_imm(12) <= opcode(6) and bit12_imm_pc;
    
    lbl_bit11_JAL:    entity work.mux port map(chooser => opcode(3), in0 => imm12(11), in1 => imm21(11), result => bit11_imm_pc_JAL_JALR);
    lbl_bit11_imm_pc: entity work.mux port map(chooser => opcode(2), in0 => imm13(11), in1 => bit11_imm_pc_JAL_JALR, result => bit11_imm_pc);
    PC_imm(11) <= opcode(6) and bit11_imm_pc;
    
    for_imm_pc_5_10: for i in 5 to 10 generate
        PC_imm(i) <= imm12(i) and opcode(6);
    end generate;
    
    for_imm_pc_1_3_4: for i in 1 to 4 generate
        if_pc_2: if(i /= 2) generate
            mux_imm_pc_1_3_4: entity work.mux port map(chooser => opcode(2), in0 => imm13(i), in1 => imm12(i), result => pc_1_3_4(i));
            PC_imm(i) <= opcode(6) and pc_1_3_4(i);
        end generate;
    end generate;
    
    lbl_bit2_imm_pc:  entity work.mux port map(chooser => opcode(2), in0 => imm13(2), in1 => imm12(2), result => bit2_imm_pc);
    PC_imm(2) <= (not opcode(6)) or bit2_imm_pc;
    
    PC_imm(0) <= opcode(2) and (not opcode(3)) and imm12(0);
    
    ----------------------------------------------------------
    -- SORTIES CONCERNANT LA RAM
    
    RAM_read        <= not (opcode(3) or opcode(4) or opcode(5));
    RAM_write       <= opcode(5) and (not (opcode(4) or opcode(6)));
    RAM_sign_extend <= not funct3(2);
    RAM_word_size   <= funct3(0 to 1);
end Behavioral;
