----------------------------------------------------------------------------------
-- 
-- Create Date: 11/20/2021 10:08:00 PM
--
-- Module Name: global - Behavioral
-- Project Name: Processeur SysNum 
-- 
----------------------------------------------------------------------------------

package global is
    constant prgm_addrSize : positive := 13;
    
    constant ram_addrSize : positive := 16;
    
    constant wordRank : positive := 5;
    constant wordSize : positive := 2**wordRank;
    
    constant regNumRank : positive := 5;
    
end package global;
