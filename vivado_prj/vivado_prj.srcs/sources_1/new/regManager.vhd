----------------------------------------------------------------------------------
-- 
-- Create Date: 11/20/2021 03:15:26 PM
--
-- Module Name: regManager - Behavioral
-- Project Name: Processeur SysNum 
-- 
----------------------------------------------------------------------------------

-- register manager

entity reg_elem is
    generic ( wordSize : positive;
              zero : boolean );
    port ( data : in bit_vector (0 to wordSize - 1);
           outD : out bit_vector (0 to wordSize - 1);
           we, clk : in bit;
           ram_we : in bit; ram_rd : in bit_vector(0 to wordSize - 1) );
end reg_elem;

architecture basic of reg_elem is
    signal reg : bit_vector(0 to wordSize - 1);
    signal eff_data : bit_vector(0 to wordSize - 1);
    signal eff_we : bit;
begin
    Z_register : if zero generate
        outD <= (others => '0');
    end generate;
    
    NZ_register : if not zero generate
        mux_out : entity work.bmux
            generic map ( wordSize )
            port map (result => outD,
                    in0 => reg, in1 => ram_rd, chooser => ram_we );
                    
        mux_eff_data : entity work.bmux
            generic map (wordSize)
            port map(result => eff_data,
                    in0 => ram_rd, in1 => data, chooser => we);
        
        eff_we <= we or ram_we;
    
        P : entity work.bit_vect_treg 
            generic map ( wordSize )
            port map (clk => clk, we => eff_we, inD => eff_data, outD => reg);
    end generate;
end basic;

entity regManagerImpl is
    generic ( wordSize, addrSize : positive;
              zero : boolean );
    Port ( raddr0, raddr1, waddr : in bit_vector (0 to addrSize - 1);
           we,clk : in bit; data : in bit_vector (0 to wordSize - 1);
           R0, R1 : out bit_vector (0 to wordSize - 1);
           ram_we : in bit; ram_dest : in bit_vector(0 to addrSize - 1); 
           ram_rd : in bit_vector(0 to wordSize - 1) );
end regManagerImpl;

architecture Behavioral of regManagerImpl is
    signal R00, R01, R10, R11 : bit_vector(0 to wordSize - 1);
begin
    
    Base_case : if addrSize = 1 generate
        reg0 : entity work.reg_elem
                generic map ( wordSize, zero )
                port map ( data => data, clk => clk,
                           we => we and (not waddr(0)), outD => R00,
                           ram_we => ram_we and (not ram_dest(0)),
                           ram_rd  => ram_rd);
        R01 <= R00;        
        reg1 : entity work.reg_elem
                generic map ( wordSize, false )
                port map ( data => data, clk => clk,
                           we => we and waddr(0), outD => R10,
                           ram_we => ram_we and ram_dest(0),
                           ram_rd  => ram_rd);
        R11 <= R10;
    end generate;    
    
    Rec_case : if addrSize > 1 generate
        reg0 : entity work.regManagerImpl
                generic map ( wordSize => wordSize, addrSize => addrSize - 1, zero => zero )
                port map ( data => data, clk => clk, we => we and (not waddr(0)), 
                           raddr0 => raddr0(1 to addrSize - 1),
                           raddr1 => raddr1(1 to addrSize - 1),
                           waddr  => waddr (1 to addrSize - 1),
                           R0 => R00, R1 => R01,
                           ram_we => ram_we and (not ram_dest(0)),
                           ram_dest => ram_dest(1 to addrSize - 1),
                           ram_rd  => ram_rd );
        reg1 : entity work.regManagerImpl
                generic map ( wordSize => wordSize, addrSize => addrSize - 1, zero => false )
                port map ( data => data, clk => clk, we => we and waddr(0), 
                           raddr0 => raddr0(1 to addrSize - 1),
                           raddr1 => raddr1(1 to addrSize - 1),
                           waddr  => waddr (1 to addrSize - 1),
                           R0 => R10, R1 => R11,
                           ram_we => ram_we and ram_dest(0),
                           ram_dest => ram_dest(1 to addrSize - 1),
                           ram_rd  => ram_rd );
    end generate;
    
    Mux0 : entity work.bmux
        generic map ( wordSize )
        port map (in0 => R00, in1 => R10, chooser => raddr0(0), result => R0);
    
    Mux1 : entity work.bmux
        generic map ( wordSize )
        port map (in0 => R01, in1 => R11, chooser => raddr1(0), result => R1);

end Behavioral;

use work.global.all;
entity regManager is
    Port ( raddr0, raddr1, waddr : in bit_vector (0 to regNumRank - 1);
           we,clk : in bit; data : in bit_vector (0 to wordSize - 1);
           R0, R1 : out bit_vector (0 to wordSize - 1);
           reg_io : in bit;
           inputs : in bit_vector(0 to wordSize - 1);
           outputs : out bit_vector(0 to wordSize - 1);
           ram_read : in bit; ram_rd : in bit_vector(0 to wordSize - 1) );
end regManager;

architecture Behavioral of regManager is
    signal data_to_reg : bit_vector(0 to wordSize - 1);
    signal last_was_ramR : bit;
    signal ram_we : bit;
    signal reg_inputs : bit_vector(0 to wordSize - 1);
    
    signal last_addr_rd : bit_vector(0 to regNumRank - 1);
    signal ram_addr_rd : bit_vector(0 to regNumRank - 1);
begin
    reg_in : entity work.bit_vect_reg 
        generic map (wordSize)
        port map (inD => inputs , outD => reg_inputs, clk => clk);

    data_choose : entity work.bmux 
        generic map ( wordSize )
        port map (in0 => data, in1 => reg_inputs, chooser => reg_io, result => data_to_reg);

    ramR_reg1 : entity work.bit_reg 
        port map (inD => ram_read, clk => clk, outD => last_was_ramR);
    ramR_reg2 : entity work.bit_reg 
        port map (inD => last_was_ramR, clk => clk, outD => ram_we);
        
    ramRD_reg1 : entity work.bit_vect_reg 
        generic map ( regNumRank )
        port map (inD => waddr, clk => clk, outD => last_addr_rd);
    ramRD_reg2 : entity work.bit_vect_reg 
        generic map ( regNumRank )
        port map (inD => last_addr_rd, clk => clk, outD => ram_addr_rd);
     

    Impl : entity work.regManagerImpl 
        generic map (wordSize, regNumRank, true)
        port map (data => data_to_reg, clk => clk, we => we and (not ram_read), 
                  raddr0 => raddr0, raddr1 => raddr1, waddr => waddr,
                  R0 => R0, R1 => R1,
                  ram_we => ram_we,
                  ram_dest => ram_addr_rd,
                  ram_rd  => ram_rd );
    
    REG_out : entity work.reg_elem
        generic map (wordSize, false)
        port map ( outD => outputs, we => reg_io, data => data, clk => clk,
                           ram_we => '0',
                           ram_rd  => (others => '0') );
end Behavioral;

-- color Palette manager

entity PaletteImpl is
    generic ( wordSize, addrSize : positive );
    Port ( raddr, waddr : in bit_vector (0 to addrSize - 1);
           we,clk : in bit; data : in bit_vector (0 to wordSize - 1);
           rd : out bit_vector (0 to wordSize - 1)
           );
end PaletteImpl;

architecture Behavioral of PaletteImpl is
    signal R0, R1: bit_vector(0 to wordSize - 1);
begin
    
    Base_case : if addrSize = 1 generate
        reg0 : entity work.bit_vect_treg
                generic map ( wordSize )
                port map ( inD => data, clk => clk,
                           we => we and (not waddr(0)), outD => R0);
        reg1 : entity work.bit_vect_treg
                generic map ( wordSize )
                port map ( inD => data, clk => clk,
                           we => we and waddr(0), outD => R1 );
    end generate;    
    
    Rec_case : if addrSize > 1 generate
        reg0 : entity work.PaletteImpl
                generic map ( wordSize => wordSize, addrSize => addrSize - 1 )
                port map ( data => data, clk => clk, we => we and (not waddr(0)), 
                           raddr => raddr(1 to addrSize - 1),
                           waddr  => waddr (1 to addrSize - 1),
                           rd => R0 );
        reg1 : entity work.PaletteImpl
                generic map ( wordSize => wordSize, addrSize => addrSize - 1 )
                port map ( data => data, clk => clk, we => we and waddr(0), 
                           raddr => raddr(1 to addrSize - 1),
                           waddr  => waddr (1 to addrSize - 1),
                           rd => R1 );
    end generate;
    
    MuxRd : entity work.bmux
        generic map ( wordSize )
        port map (in0 => R0, in1 => R1, chooser => raddr(0), result => rd);

end Behavioral;

use work.global.all;
entity Palette is
    generic ( colNumRank : positive );
    Port ( raddr : in bit_vector (0 to colNumRank - 1);
           clk : in bit;
           col : out bit_vector (0 to 23);
           input : in bit_vector (0 to wordSize -1) );
end Palette;

architecture Behavioral of Palette is
    signal data : bit_vector(0 to 23);
    signal we : bit;
begin
    data <= input(0 to 23);
    
    has1 : entity work.forall
        generic map ( wordSize - colNumRank - 24 )
        port map ( input => input(24 + colNumRank to wordSize - 1), result => we ); 
     
    Impl : entity work.PaletteImpl 
        generic map (24, colNumRank)
        port map (data => data, clk => clk, we => we, 
                  raddr => raddr, waddr => input(24 to 23 + colNumRank),
                  rd => col );
    
end Behavioral;