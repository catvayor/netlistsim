\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage{color}
\usepackage{verbatim}
\usepackage{listings}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathrsfs}
\usepackage{graphicx}
\usepackage[shortlabels]{enumitem}
\usepackage{physics}
\usepackage{bbm}
\usepackage{indentfirst}


\usepackage{geometry}
\geometry{hmargin=2.5cm,vmargin=1.5cm}

\definecolor{comment}{gray}{0.4}

\lstset{ %
language=Caml,	                   % choix du langage
basicstyle=\small,                 % taille de la police du code
numbers=left,                      % placement du numéro de chaque ligne
numberstyle=\footnotesize,         % style des numéros
numbersep=7pt,       	           % distance entre le code et sa numérotation
commentstyle=\color{comment}\emph, % style des commentaires
keywordstyle=\textbf,
stringstyle=\textit,
keepspaces=true,				   % afficher les espaces
showspaces=false,                  % ne pas les remplacer par des underscores
showstringspaces=false,            % pas non plus dans les strings
}

\newcommand{\hs}{\hspace{0.7cm}}
\newcommand{\be}{\begin{equation}}
\newcommand{\ee}{\end{equation}}
\newcommand{\bee}{\begin{eqnarray}}
\newcommand{\eee}{\end{eqnarray}}
\newcommand{\fin}{\nonumber\\}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}

\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{example}{Example}
\newtheorem{proof}{Proof}
\newtheorem{theorem}{Theorem}

\title{Projet de Systèmes Numériques}
\author{Lubin Bailly, Sylvain Gay, Victor Miquel}
\date{2021}

\begin{document}

\maketitle
\tableofcontents

\section{Instruction Set Architecture}

We will implement a subset of the RISC-V ISA, and a few custom instructions for inputs and outputs. We will be using the opcode maps suggested for RV32G in the RISC-V specification.

\subsection{RV32I Base}

We will implement most of the RV32I as described in the RISC-V specification. We did \emph{not} implement the following instructions:
\begin{itemize}
	\item FENCE
	\item ECALL
	\item EBREAK
\end{itemize}

\subsection{Inputs and Outputs}

We define the following custom instructions in order to handle input and output. Some pins will be read-only and others write-only, no pin can be used as input \emph{and} output.

Inputs and outputs have two dedicated registers \verb|rin| and \verb|rout| (which allows up to 32 inputs and 32 outputs).

We will decide on a later date which pins will be inputs and which will be outputs.

The following instructions all use the same opcode IO, mapped to $00\_010\_11$ (custom-0) - each one has a different value for $func3$ and $func7$.

They all affect $rd := rin$ and $rout := rout \oplus (rs1 \cdot rs2)$, where $\cdot$ represents the operation (SLL, XOR, SRL, SRA, OR, AND).

\begin{center}
\begin{tabular}{| c | c | c | c | c | c || l |}
	\hline
	0000000 & rs2 & rs1 & 001 & rd & 0001011 & OUTSLL \\
	\hline
	0000000 & rs2 & rs1 & 100 & rd & 0001011 & OUTXOR \\
	\hline
	0000000 & rs2 & rs1 & 101 & rd & 0001011 & OUTSRL \\
	\hline
	0100000 & rs2 & rs1 & 101 & rd & 0001011 & OUTSRA \\
	\hline
	0000000 & rs2 & rs1 & 110 & rd & 0001011 & OUTOR \\
	\hline
	0000000 & rs2 & rs1 & 111 & rd & 0001011 & OUTAND \\
	\hline
\end{tabular}
\end{center}

\section{Schematic}

See figure \ref{sch-proc} for a global view of our processor.

\begin{figure}[p]
	\centering
	\includegraphics[width=17cm]{proc.png}
	\caption{Global schematic}
	\label{sch-proc}
\end{figure}

\subsection{Programm Counter}

The "PC" block contains a PC register and two adders: one is an incrementer for PC, the other is used for jumps. Inputs from the Decoder decide (through multiplexers) which output to use and wheter the second adder uses PC or another register.

Because ROM operations use $2$ cycles, we need to delay the value of $PC$ with $2$intermediate registers so we have access to the $PC$ being executed instead of the $PC$ being read. Moreover, we need not to execute the instruction following a jump: it is the next instruction in the sequential order, and the delay of $2$ cycles makes it so that we do not have access to the instruction at the jump address yet.

\subsection{RAM Manager}

This is mainly an intermediate between the RAM and the processor. Its input \verb|RAM_word_size| consists of $2$ bits: the manager must read/write $2^k$ bytes (where $k$ is a number between 0 and 2 represented in these two bits). If it reads less than $4$ bytes, it extends the result with $0$ or with the sign depending on the \verb|RAM_sign_extend| bit.

The RAM uses addresses for $32$ bits blocks, and we need access to $8$ and $16$ bits blocks for the $lh(u)$, $lb(u)$, $sh$, $sb$ instructions. We use shifts of \verb|RAM_word_size| to address this issue.

RAM operations, as well as ROM operations, use $2$ cycles. This doesn't cause any issue for writing, but for reading we need to insert a nop. Moreover, the next instruction is executed at the time of reading the value, so we need to allow double-writing in the registers \emph{and} implement shortcuts if the value is immediatly used.

\subsection{Register Manager}

The register manager contains $32$ registers (among which the \verb|zero| pseudo-register) and $2$ additionnal I/O registers, wired directly to the I/O. Inputs from the Decoder indicate which registers to use as \verb|rs1|, \verb|rs2|, \verb|rd|, whether the instruction involves I/O and whether the instruction writes something into \verb|rd|. This information is processed with multiplexers.

The register manager also implements the details mentionned in the previous subsection.

\subsection{Critical Path}

Our critical path contains the ROM, a few MUX, an adder and the RAM. We implemented a carry-lookahead adder for logarithmic critical path for the adder.

\section{VGA Display}

A circuit independant to the processor is used to communicate with a VGA Display. It uses a dual-read on the RAM to read the pixels at pre-defined addresses.

We support a palette of $16$ $6$-bits colors, and the RAM can be initialized to display a picture.

\section{Clock}

We will only mention the non-trivial parts.

\subsection{Compiling}

\subsubsection{To RISC-V assembly}

We use clang to compile to RISC-V assembly. We add, at the beginning of the produced code, a few lines specific to our device : see \verb|prelude.s|.

\subsubsection{To machine code}

We implemented an assembler which supports everything produced by clang (apart from mul/div, which we managed not to obtain in the produced code).

In particular, labels and the \verb|.data| segment were the main challenges.

\subsection{Inputs}

We use $2$ of the $32$ inputs provided by our processor: one for a clock that is slower than the main clock and one to select the mode.

\subsection{Char display}

\subsubsection{Font in memory}

A font is represented in memory, through automatically generated code. A char is an array $16$ $16$-bits integers, which gives us a table of pixels for each printable ACII character.

\subsubsection{Setting the palette}

We use the outputs of the processor to communicate with the palette manager.

\subsubsection{Setting the RAM}

Each $32$-bits integer in the RAM represents $8$ pixels with $4$-bits colors (using the palette). Using bitwise magic and a static table stored in RAM, we manage to transform the $4$-bits colors and $16$-bits integers to something we can directly write into the RAM.

\end{document}
