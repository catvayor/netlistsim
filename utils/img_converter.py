import matplotlib.image as mpimg


def get_img():
    img = (mpimg.imread('dith16.png')).tolist()
    return [[[round(c*3) for c in pix[:3]] for pix in line] for line in img]

def color_tbl(img):
    ret = []
    for line in img:
        for pix in line:
            if pix not in ret:
                ret.append(pix)
    return ret

def print_tbled(img):
    tbl = color_tbl(img)

    print("memory_initialization_radix=2;")
    print("memory_initialization_vector=")

    tmp=""

    for line in img:
        for n,pix in enumerate(line):
            tmp = format(tbl.index(pix), "04b") + tmp
            if n%8==7:
                print(tmp)
                tmp = ""
    print(";")

def print_tbl(img):
    tbl = color_tbl(img)

    print("    nop")

    for code,(r,g,b) in enumerate(tbl):
        print("    mv x1,", end="")
        print(0xf0000000+(code<<24)+(r<<22)+(g<<14)+(b<<6))
        print("    orio x0,x1,x0")

def print_xorio(img):
    tbl = color_tbl(img)

    for code,(r,g,b) in enumerate(tbl):
        print("    xorio_(&in, ", end="")
        print(hex(0xf0000000+(code<<24)+(r<<22)+(g<<14)+(b<<6)), end="")
        print(");")
