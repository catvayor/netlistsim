#define CLK_FREQ 1000
#define CLK_BIT 0x00000001
#define FAST_BIT 0x00000002

#define WIDTH 640
#define HEIGHT 480

#define POS(x, y) ((x)+((y)<<4)+((y)<<6)) // ram address for positions (8*x, y) to (8*x+7, y)

#define BALL_SIZE 8

#define FGBG(fg, bg, mask) (((fg) & (mask)) + ((bg) & (~(mask))))

int color_octupler(volatile int c){
  return c + (c << 4) + (c << 8) + (c << 12) + (c << 16) + (c << 20) + (c << 24) + (c << 28);
}

volatile int ram[WIDTH*HEIGHT/8];

// Display 1 char at 8*x, y
void display_char(unsigned short int chars[95][16], unsigned int table[256], volatile int x, volatile int y, unsigned char c, int fgc, int bgc){
  c -= ' ';
  for(int row=0; row < 16; ++row){
    int line = chars[c][row];
    int left = table[line & 0xff];
    ram[POS(x,y+row)] = (fgc & left) + (bgc & (~left));
    int right = table[line >> 8];
    ram[POS(x+1,y+row)] = (fgc & right) + (bgc & (~right));
  }
}

void display_text(unsigned short int chars[95][16], unsigned int table[256], int x, int y, char* txt, int fgc, int bgc){
  while (*txt) {
    display_char(chars, table, x, y, *txt, fgc, bgc);
    txt++;
    x+=2;
  }
}

// Fills rectangle [8*x0;8*x1)*[y0;y1)
void fill_rect(volatile int x0, volatile int y0, volatile int x1, volatile int y1, volatile int color){
  for(volatile int y = y0; y < y1; ++y){
    for(volatile int x = x0; x < x1; ++x){
      ram[POS(x,y)] = color;
    }
  }
}

// Fills rectangle [x0;x1)*[y0;y1)
void fill_rect_subword(volatile int x0, volatile int y0, volatile int x1, volatile int y1, volatile int fgc, volatile int bgc) {
  if (x0 >= x1) return;

  volatile int x0_8 = x0>>3;
  volatile int x1_8 = x1>>3;

  volatile int x0_s = x0 & 0b111;
  volatile int x1_s = x1 & 0b111;

  volatile unsigned mask_left = 0xffffffff << (x0_s << 2); // pixel order is reversed...
  volatile unsigned mask_right = ~(0xffffffff << (x1_s << 2)); // pixel order is reversed...

  if (x0_8 == x1_8) {
    volatile unsigned mask = mask_left & mask_right;
    volatile unsigned val = (fgc & mask) + (bgc & (~mask));
    for(volatile int y = y0; y < y1; ++y) {
      ram[POS(x0_8,y)] = val;
    }
  } else {
    if (x0_s == 0) {
      if (x1_s) {
        volatile unsigned val_right = (fgc & mask_right) + (bgc & (~mask_right));
        for(volatile int y = y0; y < y1; ++y) {
          ram[POS(x1_8,y)] = val_right;
        }
      }
      fill_rect(x0_8, y0, x1_8, y1, fgc);
    } else {
      volatile unsigned val_left  = (fgc & mask_left ) + (bgc & (~mask_left ));
      if (x1_s) {
        volatile unsigned val_right = (fgc & mask_right) + (bgc & (~mask_right));
        for(volatile int y = y0; y < y1; ++y) {
          ram[POS(x0_8,y)] = val_left;
          ram[POS(x1_8,y)] = val_right;
        }
      } else {
        for(volatile int y = y0; y < y1; ++y) {
          ram[POS(x0_8,y)] = val_left;
        }
      }
      fill_rect(x0_8+1, y0, x1_8, y1, fgc);
    }
  }
}

// xorio_-> write out, read in
// works iff last_out is the previous out
extern void xorio_(volatile int *in, int out);

int main(){

  // Palette manager
  int in;
  xorio_(&in, 0xf0800080); // 0 -> magenta
  xorio_(&in, 0xf1c08040);
  xorio_(&in, 0xf2c04080);
  xorio_(&in, 0xf3808040);
  xorio_(&in, 0xf4c08080);
  xorio_(&in, 0xf5c04040);
  xorio_(&in, 0xf6804040);
  xorio_(&in, 0xf7808080);
  xorio_(&in, 0xf8804080);
  xorio_(&in, 0xf9c0c000); // 9 -> yellow
  xorio_(&in, 0xfa800040);
  xorio_(&in, 0xfb404040);
  xorio_(&in, 0xfc400040);
  xorio_(&in, 0xfd400000);
  xorio_(&in, 0xfe804000);
  xorio_(&in, 0xff404000);

  int fgc = color_octupler(9);
  int bgc = color_octupler(0);


  #include "fonts/char_map.c"
  #include "quadrupler_table.c"

  fill_rect_subword(3, 4, 5, 6, color_octupler(1), color_octupler(9));
  fill_rect_subword(30, 40, 50, 60, color_octupler(10), 9);

  volatile int clk_sys = 0;
  //volatile int fast_forward = 0;

  int clk_up = clk_sys;
  int cnt = 10;

  #define BALL_X_INIT (WIDTH / 2 - BALL_SIZE / 2)
  #define BALL_Y_INIT (HEIGHT / 2 - BALL_SIZE / 2)

  int ball_x = BALL_X_INIT;
  int ball_y = BALL_Y_INIT;
  int ball_vx = 1;
  int ball_vy = 1;

  #define BALL_X0 ((ball_x >= 0) ? ball_x : 0)
  #define BALL_X1 ((ball_x + BALL_SIZE <= WIDTH) ? (ball_x + BALL_SIZE) : WIDTH)
  #define BALL_Y0 ball_y
  #define BALL_Y1 (ball_y + BALL_SIZE)

  while(1){
    xorio_(&in, 0);
    //fast_forward = in & FAST_BIT;
    clk_sys = in & CLK_BIT;
    if(clk_sys && !clk_up){
      if (cnt--) continue;
      cnt = 10;
      // erase
      fill_rect_subword(BALL_X0, BALL_Y0, BALL_X1, BALL_Y1, bgc, bgc);

      // move
      ball_x += ball_vx;
      if (ball_y + ball_vy <= 0) ball_vy = 1;
      if (ball_y + ball_vy + BALL_SIZE >= HEIGHT) ball_vy = -1;
      ball_y += ball_vy;
      // ball reset
      if ((ball_x + BALL_SIZE <= 0) || (ball_x >= WIDTH)) {
        ball_x = BALL_X_INIT;
        ball_y = BALL_Y_INIT;
      }

      // draw
      fill_rect_subword(BALL_X0, BALL_Y0, BALL_X1, BALL_Y1, fgc, bgc);
    }
    clk_up = clk_sys;
  }

  return 0;
}
