// TODO CLK_FREQ
#define CLK_FREQ 12000000

#define WIDTH 768
#define HEIGHT 512

// addr[x,y] = ecran[y + (x << 9)]

int month_length(int m, int yu, int yte, int yh, int yth){
	if(m == 1){
		// February
		if(((yu + 2*yte) % 4 == 0 && (yu != 0 || yte != 0)) || (yu == 0 && yte == 0 && (yh + 2*yth) % 4 == 0))
			return 29;
		else
			return 28;
	} else {
		if((m < 7) ^ (m % 2))
			return 31;
		else
			return 30;
	}
}

volatile char ram[64];

int main(){
	#include "fonts/char_map.c"

	volatile int clk_sys = 0;

	int cnt = 0;
	int second_units = 0;
	int second_tens = 0;
	int minute_units = 0;
	int minute_tens = 0;
	int hour_units = 0;
	int hour_tens = 0;
	int day = 0;
	int day_units = 0;
	int day_tens = 0;
	int month = 0;
	int month_units = 0;
	int month_tens = 0;
	int year_units = 0;
	int year_tens = 0;
	int year_hundreds = 0;
	int year_thousands = 0;

	int clk_up = clk_sys;
	int length = month_length(month, year_units, year_tens, year_hundreds, year_thousands);

	while(1){
		// TODO: READ INPUT
		clk_sys = 1;
		if(clk_sys){
			if(!clk_up){
				clk_up = 1;
				cnt += 1;
				if(cnt == CLK_FREQ){
					cnt = 0;
					second_units += 1;
					if(second_units == 10){
						second_units = 0;
						second_tens += 1;
						if(second_tens == 6){
							second_tens = 0;
							minute_units += 1;
							if(minute_units == 10){
								minute_units = 0;
								minute_tens += 1;
								if(minute_tens == 6){
									minute_tens = 0;
									hour_units += 1;
									if(hour_units == 10){
										hour_units = 0;
										hour_tens += 1;
									} else if(hour_tens == 2 && hour_units == 4){
										hour_units = 0;
										hour_tens = 0;
										day += 1;
										day_units += 1;
										if(day == length){
											day = 0;
											day_units = 0;
											day_tens = 0;
											month += 1;
											month_units += 1;
											if(month == 12){
												month = 0;
												year_units += 1;
												if(year_units == 10){
													year_units = 0;
													year_tens += 1;
													if(year_tens == 10){
														year_tens = 0;
														year_hundreds += 1;
														if(year_hundreds == 10){
															year_hundreds = 0;
															year_thousands += 1;
															if(year_thousands == 10)
																year_thousands = 0;
														}
													}
												}
											} else if(month_units == 10){
												month_units = 0;
												month_tens += 1;
											}
											length = month_length(month, year_units, year_tens, year_hundreds, year_thousands);
										} else if(day_units == 10){
											day_units = 0;
											day_tens += 1;
										}
										
									}
								}
							}
						}
					}
				}
			}
		} else {
			clk_up = 0;
		}

		// Display
		// TODO
		ram[year_thousands] = 37;
		chars[year_thousands][year_thousands][year_thousands] = 37;
	}

	return 0;
}
