let print_bit fmt (i, x) =
	Format.fprintf fmt "%d" (((if x > 0 then x else (1 lsl 32)+x) lsr i) mod 2)

let print_bits_k_times fmt (k, nb_bits, x) =
	for i = nb_bits-1 downto 0 do
		for j = 0 to k-1 do
			print_bit fmt (i, x)
		done
	done

let print_table fmt n =
	Format.fprintf fmt "unsigned int quadrupler_table[%d];@." n;
	for i = 0 to n-1 do
		Format.fprintf fmt "quadrupler_table[0b%a] = 0b%a;@." print_bits_k_times (1,8,i) print_bits_k_times (4,8,i)
	done

let () = Format.printf "%a" print_table 256
