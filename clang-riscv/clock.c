#define CLK_FREQ 1000
#define CLK_BIT 0x00000001
#define FAST_BIT 0x00000002

#define WIDTH 640
#define HEIGHT 480

#define Y       (HEIGHT - 20)


#define X_BASE  (640/8/2 - 20)

#define X_H_T   (X_BASE + 0)
#define X_H_U   (X_BASE + 2)

#define X_MIN_T (X_BASE + 6)
#define X_MIN_U (X_BASE + 8)

#define X_SEC_T (X_BASE + 12)
#define X_SEC_U (X_BASE + 14)


#define X_D_T   (X_BASE + 20)
#define X_D_U   (X_BASE + 22)

#define X_M_T   (X_BASE + 26)
#define X_M_U   (X_BASE + 28)

#define X_Y_TH  (X_BASE + 32)
#define X_Y_H   (X_BASE + 34)
#define X_Y_T   (X_BASE + 36)
#define X_Y_U   (X_BASE + 38)

#define POS(x, y) ((x)+((y)<<4)+((y)<<6)) // ram address for positions (8*x, y) to (8*x+7, y)

int month_length(int m, int yu, int yte, int yh, int yth){
  if(m == 1){
    // February
    if(((yu + 2*yte) % 4 == 0 && (yu != 0 || yte != 0)) || (yu == 0 && yte == 0 && (yh + 2*yth) % 4 == 0))
      return 29;
    else
      return 28;
  } else {
    if((m < 7) ^ (m % 2))
      return 31;
    else
      return 30;
  }
}

int color_octupler(volatile int c){
  return c + (c << 4) + (c << 8) + (c << 12) + (c << 16) + (c << 20) + (c << 24) + (c << 28);
}

volatile int ram[WIDTH*HEIGHT/8];

// Display 1 char at 8*x, y
void display_char(unsigned short int chars[95][16], unsigned int table[256], volatile int x, volatile int y, unsigned char c, int fgc, int bgc){
  c -= ' ';
  for(int row=0; row < 16; ++row){
    int line = chars[c][row];
    int left = table[line & 0xff];
    ram[POS(x,y+row)] = (fgc & left) + (bgc & (~left));
    int right = table[line >> 8];
    ram[POS(x+1,y+row)] = (fgc & right) + (bgc & (~right));
  }
}

void display_text(unsigned short int chars[95][16], unsigned int table[256], int x, int y, char* txt, int fgc, int bgc){
  while (*txt) {
    display_char(chars, table, x, y, *txt, fgc, bgc);
    txt++;
    x+=2;
  }
}
// Fills rectangle [8*x0;8*x1)*[y0;y1)
void fill_rect(volatile int x0, volatile int y0, volatile int x1, volatile int y1, volatile int color){
  for(volatile int y = y0; y < y1; ++y){
    for(volatile int x = x0; x < x1; ++x){
      ram[POS(x,y)] = color;
    }
  }
}

// xorio_-> write out, read in
// works iff last_out is the previous out
extern void xorio_(volatile int *in, int out);

int main(){

  // Palette manager
  int in;
  xorio_(&in, 0xf0800080); // 0 -> magenta
  xorio_(&in, 0xf1c08040);
  xorio_(&in, 0xf2c04080);
  xorio_(&in, 0xf3808040);
  xorio_(&in, 0xf4c08080);
  xorio_(&in, 0xf5c04040);
  xorio_(&in, 0xf6804040);
  xorio_(&in, 0xf7808080);
  xorio_(&in, 0xf8804080);
  xorio_(&in, 0xf9c0c000); // 9 -> yellow
  xorio_(&in, 0xfa800040);
  xorio_(&in, 0xfb404040);
  xorio_(&in, 0xfc400040);
  xorio_(&in, 0xfd400000);
  xorio_(&in, 0xfe804000);
  xorio_(&in, 0xff404000);

  int fgc = color_octupler(9);
  int bgc = color_octupler(0);


  #include "fonts/char_map.c"
  #include "quadrupler_table.c"

  volatile int clk_sys = 0;
  volatile int fast_forward = 0;

  int cnt = 0;
  int second_units = 0;
  int second_tens = 0;
  int minute_units = 0;
  int minute_tens = 0;
  int hour_units = 0;
  int hour_tens = 0;
  int day = 0;
  int day_units = 0;
  int day_tens = 0;
  int month = 0;
  int month_units = 0;
  int month_tens = 0;
  int year_units = 0;
  int year_tens = 0;
  int year_hundreds = 0;
  int year_thousands = 0;

  int clk_up = clk_sys;
  int length = month_length(month, year_units, year_tens, year_hundreds, year_thousands);

  char txt[16];
  txt[0]='I';
  txt[1]='t';
  txt[2]=' ';
  txt[3]='w';
  txt[4]='o';
  txt[5]='r';
  txt[6]='k';
  txt[7]='s';
  txt[8]='!';
  txt[9]=' ';
  txt[10]='<';
  txt[11]='3';
  txt[12]=0;

  char init_date[24];
  init_date[ 0]='0';
  init_date[ 1]='0';
  init_date[ 2]=':';
  init_date[ 3]='0';
  init_date[ 4]='0';
  init_date[ 5]=':';
  init_date[ 6]='0';
  init_date[ 7]='0';
  init_date[ 8]=' ';
  init_date[ 9]=' ';
  init_date[10]='0';
  init_date[11]='0';
  init_date[12]='/';
  init_date[13]='0';
  init_date[14]='0';
  init_date[15]='/';
  init_date[16]='0';
  init_date[17]='0';
  init_date[18]='0';
  init_date[19]='0';
  init_date[20]=0;

  display_text(chars, quadrupler_table, 28, 20, txt, fgc, bgc);
  display_text(chars, quadrupler_table, X_BASE, Y, init_date, fgc, bgc);

  while(1){
    xorio_(&in, 0);
    fast_forward = in & FAST_BIT;
    if (fast_forward) goto forward;
    clk_sys = in & CLK_BIT;
    if(clk_sys && !clk_up){
      cnt += 1;
      if(cnt == CLK_FREQ){
        cnt = 0;
        forward:
        second_units += 1;
        if(second_units == 10){
          second_units = 0;
          second_tens += 1;
          if(second_tens == 6){
            second_tens = 0;
            minute_units += 1;
            if(minute_units == 10){
              minute_units = 0;
              minute_tens += 1;
              if(minute_tens == 6){
                minute_tens = 0;
                hour_units += 1;
                if(hour_units == 10){
                  hour_units = 0;
                  hour_tens += 1;
                  display_char(chars, quadrupler_table, X_H_T, Y, hour_tens+'0', fgc, bgc);
                } else if(hour_tens == 2 && hour_units == 4){
                  hour_units = 0;
                  hour_tens = 0;
                  day += 1;
                  day_units += 1;
                  if(day == length){
                    day = 0;
                    day_units = 0;
                    day_tens = 0;
                    month += 1;
                    month_units += 1;
                    if(month == 12){
                      month = 0;
                      month_units = 0;
                      month_tens = 0;
                      year_units += 1;
                      if(year_units == 10){
                        year_units = 0;
                        year_tens += 1;
                        if(year_tens == 10){
                          year_tens = 0;
                          year_hundreds += 1;
                          if(year_hundreds == 10){
                            year_hundreds = 0;
                            year_thousands += 1;
                            if(year_thousands == 10)
                              year_thousands = 0;
                            display_char(chars, quadrupler_table, X_Y_TH, Y, year_thousands+'0', fgc, bgc);
                          }
                          display_char(chars, quadrupler_table, X_Y_H, Y, year_hundreds+'0', fgc, bgc);
                        }
                        display_char(chars, quadrupler_table, X_Y_T, Y, year_tens+'0', fgc, bgc);
                      }
                      display_char(chars, quadrupler_table, X_Y_U, Y, year_units+'0', fgc, bgc);
                      display_char(chars, quadrupler_table, X_M_T, Y, month_tens+'0', fgc, bgc);
                    } else if(month_units == 10){
                      month_units = 0;
                      month_tens += 1;
                      display_char(chars, quadrupler_table, X_M_T, Y, month_tens+'0', fgc, bgc);
                    }
                    length = month_length(month, year_units, year_tens, year_hundreds, year_thousands);
                    display_char(chars, quadrupler_table, X_M_U, Y, month_units+'0', fgc, bgc);
                    display_char(chars, quadrupler_table, X_D_T, Y, day_tens+'0', fgc, bgc);
                  } else if(day_units == 10){
                    day_units = 0;
                    day_tens += 1;
                    display_char(chars, quadrupler_table, X_D_T, Y, day_tens+'0', fgc, bgc);
                  }
                  display_char(chars, quadrupler_table, X_D_U, Y, day_units+'0', fgc, bgc);
                  display_char(chars, quadrupler_table, X_H_T, Y, hour_tens+'0', fgc, bgc);
                }
                display_char(chars, quadrupler_table, X_H_U, Y, hour_units+'0', fgc, bgc);
              }
              display_char(chars, quadrupler_table, X_MIN_T, Y, minute_tens+'0', fgc, bgc);
            }
            display_char(chars, quadrupler_table, X_MIN_U, Y, minute_units+'0', fgc, bgc);
          }
          display_char(chars, quadrupler_table, X_SEC_T, Y, second_tens+'0', fgc, bgc);
        }
        display_char(chars, quadrupler_table, X_SEC_U, Y, second_units+'0', fgc, bgc);
      }
    }
    clk_up = clk_sys;
  }

  return 0;
}
