import sys

for line in sys.stdin.readlines():
  if '=' in line:
    eq = line.index('=')
    val = int(line[eq+1:-2])
    print(line[:eq+1], "0b", "".join(reversed(format(val, "016b"))), ';', sep="")
  else:
    print(line)
