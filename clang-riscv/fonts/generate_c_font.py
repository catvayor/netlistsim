#!/usr/bin/python

"""
Based on https://stackoverflow.com/a/36386628
"""
from __future__ import print_function
import string
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
import numpy as np

def char_to_pixels(text, path, fontsize=14):
	"""
	Based on https://stackoverflow.com/a/27753869/190597 (jsheperd)
	"""
	font = ImageFont.truetype(path, fontsize) 
	w, h = font.getsize(text)  
	h *= 2
	image = Image.new('L', (w, h), 1)  
	draw = ImageDraw.Draw(image)
	draw.text((0, 0), text, font=font) 
	arr = np.asarray(image)
	arr = np.where(arr, 0, 1)
	arr = arr[(arr != 0).any(axis=1)]
	return arr

def display(arr):
	result = np.where(arr, '#', ' ')
	print('\n'.join([''.join(row) for row in result]))

string = " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
print(len(string))
arr = char_to_pixels(
	string, 
	path='/usr/share/fonts/truetype/liberation/LiberationMono-Regular.ttf', 
	fontsize=16)
print(arr.shape)

nb_chars = len(string)
height = 16
width = 10
result = np.where(arr, 1, 0)
print("unsigned short int chars[%d][%d];" % (nb_chars, height))
for i in range(nb_chars):
	for row in range(height):
		val = 0
		for col in range(width):
			val += result[row][i*width+col] << col
		print("chars[%d][%d]=%d;" % (i, row, val))
