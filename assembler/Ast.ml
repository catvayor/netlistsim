module Smap = Map.Make(String)

type reg = int
type size = Byte | Half | Word

type comp =
	| Eq  | Ne
	| Lt  | Ge
	| Ltu | Geu

type op =
	| Add | Sub
	| Sll | Srl | Sra
	| Slt | Sltu
	| Xor | Or | And

type instr =
	| Lui of reg * int
	| Auipc of reg * int
	| Jal of reg * int
	| JalLbl of reg * string
	| Jalr of reg * reg * int
	| Branch of comp * reg * reg * int
	| BranchLbl of comp * reg * reg * string
	| Load of size * bool * reg * reg * int
	| Store of size * reg * reg * int
	| Opimm of op * reg * reg * int
	| Opreg of op * reg * reg * reg
	| Opio of op * reg * reg * reg

type line =
	| Instr of instr
	| Label of string

type file = line list