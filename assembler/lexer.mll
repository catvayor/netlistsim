
(* Analyseur lexical pour Mini-Python *)

{
  open Lexing
  open Ast
  open Parser

  exception Lexing_error of string

  let id_or_kwd =
    let h = Hashtbl.create 32 in
    List.iter (fun (s, tok) -> Hashtbl.add h s tok)
      ["lui", LUI; "auipc", AUIPC; "jal", JAL; "jalr", JALR;
       "beq", BEQ; "bne", BNE; "bge", BGE; "bltu", BLTU; "bgeu", BGEU;
       "blt", BLT; "bge", BGE;
       "lb", LB; "lh", LH; "lw", LW; "lbu", LBU; "lhu", LHU;
       "sb", SB; "sh", SH; "sw", SW; "add", OP Add; "sub", OP Sub;
       "sll", OP Sll; "slt", OP Slt; "sltu", OP Sltu; "xor", OP Xor;
       "srl", OP Srl; "sra", OP Sra; "or", OP Or; "and", OP And;
       "sllio", OPIO Sll; "xorio", OPIO Xor; "srlio", OPIO Srl;
       "sraio", OPIO Sra; "orio", OPIO Or; "andio", OPIO And;
       "colon", COLON;

       "mov", MOV; "nop", NOP; "j", J;
      ];
   fun s -> try Hashtbl.find h s with Not_found -> LBL s

  let reg_alias=
    let h = Hashtbl.create 32 in
    List.iter (fun (s, tok) -> Hashtbl.add h s tok)
      ["zero" , 0; "ra", 1; "sp", 2; "gp", 3; "tp", 4;
       "t0", 5; "t1", 6; "t2", 7; "fp",8; "s0",8;
    	"s1",9;"a0",10;"a1",11;"a2",12;"a3",13;"a4",14;
    	"a5",15;"a6",16;"a7",17;"s2",18;"s3",19;"s4",20;
    	"s5",21;"s6",22;"s7",23;"s8",24;"s9",25;"s10",26;"s11",27;
    	"t3",28;"t4",29;"t5",30;"t6",31
      ];
   fun s -> try REG (Hashtbl.find h s) with Not_found -> raise 
			(Lexing_error ("unknwown register %"^s))
}

let letter = ['a'-'z' 'A'-'Z']
let digit = ['0'-'9']
let ident = (letter|'.') (letter | digit | '_')*
let integer = '-'? ['0'-'9']+
let space = ' ' | '\t'
let comment = "#" [^'\n']*

rule next_token = parse
  | '\n'    { new_line lexbuf; next_token lexbuf }
  | (space | comment)+
            { next_token lexbuf }
  | ident as id { id_or_kwd id }
  | ':'     { COLON }
  | "%r" (integer as s)
            { try let x = int_of_string s in
                if x < 0 || x >= 32 then
                  raise (Lexing_error ("unknown register: %r" ^ s))
                else
                  REG x
              with _ -> raise (Lexing_error ("constant too large: " ^ s))
            }
  | '%' (ident as s) { reg_alias s }
  | '$' (integer as s)
            { try IMM (int_of_string s)
              with _ -> raise (Lexing_error ("constant too large: " ^ s)) }
  | eof     { EOF }
  | _ as c  { raise (Lexing_error ("illegal character: " ^ String.make 1 c)) }

{



}
