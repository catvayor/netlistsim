
/* Analyseur syntaxique pour Mini-Python */

%{
  open Ast
%}

%token <int> IMM
%token <string> LBL
%token <int> REG
%token LUI AUIPC JAL JALR BEQ BNE BLT BGE BLTU BGEU
%token LB LH LW LBU LHU SB SH SW
%token MOV NOP J
%token <Ast.op> OP
%token <Ast.op> OPIO
%token COLON
%token EOF


/* Point d'entrée de la grammaire */
%start file

/* Type des valeurs renvoyées par l'analyseur syntaxique */
%type <Ast.file> file

%%

file:
| l = list(line) EOF { List.fold_right (@) l [] }
;

line:
| s=LBL COLON { [Label s] }
| l=instr     { List.map (fun i -> Instr i) l }
;

%inline instr:
| LUI rd=REG imm=IMM             { [Lui (rd, imm)] }
| AUIPC rd=REG imm=IMM           { [Auipc (rd, imm)] }
| JAL rd=REG ofs=IMM             { [Jal (rd, ofs)] }
| JAL rd=REG lbl=LBL             { [JalLbl (rd, lbl)]}
| JALR rd=REG rs1=REG ofs=IMM    { [Jalr (rd, rs1, ofs)] }
| c=comp rs1=REG rs2=REG ofs=IMM { [Branch (c, rs1, rs2, ofs)] }
| c=comp rs1=REG rs2=REG lbl=LBL { [BranchLbl (c, rs1, rs2, lbl)] }
| LB rd=REG addr=REG ofs=IMM     { [Load (Byte, false, rd, addr, ofs)] }
| LH rd=REG addr=REG ofs=IMM     { [Load (Half, false, rd, addr, ofs)] }
| LW rd=REG addr=REG ofs=IMM     { [Load (Word, false, rd, addr, ofs)] }
| LBU rd=REG addr=REG ofs=IMM    { [Load (Byte, true, rd, addr, ofs)] }
| LHU rd=REG addr=REG ofs=IMM    { [Load (Half, true, rd, addr, ofs)] }
| SB addr=REG data=REG ofs=IMM   { [Store (Byte, addr, data, ofs)] }
| SH addr=REG data=REG ofs=IMM   { [Store (Half, addr, data, ofs)] }
| SW addr=REG data=REG ofs=IMM   { [Store (Word, addr, data, ofs)] }
| o=OP rd=REG rs1=REG imm=IMM    { [Opimm (o, rd, rs1, imm)] }
| o=OP rd=REG rs1=REG rs2=REG    { [Opreg (o, rd, rs1, rs2)] }
| o=OPIO rd=REG rs1=REG rs2=REG  { [Opio (o, rd, rs1, rs2)] }

/* Pseudo-instructions : */
| MOV rd=REG rs=REG              { [Opimm (Or, rd, rs, 0)] }
| MOV rd=REG imm=IMM             {
  let imm_sup = imm lsr 12 in
  if imm_sup != 0 then
    if imm - (imm_sup lsl 12) != 0 then
      [Lui (rd, imm_sup); Opimm (Or, rd, rd, imm)]
    else
      [Lui (rd, imm_sup)]
  else
    [Opimm (Or, rd, 0, imm)]
}
| NOP                            { [Opimm (Or, 0, 0, 0)] }
| J imm=IMM                      { [Jal (0, imm)] }
| J lbl=LBL                      { [JalLbl (0, lbl)] }

%inline comp:
| BEQ {Eq}
| BNE {Ne}
| BLT {Lt} | BLTU {Ltu}
| BGE {Ge} | BGEU {Geu}