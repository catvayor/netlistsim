# ------- #
# Palette #
# ------- #

    nop

    mov %r1 $4026531840 # 0 -> #000000
    orio %r0 %r1 %r0
    mov %r1 $4160749823 # 1 -> #FF0000
    orio %r0 %r1 %r0
    mov %r1 $4093705984 # 2 -> #00FF00
    orio %r0 %r1 %r0
    mov %r1 $4227923967 # 3 -> #FFFF00
    orio %r0 %r1 %r0
    mov %r1 $4076797952 # 4 -> #0000FF
    orio %r0 %r1 %r0
    mov %r1 $4211015935 # 5 -> #FF00FF
    orio %r0 %r1 %r0
    mov %r1 $4143972096 # 6 -> #00FFFF
    orio %r0 %r1 %r0
    mov %r1 $4278190079 # 7 -> #FFFFFF
    orio %r0 %r1 %r0
